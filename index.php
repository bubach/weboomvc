<?php
/***
 * Name:       WebooMVC
 * About:      An MVC application framework for PHP
 * Copyright:  (C) 2014-2015, All rights reserved.
 * Author:     Christoffer Bubach
 * License:    MIT, see included license file
 ***/

// PHP error reporting level, if different from default
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(E_ALL);

// define to 0 if you want errors/exceptions handled externally
define('WMVC_ERROR_HANDLING',1);

// directory separator alias
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

// set the base directory
if (!defined('WMVC_BASEDIR')) {
    define('WMVC_BASEDIR', dirname(__FILE__) . DS);
}

// include main wmvc class
require(WMVC_BASEDIR . 'system' . DS . 'WebooMVC.php');

// instantiate & run
wmvc::app()->run();