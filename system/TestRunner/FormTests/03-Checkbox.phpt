--TEST--
Checkbox
--FILE--
<?php
$formBuilder = $this->getObject('Form_Model_FormBuilder');
echo $formBuilder->checkbox('terms')."\n";
?>
--EXPECTF--
<input type="checkbox" name="terms" value="1">
