--TEST--
Button
--FILE--
<?php
$formBuilder = $this->getObject('Form_Model_FormBuilder');
echo $formBuilder->button('click-me', 'Click Me')."\n";;
?>
--EXPECTF--
<button type="button" name="click-me">Click Me</button>
