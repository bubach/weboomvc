--TEST--
Horizontal Bootstrap markup
--FILE--
<?php
$formBuilder = $this->getObject('BootForm_Model_BootForm');
$formBuilder->openHorizontal();
echo $formBuilder->text('email', null, 'Email')->render()."\n";
?>
--EXPECTF--
<div class="form-group"><label class="control-label col-lg-2" for="email">Email</label><div class="col-lg-10"><input type="text" name="email" id="email" class="form-control"></div></div>
