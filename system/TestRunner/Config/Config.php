<?php
/**
 * Config for automated tests module
 *
 * @package     TestRunner
 * @author      Christoffer Bubach
 */
return array (
    'Modules' => array (
        'System' => array (
            'TestRunner' => array (
                'Config' => array (
                    'version' => '0.0.1',
                    'active' => true,
                    'setup' => true,
                ),
                'Model' => array (),
                'View' => array (),
                'Controller' => array (),
                'Plugins' => array (),
            ),
        ),
    ),
    'Routes' => array (
        'default' => array (
            '/runTests'    => 'TestRunner_Controller_Default',
        ),
    ),
    'Layout' => array (
        'TestRunner_Default_Index' => array (
            'root' => array (
                'view' => 'Core_View_Base',
                'data' => array (
                    'template' => 'html/testRuns.phtml',
                ),
            ),
        ),
    ),
);