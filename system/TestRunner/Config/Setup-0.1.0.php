<?php
/**
 * Setup-0.1.0.php
 *
 * Tables for running database tests
 *
 * @package     TestRunner
 * @author      Christoffer Bubach
 * @class       Core_Plugin_Setup
 */

$db = wmvc::app()->getDb();
$tables = array(
    'testcountry' => array(
        'id'   => array('type' => 'primary'),
        'name' => array('type' => 'string', 'length' => 20, 'null' => false),
    ),
    'testuser' => array(
        'id'         => array('type' => 'primary'),
        'testcountry_id' => array(
            'type' => 'integer',
            'null' => false,
            'index' => true,
            'constraint' => array('table' => 'testcountry', 'field' => 'id'),
        ),
        'type'       => array(
            'type'    => 'enum',
            'options' => array('admin', 'author'),
            'null'    => false
        ),
        'name'       => array('type' => 'string', 'length' => 20, 'null' => false),
    ),
    'testarticle' => array(
        'id'           => array('type' => 'primary'),
        'testuser_id'      => array(
            'type'       => 'integer',
            'null'       => false,
            'default'    => 0,
            'index'      => true,
            'constraint' => array('table' => 'testuser', 'field' => 'id'),
        ),
        'published_at' => array('type' => 'datetime', 'null' => false, 'default' => 0),
        'title'        => array('type' => 'string', 'length' => 100, 'null' => false, 'default' => ''),
        'content'      => array('type' => 'string', 'null' => false, 'default' => ''),
    ),
    'testcomment' => array(
        'id'         => array('type' => 'primary'),
        'testarticle_id' => array(
            'type'       => 'integer',
            'null'       => false,
            'index'      => true,
            'constraint' => array('table' => 'testarticle', 'field' => 'id'),
        ),
        'testuser_id' => array(
            'type'       => 'integer',
            'null'       => false,
            'index'      => true,
            'constraint' => array('table' => 'testuser', 'field' => 'id'),
        ),
        'content' => array('type' => 'string', 'length' => 100, 'null' => false),
    ),
);
$db->schema()->createTable($tables);


$db->insertInto('testcountry', array(
    array('id' => 1, 'name' => 'Slovakia'),
))->execute();

$db->insertInto('testuser', array(
    array('id' => 1, 'testcountry_id' => 1, 'type' => 'admin',  'name' => 'Marek'),
    array('id' => 2, 'testcountry_id' => 1, 'type' => 'author', 'name' => 'Robert'),
))->execute();

$db->insertInto('testarticle', array(
    array('id' => 1, 'testuser_id' => 1, 'published_at' => '2011-12-10 12:10:00', 'title' => 'article 1', 'content' => 'content 1'),
    array('id' => 2, 'testuser_id' => 2, 'published_at' => '2011-12-20 16:20:00', 'title' => 'article 2', 'content' => 'content 2'),
    array('id' => 3, 'testuser_id' => 1, 'published_at' => '2012-01-04 22:00:00', 'title' => 'article 3', 'content' => 'content 3')
))->execute();

$db->insertInto('testcomment', array(
    array('id' => 1, 'testarticle_id' => 1, 'testuser_id' => 2, 'content' => 'comment 1.1'),
    array('id' => 2, 'testarticle_id' => 1, 'testuser_id' => 1, 'content' => 'comment 1.2'),
    array('id' => 3, 'testarticle_id' => 2, 'testuser_id' => 1, 'content' => 'comment 2.1'),
))->execute();