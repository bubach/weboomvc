--TEST--
Disable and enable smart join feature
--FILE--
<?php

$query = $db->from('testcomment')->select('testcomment.*')
		->select('testuser.name')
		->orderBy('testarticle.published_at')
		->getQuery();
echo "-- Plain:\n$query\n\n";

$query = $db->from('testcomment')->select('testcomment.*')
		->select('testuser.name')
		->disableSmartJoin()
		->orderBy('testarticle.published_at')
		->getQuery();
echo "-- Disable:\n$query\n\n";

$query = $db->from('testcomment')->select('testcomment.*')
		->disableSmartJoin()
		->select('testuser.name')
		->enableSmartJoin()
		->orderBy('testarticle.published_at')
		->getQuery();
echo "-- Disable and enable:\n$query\n\n";

?>
--EXPECTF--
-- Plain:
SELECT testcomment.*, testuser.name
FROM testcomment
    LEFT JOIN testuser ON testuser.id = testcomment.testuser_id
    LEFT JOIN testarticle ON testarticle.id = testcomment.testarticle_id
ORDER BY testarticle.published_at

-- Disable:
SELECT testcomment.*, testuser.name
FROM testcomment
ORDER BY testarticle.published_at

-- Disable and enable:
SELECT testcomment.*, testuser.name
FROM testcomment
    LEFT JOIN testuser ON testuser.id = testcomment.testuser_id
    LEFT JOIN testarticle ON testarticle.id = testcomment.testarticle_id
ORDER BY testarticle.published_at

