--TEST--
short join - default join is left join
--FILE--
<?php

$query = $db->from('testarticle')->leftJoin('testuser');
echo $query->getQuery() . "\n";
$query = $db->from('testarticle')->leftJoin('testuser author');
echo $query->getQuery() . "\n";
$query = $db->from('testarticle')->leftJoin('testuser AS author');
echo $query->getQuery() . "\n";

?>
--EXPECTF--
SELECT testarticle.*
FROM testarticle
    LEFT JOIN testuser ON testuser.id = testarticle.testuser_id
SELECT testarticle.*
FROM testarticle
    LEFT JOIN testuser AS author ON author.id = testarticle.testuser_id
SELECT testarticle.*
FROM testarticle
    LEFT JOIN testuser AS author ON author.id = testarticle.testuser_id
