--TEST--
WHERE reset
--FILE--
<?php

$query = $db->from('testuser')->where('id > ?', 0)->orderBy('name');
$query = $query->where(null)->where('name = ?', 'Marek');
echo $query->getQuery() . "\n";
print_r($query->getParameters());
print_r($query->fetch());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE name = ?
ORDER BY name
Array
(
    [0] => Marek
)
Array
(
    [id] => 1
    [testcountry_id] => 1
    [type] => admin
    [name] => Marek
)
