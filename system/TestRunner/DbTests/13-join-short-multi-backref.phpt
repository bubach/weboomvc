--TEST--
multi short join
--FILE--
<?php

$query = $db->from('testarticle')->innerJoin('testcomment:testuser AS comment_user');
echo $query->getQuery() . "\n";
print_r($query->fetch());
?>
--EXPECTF--
SELECT testarticle.*
FROM testarticle
    INNER JOIN testcomment ON testcomment.testarticle_id = testarticle.id
    INNER JOIN testuser AS comment_user ON comment_user.id = testcomment.testuser_id
Array
(
    [id] => 1
    [testuser_id] => 1
    [published_at] => 2011-12-10 12:10:00
    [title] => article 1
    [content] => content 1
)
