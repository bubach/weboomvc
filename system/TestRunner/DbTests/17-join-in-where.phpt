--TEST--
join in where
--FILE--
<?php

$query = $db->from('testarticle')->where('testcomment:content <> "" AND testuser.testcountry.id = ?', 1);
echo $query->getQuery() . "\n";

?>
--EXPECTF--
SELECT testarticle.*
FROM testarticle
    LEFT JOIN testcomment ON testcomment.testarticle_id = testarticle.id
    LEFT JOIN testuser ON testuser.id = testarticle.testuser_id
    LEFT JOIN testcountry ON testcountry.id = testuser.testcountry_id
WHERE testcomment.content <> ""
    AND testcountry.id = ?
