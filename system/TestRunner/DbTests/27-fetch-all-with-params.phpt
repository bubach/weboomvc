--TEST--
fetch all with params
--FILE--
<?php

$result = $db->from('testuser')->fetchRows('id', 'type, name');
print_r($result);

?>
--EXPECTF--
Array
(
    [1] => Array
        (
            [id] => 1
            [type] => admin
            [name] => Marek
        )

    [2] => Array
        (
            [id] => 2
            [type] => author
            [name] => Robert
        )

)
