--TEST--
FROM table from other database
--FILE--
<?php

$query = $db->from('db2.user')->order('db2.user.name')->getQuery();
echo "$query\n";

?>
--EXPECTF--
SELECT db2.user.*
FROM db2.user
ORDER BY db2.user.name
