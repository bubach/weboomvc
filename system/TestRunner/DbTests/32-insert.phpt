--TEST--
insert into
--FILE--
<?php

$query = $db->insertInto('testarticle',
		array(
			'testuser_id' => 1,
			'title' => 'new title',
			'content' => 'new content'
		));

echo $query->getQuery() . "\n";
$lastInsert = $query->execute();

$db->getPDO()->query('DELETE FROM testarticle WHERE id > 3')->execute();
?>
--EXPECTF--
INSERT INTO testarticle (testuser_id, title, content)
VALUES (?, ?, ?)
