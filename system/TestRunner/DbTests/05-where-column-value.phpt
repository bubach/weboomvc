--TEST--
where('column', 'value')
--FILE--
<?php

$query = $db->from('testuser')->where('type', 'author');

echo $query->getQuery() . "\n";
print_r($query->getParameters());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE type = ?
Array
(
    [0] => author
)
