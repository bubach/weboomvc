--TEST--
where('column', null)
--FILE--
<?php
$query = $db->from('testuser')->where('type', null);

echo $query->getQuery() . "\n";
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE type IS NULL
