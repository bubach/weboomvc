--TEST--
join same two tables
--FILE--
<?php

$query = $db->from('testcomment')->leftJoin('testarticle.testuser');
echo $query->getQuery() . "\n";
?>
--EXPECTF--
SELECT testcomment.*
FROM testcomment
    LEFT JOIN testarticle ON testarticle.id = testcomment.testarticle_id
    LEFT JOIN testuser ON testuser.id = testarticle.testuser_id
