--TEST--
join in where
--FILE--
<?php

$query = $db->from('testarticle')->select(array('testarticle.*', 'testuser.name as author'));
echo $query->getQuery() . "\n";

?>
--EXPECTF--
SELECT testarticle.*, testuser.name as author
FROM testarticle
    LEFT JOIN testuser ON testuser.id = testarticle.testuser_id
