--TEST--
where('column', array(..))
--FILE--
<?php

$query = $db->from('testuser')->where('id', array(1,2,3));

echo $query->getQuery() . "\n";
print_r($query->getParameters());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE id IN (1, 2, 3)
Array
(
)
