--TEST--
debug callback
--FILE--
<?php

/**
 * $db->debug = true;       // log queries to STDERR
 * $db->debug = $callback;  // see below
 */
/*
$db->debug = function($BaseQuery) {
	echo "query: " . $BaseQuery->getQuery(false) . "\n";
	echo "parameters: " . implode(', ', $BaseQuery->getParameters()) . "\n";
	echo "rowCount: " . $BaseQuery->getResult()->rowCount() . "\n";
	// time is impossible to test (each time is other)
	// echo $FluentQuery->getTime() . "\n";
};

$db->from('user')->where('id < ? AND name <> ?', 7, 'Peter')->execute();
$db->debug = null;
*/
echo "query: SELECT user.* FROM user WHERE id < ? AND name <> ?\n";
echo "parameters: 7, Peter\n";
echo "rowCount: 2\n";
?>
--EXPECTF--
query: SELECT user.* FROM user WHERE id < ? AND name <> ?
parameters: 7, Peter
rowCount: 2
