--TEST--
PDO::FETCH_OBJ option.
--FILE--
<?php

$query = $db->from('testuser')->where('id > ?', 0)->orderBy('name');
$query = $query->where('name = ?', 'Marek');
$db->getPdo()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

print_r($query->getParameters());
print_r($query->fetch());

// Set back for other tests.
$db->getPdo()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_BOTH);
?>
--EXPECTF--
Array
(
    [0] => 0
    [1] => Marek
)
stdClass Object
(
    [id] => 1
    [testcountry_id] => 1
    [type] => admin
    [name] => Marek
)
