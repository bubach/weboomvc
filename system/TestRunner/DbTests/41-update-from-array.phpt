--TEST--
Basic update
--FILE--
<?php

$query = $db->update('testuser')->set(array('name' => 'keraM', '`type`' => 'author'))->where('id', 1);
$query->execute();
echo $query->getQuery() . "\n";
print_r($query->getParameters()) . "\n";

$query = $db->update('testuser')->set(array('name' => 'Marek', '`type`' => 'admin'))->where('id', 1);
$query->execute();
?>
--EXPECTF--
UPDATE testuser
SET name = ?, `type` = ?
WHERE id = ?
Array
(
    [0] => keraM
    [1] => author
    [2] => 1
)
