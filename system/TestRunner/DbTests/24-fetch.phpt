--TEST--
fetch 
--FILE--
<?php

echo $db->from('testuser', 1)->fetch('name') . "\n";
print_r($db->from('testuser', 1)->fetch());
if ($db->from('testuser', 3)->fetch() === false) echo "false\n";
if ($db->from('testuser', 3)->fetch('name') === false) echo "false\n";

?>
--EXPECTF--
Marek
Array
(
    [id] => 1
    [testcountry_id] => 1
    [type] => admin
    [name] => Marek
)
false
false
