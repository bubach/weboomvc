--TEST--
join using USING
--FILE--
<?php

//$fluent_structure2 = new FluentStructure('%s_id', '%s_id');
$db2 = $db;

$query = $db2->from('article')->select('article.*')
		->innerJoin('user USING (user_id)')
		->select('user.*')
		->getQuery();
echo "$query\n";

$query = $db2->from('article')->select('article.*')
		->innerJoin('user u USING (user_id)')
		->select('u.*')
		->getQuery();
echo "$query\n";

$query = $db2->from('article')->select('article.*')
		->innerJoin('user AS u USING (user_id)')
		->select('u.*')
		->getQuery();
echo "$query\n";

//unset($fluent_structure2);
unset($db2);

?>
--EXPECTF--
SELECT article.*, user.*
FROM article
    INNER JOIN user USING (user_id)
SELECT article.*, u.*
FROM article
    INNER JOIN user u USING (user_id)
SELECT article.*, u.*
FROM article
    INNER JOIN user AS u USING (user_id)
