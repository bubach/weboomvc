--TEST--
join in where
--FILE--
<?php

$query = $db->from('testarticle')->orderBy('testuser.name, testarticle.title');
echo $query->getQuery() . "\n";

?>
--EXPECTF--
SELECT testarticle.*
FROM testarticle
    LEFT JOIN testuser ON testuser.id = testarticle.testuser_id
ORDER BY testuser.name, testarticle.title
