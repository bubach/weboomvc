--TEST--
Basic update
--FILE--
<?php

$query = $db->update('testcountry')->set('name', 'aikavolS')->where('id', 1);
$query->execute();
echo $query->getQuery() . "\n";
print_r($query->getParameters()) . "\n";

$query = $db->from('testcountry')->where('id', 1);
print_r($query->fetch());

$db->update('testcountry')->set('name', 'Slovakia')->where('id', 1)->execute();

$query = $db->from('testcountry')->where('id', 1);
print_r($query->fetch());
?>
--EXPECTF--
UPDATE testcountry
SET name = ?
WHERE id = ?
Array
(
    [0] => aikavolS
    [1] => 1
)
Array
(
    [id] => 1
    [name] => aikavolS
)
Array
(
    [id] => 1
    [name] => Slovakia
)
