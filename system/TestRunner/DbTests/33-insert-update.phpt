--TEST--
INSERT with ON DUPLICATE KEY UPDATE
--FILE--
<?php

$query = $db->insertInto('testarticle', array('id' => 1))
		->onDuplicateKeyUpdate(array(
			'title' => 'article 1b',
			'content' => 'content 1b',
		));

echo $query->getQuery() . "\n";
echo 'last_inserted_id = ' . $query->execute() . "\n";
$q = $db->from('testarticle', 1)->fetch();
print_r($q);
$query = $db->insertInto('testarticle', array('id' => 1))
		->onDuplicateKeyUpdate(array(
			'title' => 'article 1',
			'content' => 'content 1',
		))->execute();
echo "last_inserted_id = $query\n";
$q = $db->from('testarticle', 1)->fetch();
print_r($q);
?>
--EXPECTF--
INSERT INTO testarticle (id)
VALUES (?)
ON DUPLICATE KEY UPDATE title = ?, content = ?
last_inserted_id = 1
Array
(
    [id] => 1
    [testuser_id] => 1
    [published_at] => 2011-12-10 12:10:00
    [title] => article 1b
    [content] => content 1b
)
last_inserted_id = 1
Array
(
    [id] => 1
    [testuser_id] => 1
    [published_at] => 2011-12-10 12:10:00
    [title] => article 1
    [content] => content 1
)
