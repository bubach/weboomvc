--TEST--
full join
--FILE--
<?php
$query = $db->from('testarticle')
		->select(array('testarticle.*', 'testuser.name'))
		->leftJoin('testuser ON testuser.id = testarticle.testuser_id')
		->orderBy('testarticle.title');

echo $query->getQuery() . "\n";
foreach ($query as $row) {
	echo "$row[name] - $row[title]\n";
}
?>
--EXPECTF--
SELECT testarticle.*, testuser.name
FROM testarticle
    LEFT JOIN testuser ON testuser.id = testarticle.testuser_id
ORDER BY testarticle.title
Marek - article 1
Robert - article 2
Marek - article 3
