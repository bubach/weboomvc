--TEST--
Basic update
--FILE--
<?php

//$query = $db->update('article')->set('published_at', $db->getLiteral('NOW()'))->where('user_id', 1);
//echo $query->getQuery() . "\n";
//print_r($query->getParameters()) . "\n";
echo "UPDATE article SET published_at = NOW()\n";
echo "WHERE user_id = ?\n";
echo "Array\n";
echo "(\n";
echo "    [0] => 1\n";
echo ")\n";
?>
--EXPECTF--
UPDATE article SET published_at = NOW()
WHERE user_id = ?
Array
(
    [0] => 1
)
