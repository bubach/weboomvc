--TEST--
Countable interface (doesn't break prev query)
--FILE--
<?php

$articles = $db
	->from('testarticle')
	->select('title')
	->where('id > 1');

echo count($articles) . "\n";
print_r($articles->fetchRows());
?>
--EXPECTF--
2
Array
(
    [0] => Array
        (
            [title] => article 2
        )

    [1] => Array
        (
            [title] => article 3
        )

)
