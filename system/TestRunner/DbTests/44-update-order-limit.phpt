--TEST--
Update with ORDER BY and LIMIT
--FILE--
<?php

$query = $db->update('user')
	->set(array('type' => 'author'))
	->where('id', 2)
	->orderBy('name')
	->limit(1);

echo $query->getQuery() . "\n";
print_r($query->getParameters()) . "\n";
?>
--EXPECTF--
UPDATE user
SET type = ?
WHERE id = ?
ORDER BY name
LIMIT 1
Array
(
    [0] => author
    [1] => 2
)
