--TEST--
Query with select, group, having, order
--FILE--
<?php

$query = $db
	->from('testuser')
	->select('count(*) AS total_count')
	->groupBy(array('id', 'name'));

echo $query->getQuery() . "\n";
print_r($query->fetch());
?>
--EXPECTF--
SELECT count(*) AS total_count
FROM testuser
GROUP BY id,name
Array
(
    [total_count] => 1
)
