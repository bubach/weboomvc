--TEST--
where('NOT col', array)
--FILE--
<?php

$query = $db->from('testarticle')->where('NOT id', array(1,2));

echo $query->getQuery() . "\n";
?>
--EXPECTF--
SELECT testarticle.*
FROM testarticle
WHERE NOT id IN (1, 2)
