--TEST--
Update with zero value.
--FILE--
<?php

$db->update('testarticle')->set('content', '')->where('id', 1)->execute();
$user = $db->from('testarticle')->where('id', 1)->fetch();

echo 'ID: ' . $user['id'] . ' - content: ' . $user['content'] . "\n";
$db->update('testarticle')->set('content', 'content 1')->where('id', 1)->execute();
$user = $db->from('testarticle')->where('id', 1)->fetch();
echo 'ID: ' . $user['id'] . ' - content: ' . $user['content'] . "\n";
?>
--EXPECTF--
ID: 1 - content: 
ID: 1 - content: content 1
