--TEST--
Accept array of columns in select (no aliases)
--FILE--
<?php

$query = $db
	->from('testuser')
	->select(array('id', 'name'))
	->where('id < ?', 2);

echo $query->getQuery() . "\n";
print_r($query->getParameters());
print_r($query->fetch());
?>
--EXPECTF--
SELECT id, name
FROM testuser
WHERE id < ?
Array
(
    [0] => 2
)
Array
(
    [id] => 1
    [name] => Marek
)
