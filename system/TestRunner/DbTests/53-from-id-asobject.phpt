--TEST--
from($table, $id) as stdClass
--FILE--
<?php

$query = $db->from('testuser', 2)->asObject();

echo $query->getQuery() . "\n";
print_r($query->fetch());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE testuser.id = ?
stdClass Object
(
    [id] => 2
    [testcountry_id] => 1
    [type] => author
    [name] => Robert
)
