--TEST--
Basic operations
--FILE--
<?php
$query = $db->from('testuser')->where('id > ?', 0)->orderBy('name');
$query = $query->where('name = ?', 'Marek');
echo $query->getQuery() . "\n";
print_r($query->getParameters());
print_r($query->fetch());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE id > ?
    AND name = ?
ORDER BY name
Array
(
    [0] => 0
    [1] => Marek
)
Array
(
    [id] => 1
    [testcountry_id] => 1
    [type] => admin
    [name] => Marek
)
