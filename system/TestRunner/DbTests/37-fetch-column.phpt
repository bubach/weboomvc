--TEST--
fetch column
--FILE--
<?php

echo $db->from('testuser', 1)->fetchColumn() . "\n";
echo $db->from('testuser', 1)->fetchColumn(3) . "\n";
if ($db->from('testuser', 3)->fetchColumn() === false) echo "false\n";
if ($db->from('testuser', 3)->fetchColumn(3) === false) echo "false\n";

?>
--EXPECTF--
1
Marek
false
false
