--TEST--
Shortcuts for delete
--FILE--
<?php

$query = $db->deleteFrom('user', 1);
echo $query->getQuery() . "\n";
print_r($query->getParameters()) . "\n";

?>
--EXPECTF--
DELETE
FROM user
WHERE id = ?
Array
(
    [0] => 1
)
