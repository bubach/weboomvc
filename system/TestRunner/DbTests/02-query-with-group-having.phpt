--TEST--
Query with select, group, having, order
--FILE--
<?php
$query = $db
	->from('testuser')
	->select('type, count(id) AS type_count')
	->where('id > ?', 1)
	->groupBy('type')
	->having('type_count > ?', 1)
	->orderBy('name');

echo $query->getQuery() . "\n";
?>
--EXPECTF--
SELECT type, count(id) AS type_count
FROM testuser
WHERE id > ?
GROUP BY type
HAVING type_count > ?
ORDER BY name
