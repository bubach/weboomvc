--TEST--
where(array(...))
--FILE--
<?php
$query = $db->from('testuser')->where(array(
	'id' => 2,
	'type' => 'author',
));

echo $query->getQuery() . "\n";
print_r($query->getParameters());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE id = ?
    AND type = ?
Array
(
    [0] => 2
    [1] => author
)
