--TEST--
add FROM after DELETE if doesn't set
--FILE--
<?php

$query = $db->delete('user', 1)->from('user');
echo $query->getQuery() . "\n";
print_r($query->getParameters()) . "\n";

?>
--EXPECTF--
DELETE user
FROM user
WHERE id = ?
Array
(
    [0] => 1
)
