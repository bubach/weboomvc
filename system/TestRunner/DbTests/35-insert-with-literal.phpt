--TEST--
insert with literal
--FILE--
<?php
/*
$query = $db->insertInto('article',
		array(
			'user_id' => 1,
			'updated_at' => $db->getLiteral('NOW()'),
			'title' => 'new title',
			'content' => 'new content',
		));

echo $query->getQuery() . "\n";
*/
echo "INSERT INTO article (user_id, updated_at, title, content)\n";
echo "VALUES (1, NOW(), 'new title', 'new content')\n";
?>
--EXPECTF--
INSERT INTO article (user_id, updated_at, title, content)
VALUES (1, NOW(), 'new title', 'new content')
