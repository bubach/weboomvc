--TEST--
from($table, $id) as User class
--FILE--
<?php

class User { public $id, $testcountry_id, $type, $name; }
$query = $db->from('testuser', 2)->asObject('User');

echo $query->getQuery() . "\n";
print_r($query->fetch());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE testuser.id = ?
User Object
(
    [id] => 2
    [testcountry_id] => 1
    [type] => author
    [name] => Robert
)
