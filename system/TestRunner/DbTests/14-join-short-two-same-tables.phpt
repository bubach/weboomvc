--TEST--
join two same tables
--FILE--
<?php

$query = $db->from('article')->leftJoin('user')->leftJoin('user');
echo $query->getQuery() . "\n";

?>
--EXPECTF--
SELECT article.*
FROM article
    LEFT JOIN user ON user.id = article.user_id
