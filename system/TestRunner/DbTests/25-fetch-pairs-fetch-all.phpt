--TEST--
fetch pairs, fetch all
--FILE--
<?php

$result = $db->from('testuser')->fetchPairs('id', 'name');
print_r($result);
$result = $db->from('testuser')->fetchRows();
print_r($result);

?>
--EXPECTF--
Array
(
    [1] => Marek
    [2] => Robert
)
Array
(
    [0] => Array
        (
            [id] => 1
            [testcountry_id] => 1
            [type] => admin
            [name] => Marek
        )

    [1] => Array
        (
            [id] => 2
            [testcountry_id] => 1
            [type] => author
            [name] => Robert
        )

)
