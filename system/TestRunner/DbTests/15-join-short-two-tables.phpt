--TEST--
join two tables via difference keys
--FILE--
<?php

$query = $db->from('testcomment')->select('testcomment.*')
		->where('testcomment.id', 1)
		->leftJoin('testuser comment_author')->select('comment_author.name AS comment_name')
		->leftJoin('testarticle.testuser AS article_author')->select('article_author.name AS author_name');
echo $query->getQuery() . "\n";
$result = $query->fetch();
print_r($result);

?>
--EXPECTF--
SELECT testcomment.*, comment_author.name AS comment_name, article_author.name AS author_name
FROM testcomment
    LEFT JOIN testuser AS comment_author ON comment_author.id = testcomment.testuser_id
    LEFT JOIN testarticle ON testarticle.id = testcomment.testarticle_id
    LEFT JOIN testuser AS article_author ON article_author.id = testarticle.testuser_id
WHERE testcomment.id = ?
Array
(
    [id] => 1
    [testarticle_id] => 1
    [testuser_id] => 2
    [content] => comment 1.1
    [comment_name] => Robert
    [author_name] => Marek
)
