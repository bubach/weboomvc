--TEST--
short join back reference
--FILE--
<?php

$query = $db->from('testuser')->innerJoin('testarticle:');
echo $query->getQuery() . "\n";
$query = $db->from('testuser')->innerJoin('testarticle: with_articles');
echo $query->getQuery() . "\n";
$query = $db->from('testuser')->innerJoin('testarticle: AS with_articles');
echo $query->getQuery() . "\n";
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
    INNER JOIN testarticle ON testarticle.testuser_id = testuser.id
SELECT testuser.*
FROM testuser
    INNER JOIN testarticle AS with_articles ON with_articles.testuser_id = testuser.id
SELECT testuser.*
FROM testuser
    INNER JOIN testarticle AS with_articles ON with_articles.testuser_id = testuser.id