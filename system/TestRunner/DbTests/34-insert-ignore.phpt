--TEST--
insert ignore
--FILE--
<?php

$query = $db->insertInto('article',
		array(
			'user_id' => 1,
			'title' => 'new title',
			'content' => 'new content',
		))->ignore();

echo $query->getQuery() . "\n";

?>
--EXPECTF--
INSERT IGNORE INTO article (user_id, title, content)
VALUES (?, ?, ?)
