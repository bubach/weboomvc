--TEST--
from($table, $id)
--FILE--
<?php
$query = $db->from('testuser', 2);

echo $query->getQuery() . "\n";
print_r($query->getParameters());
?>
--EXPECTF--
SELECT testuser.*
FROM testuser
WHERE testuser.id = ?
Array
(
    [0] => 2
)
