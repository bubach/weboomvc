--TEST--
join in where
--FILE--
<?php

$query = $db->from('testarticle')->groupBy('testuser.type')
	->select('testuser.type, count(testarticle.id) as article_count');
echo $query->getQuery() . "\n";
$result = $query->fetchRows();
print_r($result);
?>
--EXPECTF--
SELECT testuser.type, count(testarticle.id) as article_count
FROM testarticle
    LEFT JOIN testuser ON testuser.id = testarticle.testuser_id
GROUP BY testuser.type
Array
(
    [0] => Array
        (
            [type] => admin
            [article_count] => 2
        )

    [1] => Array
        (
            [type] => author
            [article_count] => 1
        )

)
