<?php
/**
 * Default controller for running tests
 *
 * @package     TestRunner
 * @author      Christoffer Bubach
 * @class       TestRunner_Controller_Default
 */
class TestRunner_Controller_Default extends Core_Controller_Base
{

    public function index()
    {
        $db       = wmvc::app()->getDb();
        $verbose  = true;
        $tests    = glob(dirname(__FILE__) . "/../*Tests/*.phpt", GLOB_NOSORT);
        $response = $this->getResponse();
        $regex    = "~^--TEST--\n(.*?)\n(?:--SKIPIF--\n(.*\n)?)?--FILE--\n(.*\n)?--EXPECTF--\n(.*)~s";

        natsort($tests);

        foreach ($tests as $filename) {
            ob_start();
            include $filename;
            $result = str_replace("\r\n", "\n", ob_get_clean());

            if (!preg_match($regex, $result, $match)) {
                $response->appendBody("wrong test in ".basename($filename)."\n<br />");
            } elseif ($match[2]) {
                $response->appendBody("skipped ".basename($filename)." ($match[1]): $match[2]\n<br />");
            } elseif (trim($match[3]) !== trim($match[4])) {
                $error = true;
                $response->appendBody("failed ".basename($filename)." ($match[1])\n<br />");
                if ($verbose) {
                    $response->appendBody("--expected result--\n");
                    ob_start();
                    var_dump(trim($match[4]));
                    $response->appendBody(ob_get_clean());
                    $response->appendBody("--actual result--\n");
                    ob_start();
                    var_dump(trim($match[3]));
                    $response->appendBody(ob_get_clean());
                    $response->appendBody("--end--\n");
                    break;
                }
            }
        }

        $response->appendBody("Tests completed in {{time}} second using {{memory}}");
    }

}