<?php
/**
 * Form_Model_ErrorStore_SessionError
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_ErrorStore_SessionError
{

    /**
     * @var Core_Plugin_Session
     */
    private $_session;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_session = wmvc::app()->getObject('Core_Plugin_Session');
    }

    /**
     * @param  $key
     * @return bool
     */
    public function hasError($key)
    {
        if (!$this->hasErrors()) {
            return false;
        }

        $key = $this->transformKey($key);
        return $this->getErrors()->has($key);
    }

    /**
     * @param  $key
     * @return null
     */
    public function getError($key)
    {
        if (!$this->hasError($key)) {
            return null;
        }

        $key = $this->transformKey($key);
        return $this->getErrors()->first($key);
    }

    /**
     * @return bool
     */
    protected function hasErrors()
    {
        return (bool)$this->_session->get('errors');
    }

    /**
     * @return mixed|null
     */
    protected function getErrors()
    {
        return $this->hasErrors() ? $this->_session->get('errors') : null;
    }

    /**
     * @param  $key
     * @return mixed
     */
    protected function transformKey($key)
    {
        return str_replace(array('.', '[]', '[', ']'), array('_', '', '.', ''), $key);
    }

}
