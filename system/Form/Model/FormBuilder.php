<?php
/**
 * Form_Model_FormBuilder
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_FormBuilder extends Core_Model_Base
{

    /**
     * @var Form_Model_OldInput_SessionOld
     */
    protected $_oldInput;

    /**
     * @var Form_Model_ErrorStore_SessionError
     */
    protected $_errorStore;

    /**
     * @var
     */
    protected $_csrfToken;

    /**
     * @var
     */
    protected $_model;

    /**
     * @var
     */
    protected $_elementCount = 0;

    /**
     * @param $oldInputProvider
     */
    public function setOldInputProvider($oldInputProvider)
    {
        $this->_oldInput = $oldInputProvider;
    }

    /**
     * @param $errorStore
     */
    public function setErrorStore($errorStore)
    {
        $this->_errorStore = $errorStore;
    }

    /**
     * Set CSRF form-token
     *
     * @param $token
     */
    public function setToken($token)
    {
        $this->_csrfToken = $token;
    }

    /**
     * Open form
     *
     * @return object
     */
    public function open()
    {
        $this->_elementCount++;
        $open = $this->getObject(
            'Form_Model_Elements_FormOpen',
            'FormOpen_'.$this->_elementCount
        );

        if ($this->hasToken()) {
            $open->token($this->_csrfToken);
        }

        return $open;
    }

    /**
     * Check for form token
     *
     * @return bool
     */
    protected function hasToken()
    {
        return isset($this->_csrfToken);
    }

    /**
     * Close form
     *
     * @return string
     */
    public function close()
    {
        $this->unbindModel();
        return '</form>';
    }

    /**
     * Get text input
     *
     * @param  $name
     * @return Form_Model_Elements_Input
     */
    public function text($name)
    {
        $this->_elementCount++;
        $text = $this->getObject(
            'Form_Model_Elements_Input',
            'FormText_'.$this->_elementCount,
            $name
        );
        $text->setAttribute('type', 'text', true);

        if (!is_null($value = $this->getValueFor($name))) {
            $text->value($value);
        }

        return $text;
    }

    /**
     * Get date input
     *
     * @param  $name
     * @return object
     */
    public function date($name)
    {
        $this->_elementCount++;
        $date = $this->getObject(
            'Form_Model_Elements_Input',
            'FormDate_'.$this->_elementCount,
            $name
        );
        $date->setAttribute('type', 'date', true);

        if (!is_null($value = $this->getValueFor($name))) {
            $date->value($value);
        }

        return $date;
    }

    /**
     * Get email input
     *
     * @param  $name
     * @return object
     */
    public function email($name)
    {
        $this->_elementCount++;
        $email = $this->getObject(
            'Form_Model_Elements_Input',
            'FormEmail_'.$this->_elementCount,
            $name
        );
        $email->setAttribute('type', 'email', true);

        if (!is_null($value = $this->getValueFor($name))) {
            $email->value($value);
        }

        return $email;
    }

    /**
     * Get hidden input
     *
     * @param $name
     * @return object
     */
    public function hidden($name)
    {
        $this->_elementCount++;
        $hidden = $this->getObject(
            'Form_Model_Elements_Input',
            'FormHidden_'.$this->_elementCount,
            $name
        );
        $hidden->setAttribute('type', 'hidden', true);

        if (!is_null($value = $this->getValueFor($name))) {
            $hidden->value($value);
        }

        return $hidden;
    }

    /**
     * Get textarea
     *
     * @param  $name
     * @return object
     */
    public function textarea($name)
    {
        $this->_elementCount++;
        $textarea = $this->getObject(
            'Form_Model_Elements_TextArea',
            'FormTextarea_'.$this->_elementCount,
            $name
        );

        if (!is_null($value = $this->getValueFor($name))) {
            $textarea->value($value);
        }

        return $textarea;
    }

    /**
     * Get password input
     *
     * @param  $name
     * @return object
     */
    public function password($name)
    {
        $this->_elementCount++;
        $pwd = $this->getObject(
            'Form_Model_Elements_Input',
            'FormPwd_'.$this->_elementCount,
            $name
        );
        return $pwd->setAttribute('type', 'password', true);
    }

    /**
     * Get checkbox
     *
     * @param  $name
     * @param  int $value
     * @return object
     */
    public function checkbox($name, $value = 1)
    {
        $this->_elementCount++;
        $checkbox = $this->getObject(
            'Form_Model_Elements_Checkbox',
            'FormCheckbox_'.$this->_elementCount,
            array(
                $name,
                $value
            )
        );
        $oldValue = $this->getValueFor($name);

        if ($value == $oldValue) {
            $checkbox->check();
        }

        return $checkbox;
    }

    /**
     * Get radio button
     *
     * @param  $name
     * @param  $value
     * @return object
     */
    public function radio($name, $value = null)
    {
        $this->_elementCount++;
        $value = is_null($value) ? $name : $value;
        $radio = $this->getObject(
            'Form_Model_Elements_RadioButton',
            'FormRadio_'.$this->_elementCount,
            array(
                $name,
                $value
            )
        );
        $oldValue = $this->getValueFor($name);

        if ($value == $oldValue) {
            $radio->check();
        }

        return $radio;
    }

    /**
     * Get button
     *
     * @param  $value
     * @param  $name
     * @return object
     */
    public function button($name, $value)
    {
        $this->_elementCount++;
        return $this->getObject(
            'Form_Model_Elements_Button',
            'FormButton_'.$this->_elementCount,
            array(
                $name,
                $value
            )
        );
    }

    /**
     * Get submit
     *
     * @param  string $value
     * @return object
     */
    public function submit($value = 'Submit')
    {
        $this->_elementCount++;
        $submit = $this->getObject(
            'Form_Model_Elements_Button',
            'FormSubmit_'.$this->_elementCount,
            array(
                '',
                $value,
            )
        );
        $submit->setAttribute('type', 'submit', true);

        return $submit;
    }

    /**
     * Get select
     *
     * @param  $name
     * @param  array $options
     * @return object
     */
    public function select($name, $options = array())
    {
        $this->_elementCount++;
        $select = $this->getObject(
            'Form_Model_Elements_Select',
            'FormSelect_'.$this->_elementCount,
            array(
                $name,
                $options
            )
        );
        $selected = $this->getValueFor($name);
        $select->select($selected);

        return $select;
    }

    /**
     * Get label
     *
     * @param  $label
     * @return object
     */
    public function label($label)
    {
        $this->_elementCount++;

        return $this->getObject(
            'Form_Model_Elements_Label',
            'FormLabel_'.$this->_elementCount,
            $label
        );
    }

    /**
     * Get file uploader input
     *
     * @param  $name
     * @return object
     */
    public function file($name)
    {
        $this->_elementCount++;
        $file = $this->getObject(
            'Form_Model_Elements_Input',
            'FormFile_'.$this->_elementCount,
            $name
        );
        return $file->setAttribute('type', 'file', true);
    }

    /**
     * Get token
     *
     * @return object
     */
    public function token()
    {
        $token = $this->hidden('_token');

        if (isset($this->_csrfToken)) {
            $token->value($this->_csrfToken);
        }

        return $token;
    }

    /**
     * Check for form errors
     *
     * @param $name
     * @return bool
     */
    public function hasError($name)
    {
        if (!isset($this->_errorStore)) {
            return false;
        }
        return $this->_errorStore->hasError($name);
    }

    /**
     * Get a form error
     *
     * @param  $name
     * @param  null $format
     * @return mixed|null|string
     */
    public function getError($name, $format = null)
    {
        if (! isset($this->_errorStore)) {
            return null;
        }

        if (!$this->hasError($name)) {
            return '';
        }
        $message = $this->_errorStore->getError($name);

        if ($format) {
            $message = str_replace(':message', $message, $format);
        }

        return $message;
    }

    /**
     * Bind model to form
     *
     * @param $model
     */
    public function bind($model)
    {
        $this->_model = is_array($model) ? (new Core_Object($model)) : $model;
    }

    /**
     * Get a specific value from old input or model
     *
     * @param  $name
     * @return null|string
     */
    public function getValueFor($name)
    {
        if ($this->hasOldInput()) {
            return $this->getOldInput($name);
        }

        if ($this->hasModelValue($name)) {
            return $this->getModelValue($name);
        }

        return null;
    }

    /**
     * Does the form has old input
     *
     * @return bool
     */
    protected function hasOldInput()
    {
        if (! isset($this->_oldInput)) {
            return false;
        }

        return $this->_oldInput->hasOldInput();
    }

    /**
     * Get old input
     *
     * @param  $name
     * @return string
     */
    protected function getOldInput($name)
    {
        return $this->escape($this->_oldInput->getOldInput($name));
    }

    /**
     * Does the model has a value
     *
     * @param  $name
     * @return bool
     */
    protected function hasModelValue($name)
    {
        if (!isset($this->_model)) {
            return false;
        }
        $value = $this->_model->getData($name);
        return empty($value);
    }

    /**
     * Get model value
     *
     * @param  $name
     * @return string
     */
    protected function getModelValue($name)
    {
        return $this->escape($this->_model->getData($name));
    }

    /**
     * Escape value
     *
     * @param  $value
     * @return string
     */
    protected function escape($value)
    {
        if (!is_string($value)) {
            return $value;
        }
        return htmlentities($value, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Unbind any model connected to this form
     *
     * @return $this
     */
    protected function unbindModel()
    {
        $this->_model = null;
        return $this;
    }

    /**
     * Get select input with months
     *
     * @param  $name
     * @return object
     */
    public function selectMonth($name)
    {
        $options = array(
            "1"  => "January",
            "2"  => "February",
            "3"  => "March",
            "4"  => "April",
            "5"  => "May",
            "6"  => "June",
            "7"  => "July",
            "8"  => "August",
            "9"  => "September",
            "10" => "October",
            "11" => "November",
            "12" => "December",
        );

        return $this->select($name, $options);
    }
}
