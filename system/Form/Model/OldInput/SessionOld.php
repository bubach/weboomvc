<?php
/**
 * Form_Model_OldInput_SessionOld
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_OldInput_SessionOld extends Core_Model_Base
{

    /**
     * @var Core_Plugin_Session
     */
    private $_session;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_session = $this->getObject("Core_Plugin_Session");
    }

    /**
     * @return bool
     */
    public function hasOldInput()
    {
        return ($this->_session->get('_old_input')) ? true : false ;
    }

    /**
     * @param  $key
     * @return mixed
     */
    public function getOldInput($key)
    {
        $oldInput = $this->_session->get('_old_input');
        return is_array($oldInput) ? $oldInput[$this->transformKey($key)] : $oldInput;
    }

    /**
     * @param  $key
     * @return mixed
     */
    protected function transformKey($key)
    {
        return str_replace(array('.', '[]', '[', ']'), array('_', '', '.', ''), $key);
    }
}
