<?php
/**
 * Form_Model_Elements_FormOpen
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_FormOpen extends Form_Model_Elements_Base
{

    /**
     * @var array Form attributes
     */
    protected $_attributes = array(
        'method' => 'post',
        'action' => '',
    );

    /**
     * @var mixed Hidden input if CSRF-token is used
     */
    protected $_token;

    /**
     * @var mixed Hidden input if a hidden method is used
     */
    protected $_hiddenMethod;

    /**
     * Render form-tag as HTML
     *
     * @return mixed|string
     */
    public function render()
    {
        $result  = '<form';
        $result .= $this->renderAttributes();
        $result .= '>';

        if ($this->hasToken() && ($this->attributes['method'] !== 'get')) {
            $result .= $this->_token->render();
        }
        if ($this->hasHiddenMethod()) {
            $result .= $this->_hiddenMethod->render();
        }
        return $result;
    }

    /**
     * Return true if form has any CSRF-token set
     *
     * @return bool
     */
    protected function hasToken()
    {
        return isset($this->_token);
    }

    /**
     * Return true if form has hidden method set
     *
     * @return bool
     */
    protected function hasHiddenMethod()
    {
        return isset($this->_hiddenMethod);
    }

    /**
     * Set method to post
     *
     * @return $this
     */
    public function post()
    {
        $this->setMethod('post');
        return $this;
    }

    /**
     * Set method to get
     *
     * @return $this
     */
    public function get()
    {
        $this->setMethod('get');
        return $this;
    }

    /**
     * Set method to put
     *
     * @return $this
     */
    public function put()
    {
        return $this->setHiddenMethod('put');
    }

    /**
     * Set method to patch
     *
     * @return $this
     */
    public function patch()
    {
        return $this->setHiddenMethod('patch');
    }

    /**
     * Set method to delete
     *
     * @return $this
     */
    public function delete()
    {
        return $this->setHiddenMethod('delete');
    }

    /**
     * Set CSRF-token as hidden input
     *
     * @param $token
     * @return $this
     */
    public function token($token)
    {
        $this->_token = $this->getObject('Form_Model_Elements_Hidden', null, '_token');
        $this->_token->value($token);
        return $this;
    }

    /**
     * Set method as hidden input
     *
     * @param  $method
     * @return $this
     */
    protected function setHiddenMethod($method)
    {
        $this->setMethod('post');
        $this->_hiddenMethod = $this->getObject('Form_Model_Elements_Hidden', null, '_method');
        $this->_hiddenMethod->value($method);
        return $this;
    }

    /**
     * Set form method
     *
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->setAttribute('method', $method);
        return $this;
    }

    /**
     * Set form action
     *
     * @param  $action
     * @return $this
     */
    public function action($action)
    {
        $this->setAttribute('action', $action);
        return $this;
    }

    /**
     * Set encoding type
     *
     * @param $type
     * @return $this
     */
    public function encodingType($type)
    {
        $this->setAttribute('enctype', $type);
        return $this;
    }

    /**
     * Set as multipart form data (file uploading)
     *
     * @return $this
     */
    public function multipart()
    {
        return $this->encodingType('multipart/form-data');
    }
}
