<?php
/**
 * Form_Model_Elements_Input
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_Input extends Form_Model_Elements_Base
{

    /**
     * @return mixed|string
     */
    public function render()
    {
        $result  = '<input';
        $result .= $this->renderAttributes();
        $result .= '>';

        return $result;
    }

    /**
     * @param $value
     * @return $this
     */
    public function value($value)
    {
        $this->setValue($value);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    protected function setValue($value)
    {
        $this->setAttribute('value', $value);
        return $this;
    }

    /**
     * @param $placeholder
     * @return $this
     */
    public function placeholder($placeholder)
    {
        $this->setAttribute('placeholder', $placeholder);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function defaultValue($value)
    {
        if (!$this->hasValue()) {
            $this->setValue($value);
        }
        return $this;
    }

    /**
     * @return bool
     */
    protected function hasValue()
    {
        return isset($this->_attributes['value']);
    }
}
