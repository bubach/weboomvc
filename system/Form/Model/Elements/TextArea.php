<?php
/**
 * Form_Model_Elements_TextArea
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_TextArea extends Form_Model_Elements_Base {

    /**
     * @var array
     */
    protected $_attributes = array(
        'name' => '',
        'rows' => 10,
        'cols' => 50,
    );

    /**
     * @var
     */
    protected $_value;

    /**
     * @return mixed|string
     */
    public function render() {
        $result = '<textarea';
        $result .= $this->renderAttributes();
        $result .= '>';
        $result .= $this->_value;
        $result .= '</textarea>';

        return $result;
    }

    /**
     * @param $rows
     * @return $this
     */
    public function rows($rows) {
        $this->setAttribute('rows', $rows);
        return $this;
    }

    /**
     * @param $cols
     * @return $this
     */
    public function cols($cols) {
        $this->setAttribute('cols', $cols);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function value($value) {
        $this->_value = $value;
        return $this;
    }

    /**
     * @param $placeholder
     * @return $this
     */
    public function placeholder($placeholder) {
        $this->setAttribute('placeholder', $placeholder);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function defaultValue($value) {
        if (! $this->hasValue()) {
            $this->value($value);
        }
        return $this;
    }

    /**
     * @return bool
     */
    protected function hasValue() {
        return isset($this->_value);
    }

}
