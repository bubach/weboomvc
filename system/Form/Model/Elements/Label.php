<?php
/**
 * Form_Model_Elements_Label
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_Label extends Form_Model_Elements_Base {

    /**
     * @var
     */
    protected $_element;

    /**
     * @var
     */
    protected $_labelBefore;

    /**
     * @var
     */
    protected $_label;

    /**
     * @param null $label
     */
    public function __construct($label = null)
    {
        $this->_label = $label;
    }

    /**
     * @return mixed|string
     */
    public function render()
    {
        $result = '<label';
        $result .= $this->renderAttributes();
        $result .= '>';

        if ($this->_labelBefore) {
            $result .= $this->_label;
        }
        $result .= $this->renderElement();

        if (!$this->_labelBefore) {
            $result .= $this->_label;
        }

        $result .= '</label>';
        return $result;
    }

    /**
     * @param $name
     * @return $this
     */
    public function forId($name)
    {
        $this->setAttribute('for', $name);
        return $this;
    }

    /**
     * @param Form_Model_Elements_Base $element
     * @return $this
     */
    public function before(Form_Model_Elements_Base $element)
    {
        $this->_element = $element;
        $this->_labelBefore = true;
        return $this;
    }

    /**
     * @param Form_Model_Elements_Base $element
     * @return $this
     */
    public function after(Form_Model_Elements_Base $element)
    {
        $this->_element = $element;
        $this->_labelBefore = false;
        return $this;
    }

    /**
     * @return string
     */
    protected function renderElement()
    {
        if (!$this->_element) {
            return '';
        }
        return $this->_element->render();
    }

    /**
     * @return mixed
     */
    public function getControl()
    {
        return $this->_element;
    }
}
