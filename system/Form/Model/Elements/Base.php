<?php
/**
 * Form_Model_Elements_Base
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

abstract class Form_Model_Elements_Base
{

    /**
     * @var array
     */
    protected $_attributes = array();

    /**
     * Constructor
     *
     * @param $name
     */
    public function __construct($name = null)
    {
        if (!empty($name)) {
            $this->setName($name);
        }
    }

    /**
     * Set element attribute
     *
     * @param  $attribute
     * @param  null $value
     * @param  bool $first
     * @return $this
     */
    public function setAttribute($attribute, $value = null, $first = false)
    {
        if (is_null($value)) {
            return $this;
        }

        if ($first) {
            $this->_attributes = array($attribute => $value) + $this->_attributes;
        } else {
            $this->_attributes[$attribute] = $value;
        }
        return $this;
    }

    /**
     * Remove element attribute
     *
     * @param $attribute
     */
    protected function removeAttribute($attribute)
    {
        unset($this->_attributes[$attribute]);
    }

    /**
     * Get element attribute
     *
     * @param $attribute
     * @return mixed
     */
    public function getAttribute($attribute)
    {
        return $this->_attributes[$attribute];
    }

    /**
     * Set data attribute
     *
     * @param $attribute
     * @param $value
     * @return $this
     */
    public function data($attribute, $value)
    {
        $this->setAttribute('data-'.$attribute, $value);
        return $this;
    }

    /**
     * Set attribute shorthand
     *
     * @param $attribute
     * @param $value
     * @return $this
     */
    public function attribute($attribute, $value)
    {
        $this->setAttribute($attribute, $value);
        return $this;
    }

    /**
     * Remove attribute
     *
     * @param $attribute
     * @return $this
     */
    public function clear($attribute)
    {
        if (!isset($this->_attributes[$attribute])) {
            return $this;
        }
        $this->removeAttribute($attribute);
        return $this;
    }

    /**
     * Add element class
     *
     * @param $class
     * @return $this
     */
    public function addClass($class)
    {
        if (isset($this->_attributes['class'])) {
            $class = $this->_attributes['class'] . ' ' . $class;
        }
        $this->setAttribute('class', $class);
        return $this;
    }

    /**
     * Remove element class
     *
     * @param $class
     * @return $this
     */
    public function removeClass($class)
    {
        if (! isset($this->_attributes['class'])) {
            return $this;
        }

        $class = trim(str_replace($class, '', $this->_attributes['class']));
        if ($class == '') {
            $this->removeAttribute('class');
            return $this;
        }

        return $this->setAttribute('class', $class);

    }

    /**
     * Set element Id
     *
     * @param $id
     * @return $this
     */
    public function id($id)
    {
        $this->setId($id);
        return $this;
    }

    /**
     * Set element Id
     *
     * @param $id
     */
    protected function setId($id)
    {
        $this->setAttribute('id', $id);
    }

    /**
     * Set element name
     *
     * @param $name
     */
    protected function setName($name)
    {
        $this->setAttribute('name', $name);
    }

    /**
     * Set element required attribute
     *
     * @return $this
     */
    public function required()
    {
        $this->setAttribute('required', 'required');
        return $this;
    }

    /**
     * Remove element required attribute
     *
     * @return $this
     */
    public function optional()
    {
        $this->removeAttribute('required');
        return $this;
    }

    /**
     * Set element disabled attribute
     *
     * @return $this
     */
    public function disable()
    {
        $this->setAttribute('disabled', 'disabled');
        return $this;
    }

    /**
     * Remove element disabled attribute
     *
     * @return $this
     */
    public function enable()
    {
        $this->removeAttribute('disabled');
        return $this;
    }

    /**
     * Set element autofocus attribute
     *
     * @return $this
     */
    public function autofocus()
    {
        $this->setAttribute('autofocus', 'autofocus');
        return $this;
    }

    /**
     * Remove element autofocus attribute
     *
     * @return $this
     */
    public function unfocus()
    {
        $this->removeAttribute('autofocus');
        return $this;
    }

    /**
     * Render the element HTML
     *
     * @return mixed
     */
    abstract public function render();

    /**
     * String output calls element HTML rendering
     *
     * @return mixed
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Render element's attributes
     *
     * @return string
     */
    protected function renderAttributes()
    {
        $result = '';

        foreach ($this->_attributes as $attribute => $value) {
            $result .= " {$attribute}=\"{$value}\"";
        }

        return $result;
    }

    /**
     * Magic setter for attributes
     *
     * @param $method
     * @param $params
     * @return $this
     */
    public function __call($method, $params)
    {
        $params = count($params) ? $params : array($method);
        $params = array_merge(array($method), $params);
        call_user_func_array(array($this, 'attribute'), $params);
        return $this;
    }
}
