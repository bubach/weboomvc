<?php
/**
 * Form_Model_Elements_Select
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_Select extends Form_Model_Elements_Base {

    /**
     * @var
     */
    protected $_options;

    /**
     * @var
     */
    protected $_selected;

    /**
     * @param array $name
     * @param array $options
     */
    public function __construct($name = array(), $options = array())
    {
        $this->setName($name);
        $this->setOptions($options);
    }

    /**
     * @param $option
     * @return $this
     */
    public function select($option)
    {
        $this->_selected = $option;
        return $this;
    }

    /**
     * @param $options
     */
    protected function setOptions($options)
    {
        $this->_options = $options;
    }

    /**
     * @param $options
     * @return $this
     */
    public function options($options)
    {
        $this->setOptions($options);
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function render()
    {
        $result = '<select';
        $result .= $this->renderAttributes();
        $result .= '>';
        $result .= $this->renderOptions();
        $result .= '</select>';

        return $result;
    }

    /**
     * @return string
     */
    protected function renderOptions()
    {
        $result = '';

        foreach ($this->_options as $value => $label) {
            if (is_array($label)) {
                $result .= $this->renderOptGroup($value, $label);
                continue;
            }
            $result .= $this->renderOption($value, $label);
        }
        return $result;
    }

    /**
     * @param $label
     * @param $options
     * @return string
     */
    protected function renderOptGroup($label, $options)
    {
        $result = "<optgroup label=\"{$label}\">";
        foreach ($options as $value => $label) {
            $result .= $this->renderOption($value, $label);
        }
        $result .= "</optgroup>";
        return $result;
    }

    /**
     * @param $value
     * @param $label
     * @return string
     */
    protected function renderOption($value, $label)
    {
        $option = '<option ';
        $option .= 'value="' . $value . '"';
        $option .= $this->isSelected($value) ? ' selected' : '';
        $option .= '>';
        $option .= $label;
        $option .= '</option>';
        return $option;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isSelected($value)
    {
        return in_array($value, (array) $this->_selected);
    }

    /**
     * @param $value
     * @param $label
     * @return $this
     */
    public function addOption($value, $label)
    {
        $this->_options[$value] = $label;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function defaultValue($value)
    {
        if (isset($this->_selected)) {
            return $this;
        }

        $this->select($value);
        return $this;
    }

    /**
     * @return $this
     */
    public function multiple()
    {
        $name = $this->_attributes['name'];
        if (substr($name, -2) != '[]') {
            $name .= '[]';
        }

        $this->setName($name);
        $this->setAttribute('multiple', 'multiple');
        return $this;
    }
}
