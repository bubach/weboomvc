<?php
/**
 * Form_Model_Elements_Checkbox
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_Checkbox extends Form_Model_Elements_Input {

    /**
     * @var array
     */
    protected $_attributes = array(
        'type' => 'checkbox',
    );

    /**
     * @var
     */
    protected $_checked;

    /**
     * @param string $name
     * @param string $value
     */
    public function __construct($name = '', $value = '')
    {
        parent::__construct($name);
        $this->setValue($value);
    }

    /**
     * @return $this
     */
    public function defaultToChecked() {
        if (! isset($this->_checked)) {
            $this->check();
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function defaultToUnchecked() {
        if (! isset($this->_checked)) {
            $this->uncheck();
        }
        return $this;
    }

    /**
     * @param $state
     * @return $this
     */
    public function defaultCheckedState($state) {
        $state ? $this->defaultToChecked() : $this->defaultToUnchecked();
        return $this;
    }

    /**
     * @return $this
     */
    public function check() {
        $this->setChecked(true);
        return $this;
    }

    /**
     * @return $this
     */
    public function uncheck() {
        $this->setChecked(false);
        return $this;
    }

    /**
     * @param bool $checked
     */
    protected function setChecked($checked = true) {
        $this->_checked = $checked;
        $this->removeAttribute('checked');

        if ($checked) {
            $this->setAttribute('checked', 'checked');
        }
    }

}
