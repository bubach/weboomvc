<?php
/**
 * Form_Model_Elements_RadioButton
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_RadioButton extends Form_Model_Elements_Checkbox {

    /**
     * @var array
     */
    protected $_attributes = array(
        'type' => 'radio',
    );

    /**
     * @param string $name
     * @param null   $value
     */
    public function __construct($name = '', $value = null)
    {
        parent::__construct($name);

        if (is_null($value)) {
            $value = $name;
        }
        $this->setValue($value);
    }
}
