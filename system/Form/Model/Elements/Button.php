<?php
/**
 * Form_Model_Elements_Button
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Form_Model_Elements_Button extends Form_Model_Elements_Base {

    /**
     * @var array
     */
    protected $_attributes = array(
        'type' => 'button',
    );

    /**
     * @var
     */
    protected $_value;

    /**
     * @param string $name
     * @param string $value
     */
    public function __construct($name = '', $value = '')
    {
        parent::__construct($name);
        $this->value($value);
    }

    /**
     * @return string
     */
    public function render()
    {
        return sprintf(
            '<button%s>%s</button>',
            $this->renderAttributes(),
            $this->_value
        );
    }

    /**
     * @param $value
     */
    public function value($value)
    {
        $this->_value = $value;
    }
}
