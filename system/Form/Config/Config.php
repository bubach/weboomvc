<?php
/**
 * Config for automated forms module
 *
 * @package     Form
 * @author      Christoffer Bubach
 */
return array (
    'Modules' => array (
        'System' => array (
            'Form' => array (
                'Config' => array (
                    'version' => '0.0.1',
                    'active' => true,
                    'setup' => false,
                ),
                'Model' => array (),
                'View' => array (),
                'Controller' => array (),
                'Plugins' => array (),
            ),
        ),
    ),
);