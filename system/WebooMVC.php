<?php
/***
 * Name:       WebooMVC
 * About:      An MVC application framework for PHP
 * Copyright:  (C) 2009-2017, All rights reserved.
 * Author:     Christoffer Bubach
 * License:    MIT, see included license file
 ***/

/**
 * WMVC
 *
 * main object class
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class wmvc
{

    /** @var wmvc[] */
    private static $_instances = array();

    /** @var string */
    private static $_defaultName = 'app';

    /**
     * object handles
     */
    public $config, $load, $profiler, $event, $request, $controller;

    /**
     * class constructor
     *
     * @access  private
     */
    private function __construct()
    {
        $this->checkRequirements();
    }

    /**
     * Get the wmvc object instance
     *
     * @param $name
     * @return  wmvc|null $instance
     */
    public static function app($name = null)
    {
        $name = (empty($name)) ? self::$_defaultName : $name;

        if (!isset(self::$_instances[$name])) {
            self::$_instances[$name] = new wmvc();
        }

        return self::$_instances[$name];
    }

    /**
     * @param $name
     */
    public static function setDefaultAppName($name) {
        self::$_defaultName = $name;
    }

    /**
     * main method of execution
     *
     * @access  public
     */
    public function run()
    {
        // minimum to load rest
        $this->setupAutoload();
        $this->setupErrorHandling();
        $this->config = new Core_Plugin_Config;
        $this->load   = new Core_Plugin_Load;

        // profiling set asap
        $this->getObject('Core_Plugin_Profiling', 'profiler');
        $this->profiler->initProfiling();

        // load events & setup classes
        $this->getObject('Core_Plugin_Event', 'event');
        $this->getObject('Core_Plugin_Setup', 'setup');

        // our very first event
        $this->triggerEvent('wmvc_run_start', array('eventObject' => $this));

        // setup Request & Routing
        $this->getObject('Core_Plugin_Request', 'request');

        // setup & run controller->action
        $this->getRequest()->dispatchRequest();
        $this->triggerEvent('wmvc_run_controller', array('eventObject' => $this));
        $this->controller->{$this->action}();

        // let profiling & events manipulate body before response
        $this->profiler->stopProfiling();
        $this->triggerEvent('wmvc_run_response', array('eventObject' => $this));

        // output the final product!
        $this->controller->getResponse()->sendResponse();
        $this->triggerEvent('wmvc_run_end', array('eventObject' => $this));
    }

    public function checkRequirements() {
        if (!defined('WMVC_VERSION')) {
            define('WMVC_VERSION', '1.0.0');
        }
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
        if (!defined('WMVC_BASEDIR')) {
            define('WMVC_BASEDIR', dirname(__FILE__) . DS . '..' . DS);
        }
        if (!defined('WMVC_APPDIR')) {
            define('WMVC_APPDIR', WMVC_BASEDIR . 'app' . DS);
        }
        if (!defined('WMVC_COMMONDIR')) {
            define('WMVC_COMMONDIR', WMVC_BASEDIR . 'common' . DS);
        }
        if (!defined('WMVC_SYSTEMDIR')) {
            define('WMVC_SYSTEMDIR', WMVC_BASEDIR . 'system' . DS);
        }
        if (!defined('WMVC_FILESDIR')) {
            define('WMVC_FILESDIR', WMVC_BASEDIR . 'files' . DS);
        }

        //TODO, check PHP version for ex.
    }

    /**
     * Setup PRS-0 autoloading
     *
     * @return $this
     */
    public function setupAutoload()
    {
        set_include_path(
            get_include_path() .
            PATH_SEPARATOR .
            $this->getAppPaths()
        );

        spl_autoload_extensions('.php,.inc');

        spl_autoload_register(
            function ($class) {
                $file = preg_replace('#\\\|_(?!.+\\\)#', '/', $class) . '.php';

                if (stream_resolve_include_path($file)) {
                    require $file;
                } else {
                    throw new Exception('Error loading class: ' . $class, 1);
                }
            }
        );

        return $this;
    }

    /**
     * WMVC's fallback path order
     *
     * @return string
     */
    public function getAppPaths()
    {
        return WMVC_APPDIR .
            PATH_SEPARATOR . WMVC_COMMONDIR .
            PATH_SEPARATOR . WMVC_SYSTEMDIR
        ;
    }

    /**
     * setup error handling for wmvc
     *
     * @access  public
     */
    public function setupErrorHandling()
    {
        if (defined('WMVC_ERROR_HANDLING') && WMVC_ERROR_HANDLING == 1) {
            set_exception_handler(array('Core_Plugin_ExceptionHandler', 'handleException'));
            set_error_handler(array('Core_Plugin_ExceptionHandler', 'handleError'));
        }
    }

    /**
     * get config value based on path
     *
     * @param string $path
     * @return mixed
     */
    public function getConfig($path = '')
    {
        return $this->config->get($path);
    }

    /**
     * set config value based on path
     *
     * @param string $path
     * @param $value
     * @return bool
     */
    public function setConfig($path, $value)
    {
        return $this->config->set($path, $value);
    }

    /**
     * Get database connection object
     *
     * @param string $name
     * @return mixed
     */
    public function getDb($name = 'default')
    {
        return $this->load->database($name);
    }

    /**
     * TODO: replace this & others with static access-points,
     * remove service-binding to wmvc, use "Load" as container.
     *
     * Shortcut to get class instances. Will default to
     * singletons unless a new unique alias is requested.
     *
     * @param $className
     * @param string|null $alias
     * @param mixed $params
     * @return object
     */
    public function getObject($className, $alias = null, $params = null)
    {
        return $this->load->getObject($className, $alias, $params);
    }

    /**
     * Trigger a system event
     *
     * @param $name
     * @param array $data
     */
    public function triggerEvent($name, $data = array())
    {
        $this->event->triggerEvent($name, $data);
    }

    /**
     * Get request object
     *
     * @return Core_Plugin_Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Get the module namespace
     *
     * @param  $moduleName
     * @return string
     */
    public function getNamespace($moduleName)
    {
        return $this->load->getNamespace($moduleName);
    }
}