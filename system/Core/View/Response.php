<?php
/**
 * Core_View_Response
 *
 * Response object, responsible for outputting content,
 * doing redirects and stuff..
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class Core_View_Response
{

    /**
     * main content to output
     * @var string
     */
    private $_body = '';

    /**
     * array of headers in format 'name' => 'value'
     * @var array
     */
    private $_headers = array();

    /**
     * HTTP response code
     * @var int
     */
    private $_httpResponseCode = 200;

    /**
     * Response message
     * @var int
     */
    private $_responseMessage = 'OK';

    /**
     * Redirect flag
     * @var boolean
     */
    private $_isRedirect = false;

    /**
     * Send response
     *
     * @return void
     */
    public function sendResponse()
    {
        wmvc::app()->triggerEvent('wmvc_send_response', array('eventObject' => $this));

        $this->sendHeaders();
        if (!empty($this->_body) && !$this->_isRedirect) {
            $this->outputBody();
        }
    }

    /**
     * Set a response header
     *
     * @param  string $name
     * @param  string $value
     * @param  bool $replace
     * @return $this
     */
    public function setHeader($name, $value, $replace = false)
    {
        $name  = str_replace(array('-', '_'), ' ', (string) $name);
        $name  = ucwords(strtolower($name));
        $name  = str_replace(' ', '-', $name);
        $value = (string) $value;

        if ($replace || !isset($this->_headers[$name])) {
            $this->_headers = array_replace($this->_headers, array($name => $value));
        }

        return $this;
    }

    /**
     * Sends all headers specified
     *
     * @return $this
     */
    public function sendHeaders()
    {
        foreach ($this->_headers as $headerName => $headerValue) {
            header($headerName . ': ' . $headerValue);
        }

        header('HTTP/1.1 ' . $this->_httpResponseCode);
        return $this;
    }

    /**
     * Output body
     *
     * @return void
     */
    public function outputBody()
    {
        echo $this->_body;
    }

    /**
     * Sets redirect location and headers
     *
     * @param  string $url
     * @param  int $code
     * @return $this
     */
    public function setRedirect($url, $code = 302)
    {
        if ($url == '/') {
            $url = '';
        }

        $this->setHeader('Location', rtrim(wmvc::app()->getConfig('Runtime/Request/appUrl'), '/') . $url, true);
        $this->setHttpResponseCode($code, 'Redirecting to URL: ' . $url);
        return $this;
    }

    /**
     * Sets absolute URL redirect location and headers
     *
     * @param  string $url
     * @param  int $code
     * @return $this
     */
    public function setUrlRedirect($url, $code = 302)
    {
        $this->setHeader('Location', $url, true)->setHttpResponseCode($code, 'Redirecting to: ' . $url);
        return $this;
    }

    /**
     * Set body
     *
     * @param  $content
     * @return $this
     */
    public function setBody($content)
    {
        $this->_body = $content;
        return $this;
    }

    /**
     * Append body
     *
     * @param  $content
     * @return $this
     */
    public function appendBody($content)
    {
        $this->_body = $this->_body.$content;
        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * Set HTTP response code
     *
     * @param  int    $code
     * @param  string $message
     * @return $this
     */
    public function setHttpResponseCode($code, $message = '')
    {
        if ((300 <= $code) && (307 >= $code)) {
            $this->_isRedirect = true;
        } else {
            $this->_isRedirect = false;
        }

        $this->_httpResponseCode = $code;
        $this->_responseMessage  = $message;
        return $this;
    }

    /**
     * Is it a redirect
     *
     * @return boolean
     */
    public function isRedirect()
    {
        return $this->_isRedirect;
    }

    /**
     * Get HTTP Response code
     *
     * @return int
     */
    public function getHttpResponseCode()
    {
        return $this->_httpResponseCode;
    }

    /**
     * Get response message
     *
     * @return int
     */
    public function getResponseMessage()
    {
        return $this->_responseMessage;
    }

    /**
     * Get headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * Clear headers
     *
     * @return $this
     */
    public function clearHeaders()
    {
        $this->_headers    = array();
        $this->_isRedirect = false;
        return $this;
    }
}
