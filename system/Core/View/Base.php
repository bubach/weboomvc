<?php
/**
 * Core_View_Base
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_View_Base extends Core_Object
{

    /**
     * Contains reference to current layout.
     *
     * @var Core_View_Layout
     */
    private $_layout = null;

    /**
     * Set layout object
     *
     * @param   Core_View_Layout $layout
     * @return  Core_View_Base
     */
    public function setLayout($layout)
    {
        $this->_layout = $layout;
        $this->afterLayoutSet();
        return $this;
    }

    /**
     * Get layout object
     *
     * @return   Core_View_Layout
     */
    public function getLayout()
    {
        return $this->_layout;
    }

    /**
     * Add view to layout
     *
     * @return   Core_View_Base
     */
    public function addToLayout()
    {
        $this->_layout->addView($this);
        return $this;
    }

    /**
     * Usable in views where before rendering
     * but after assigned to layout object.
     *
     */
    public function afterLayoutSet()
    {
    }

    /**
     * Complete output rendering.
     *
     * @return string $out
     */
    final public function render()
    {
        $this->beforeRender();
        $out = $this->renderOutput();
        $out = $this->afterRender($out);
        return $out;
    }

    /**
     * Render output, either from the view template
     * or from child-views if no template defined.
     * Can be overridden to output whatever.
     *
     * @return string $out
     */
    public function renderOutput()
    {
        $out = '';

        if ($this->getData('template')) {
            ob_start();
            include($this->getFile($this->getData('template'), true));
            $out = ob_get_clean();
        } else {
            if (is_object($this->_layout)) {
                $childViews = $this->_layout->getChildViews($this->getData('viewName'));

                foreach ($childViews as $childView) {
                    $childView->setData('parentView', $this->getData('viewName'));
                    $out .= $this->_layout->renderView($childView);
                }
            }
        }

        return $out;
    }

    /**
     * Called before the output is rendered.
     * 
     */
    public function beforeRender()
    {
    }

    /**
     * Called after the output is rendered.
     * 
     * @param  string $out
     * @return string $out
     */
    public function afterRender($out)
    {
        return $out;
    }

    /**
     * Retrieve request array
     *
     * @param  string $path
     * @return array
     */
    public function getRequest($path = null)
    {
        if (!empty($path)) {
            return wmvc::app()->getConfig('Runtime/Request/'.$path);
        } else {
            return wmvc::app()->getConfig('Runtime/Request');
        }
    }

    /**
     * Get relative theme path to file
     *
     * @param  $file
     * @param  bool $fsPath
     * @return string
     */
    public function getFile($file, $fsPath = false)
    {
        return $this->_layout->getThemeFile($file, $fsPath);
    }

    /**
     * Render child-view
     *
     * @param  string $viewName
     * @return string $html
     */
    public function renderChild($viewName = '')
    {
        return $this->_layout->renderView($this->_layout->getView($viewName));
    }
}
