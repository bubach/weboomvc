<?php
/**
 * Core_View_Component
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_View_Component
{
    /**
     * @var array
     */
    public $arrayAttributes = array(
        'class'    => ' ',
        'style'    => ';',
        'keywords' => ',',
    );

    /**
     * @var array
     */
    public $elementAliases = array(
        'paragraph' => 'p',
        'bold' => 'strong',
        'italic' => 'em',
        'comment' => '!--',
    );

    /**
     * @var array
     */
    public $nullAttributes = array(
        'checked',
        'selected',
        'disabled',
    );

    /**
     * @var array
     */
    public $selfClosing = array(
        '!--', 'img', 'br', 'hr', 'input', 'meta'
    );
} 