<?php
/**
 * Core_Plugin_Layout
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_View_Layout
{

    /**
     * Combined layout based on config-handles
     *
     * @var array
     */
    private $_layout = array();

    /**
     * Response object
     *
     * @object Core_View_Response
     */
    private $_response = null;

    /**
     * Views sorted and ready for rendering
     * in a flat one dimensional array
     *
     * @var Core_View_Base[]
     */
    private $_views = array();

    /**
     * Total view-count, including views
     * not part of layout, for anonymous naming
     *
     * @var int
     */
    private $_viewCount = 0;

    /**
     * Current active theme
     *
     * @var array
     */
    private $_theme = 'default';

    /**
     * Current list of themes to look through for files
     *
     * @var array
     */
    private $_themeFallbacks = array();

    /**
     * Theme-file found, kept for event observer access
     *
     * @var null
     */
    private $_foundFile = null;

    /**
     * Constructor
     *
     * @param  null   $response
     * @param  string $handles
     * @param  null   $theme
     * @throws Exception
     */
    public function __construct($response = null, $handles = 'default', $theme = null)
    {
        if (!is_object($response)) {
            throw new Exception('Layout needs a valid response object as parameter!', 1);
        }

        $this->_response = $response;
        $this->setTheme(empty($theme) ? wmvc::app()->getConfig('Runtime/Theme') : $theme);
        $this->addHandle($handles);
        $this->generateLayoutViews($this->_layout);
    }

    /**
     * Set layout theme
     *
     * @param  string  $theme
     * @return Core_View_Layout  Core_Plugin_Layout
     */
    public function setTheme($theme)
    {
        $this->_theme = $theme;
        return $this;
    }

    /**
     * Get layout theme
     *
     * @return  string  $theme
     */
    public function getTheme()
    {
        return $this->_theme;
    }

    /**
     * Add layout handle(s)
     *
     * @param  string|array $handle
     * @return Core_View_Layout
     */
    public function addHandle($handle)
    {
        if (!is_array($handle)) {
            $handle = array($handle);
        }

        foreach ($handle as $h) {
            $newHandle = wmvc::app()->getConfig('Layout/'.$h);

            if (is_array($newHandle)) {
                $this->_layout = array_replace_recursive($this->_layout, $newHandle);
            }
        }

        return $this;
    }

    /**
     * Create view, not part of layout/output
     *
     * @param  string   $name
     * @param  array    $data
     * @param  string   $type
     * @return Core_View_Base
     * @throws Exception
     */
    public function createView($name = '', $type = 'Core_View_Base', $data = array())
    {
        if (empty($name)) {
            $name = 'ANONYMOUS_'.$this->_viewCount;
        }
        $data['viewName'] = $name;

        $view = wmvc::app()->getObject($type, $name, $data);
        $view->setLayout($this);

        $this->_viewCount++;
        return $view;
    }

    /**
     * Add view to layout output
     *
     * @param  Core_View_Base $view
     * @return $this
     * @throws Exception
     */
    public function addView($view)
    {
        $parentView = $view->getData('parentView') ? $view->getData('parentView') : null;

        if (!empty($parentView) && !$this->getView($parentView)) {
            throw new Exception('Error adding view, parent not found', 1);
        }

        $newView = array('view' => get_class($view), 'data' => $view->getData());
        $this->addViewToLayout($newView, $this->_layout);

        $newViewArray = array();
        $tmpObj       = &$this;

        array_walk_recursive(
            $this->_layout,
            function ($v, $k) use (&$newViewArray, &$tmpObj, &$view) {
                if ($k == 'viewName') {
                    if ($v == $view->getData('viewName')) {
                        $newViewArray[$v] = $view;
                    } else {
                        $newViewArray[$v] = $tmpObj->getView($v);
                    }
                }
            }
        );

        $this->_views = $newViewArray;
        return $this;
    }

    /**
     * add a view to the layout array that keeps
     * config friendly syntax for export/save.
     *
     * @param  $view
     * @param  $layout
     * @param  int $iteration
     * @return bool
     */
    public function addViewToLayout($view, &$layout = null, $iteration = 0)
    {
        if (is_null($layout)) {
            return $this->addViewToLayout($view, $this->_layout, $iteration);
        }

        $parent   = isset($view['data']['parentView']) ? $view['data']['parentView'] : null;
        $before   = array('data', 'insertBefore');
        $after    = array('data', 'insertAfter');
        $viewName = $view['data']['viewName'];

        foreach ($layout as $sliceName => &$layoutSlice) {
            if ($sliceName == $parent) {
                return $this->addChildViewToLayout($view, $viewName, $layoutSlice['childViews'], $before, $after);
            }
            if (isset($layoutSlice['childViews'])) {
                if ($this->addViewToLayout($view, $layoutSlice['childViews'], $iteration + 1)) {
                    return true;
                }
            }
        }

        if ($iteration == 0) {
            return $this->addChildViewToLayout($view, $viewName, $this->_layout, $before, $after);
        } else {
            return false;
        }
    }

    /**
     * Add a child view to layout array and sort
     *
     * @param  $view
     * @param  $viewName
     * @param  $layoutSlice
     * @param  string[] $before
     * @param  string[] $after
     * @return bool
     */
    private function addChildViewToLayout($view, $viewName, &$layoutSlice, $before, $after)
    {
        $layoutSlice[$viewName] = $view;

        if (count($layoutSlice) > 1) {
            wmvc::app()->getObject('Core_Plugin_Config')->sortConfigArray($layoutSlice, $before, $after);
        }

        return true;
    }

    /**
     * Generate views from layout config, recursive
     * 
     * @param  array  $layout
     * @param  string $parentView
     * @return Core_View_Layout
     */
    public function generateLayoutViews($layout, $parentView = null)
    {
        foreach ($layout as $name => $attributes) {
            $data = isset($attributes['data']) ? $attributes['data'] : array();
            $type = isset($attributes['view']) ? $attributes['view'] : 'Core_View_Base';

            if ($parentView) {
                $data['parentView'] = $parentView;
            }

            $this->createView($name, $type, $data)->addToLayout();

            if (!empty($attributes['childViews'])) {
                $this->generateLayoutViews($attributes['childViews'], $name);
            }
        }

        return $this;
    }

    /**
     * Render full layout and set response body. Renders children
     * automatically if parent has no template.
     *
     * @return $this
     */
    public function renderLayout()
    {
        foreach ($this->_views as $name => $view) {
            if ($view->getData('parentView')) {
                $parentView = $this->getView($view->getData('parentView'));

                if (!$parentView->getData('template') && $view->getData('autoRender') != false) {
                    $this->_response->appendBody($this->renderView($view));
                }
            } else {
                $this->_response->appendBody($this->renderView($view));
            }
        }

        return $this;
    }

    /**
     * Render a view and return output html.
     *
     * @param  Core_View_Base
     * @throws Exception
     * @return string $html
     */
    public function renderView($view)
    {
        if (is_object($view)) {
            return $view->render();
        } else {
            return '';
        }
    }

    /**
     * Retrieve all views from layout as array
     *
     * @return array
     */
    public function getViewsArray()
    {
        return $this->_views;
    }

    /**
     * Retrieve layout array
     *
     * @return array
     */
    public function getLayoutArray()
    {
        return $this->_layout;
    }

    /**
     * Retrieve all child-views from parent view
     *
     * @param  string  $name
     * @param  boolean $recursive
     * @return array
     */
    public function getChildViews($name, $recursive = false)
    {
        $childViews = array();

        foreach ($this->_views as $view) {
            if ($view->getData('parentView') == $name) {
                $childViews[$view->getData('viewName')] = $view;

                if ($recursive == true) {
                    $childViews += $this->getChildViews($view->getData('viewName'), true);
                }
            }
        }

        return $childViews;
    }

    /**
     * Get view object by name
     *
     * @param  string $name
     * @return bool | Core_View_Base
     */
    public function getView($name)
    {
        if (isset($this->_views[$name])) {
            return $this->_views[$name];
        } else {
            return false;
        }
    }

    /**
     * Set the paths to look through for files
     *
     * @param  $themeFallbacks
     * @return $this
     */
    public function setThemeFallbacks($themeFallbacks)
    {
        $this->_themeFallbacks = $themeFallbacks;
        return $this;
    }

    /**
     * get theme fallbacks array
     *
     * @return array
     */
    public function getThemeFallbacks()
    {
        if (empty($this->_themeFallbacks)) {
            $this->generateThemeFallbacks();
        }
        return $this->_themeFallbacks;
    }

    /**
     * Generate the theme fallback paths at init
     *
     * @return $this
     */
    public function generateThemeFallbacks()
    {
        $nextFallback = $this->getTheme();

        while ($nextFallback) {
            $active = wmvc::app()->getConfig('Theme/' . $nextFallback.'/active');

            if ($active && $themeModule = wmvc::app()->getConfig('Theme/'.$nextFallback.'/module')) {
                $themePath = strtolower(wmvc::app()->getNamespace($themeModule)) . DS . $themeModule . DS . 'Theme';
                if (!in_array($themePath, $this->_themeFallbacks)) {
                    $this->_themeFallbacks[] = $themePath;
                }
                $nextFallback = wmvc::app()->getConfig('Theme/' . $nextFallback . '/extends');
            } else {
                $nextFallback = null;
            }
        }

        $runtimeModule           = wmvc::app()->getConfig('Runtime/Request/module');
        $this->_themeFallbacks[] = strtolower(wmvc::app()->getNamespace($runtimeModule)) . DS . $runtimeModule . DS . 'Theme';

        wmvc::app()->triggerEvent('wmvc_generate_theme_fallbacks', array('eventObject' => $this));
        return $this;
    }

    /**
     * Get file path based on active theme and fallback
     *
     * @param  string $file
     * @param  bool   $fsPath
     * @return string $filePath
     */
    public function getThemeFile($file, $fsPath = false)
    {
        $themeFallbacks = $this->getThemeFallbacks();

        foreach ($themeFallbacks as $path) {
            $filePath = $path . DS . $file;

            if (file_exists($filePath)) {
                if (!$fsPath) {
                    $filePath = str_replace(wmvc::app()->getConfig('Runtime/Request/documentRoot'), '', $filePath);
                }

                $this->setFoundFile($filePath);
                break;
            }
        }

        wmvc::app()->triggerEvent('wmvc_get_theme_file', array('eventObject' => $this));
        return $this->getFoundFile();
    }

    /**
     * Return the path to found file, used in event observers
     *
     * @return null
     */
    public function getFoundFile()
    {
        return $this->_foundFile;
    }

    /**
     * Set the path to requested file, used in event observers
     *
     * @param $filePath
     */
    public function setFoundFile($filePath)
    {
        $this->_foundFile = $filePath;
    }
}
