<?php
/**
 * Core_View_Assets_Css
 *
 * Minify CSS and handle relative URL's,
 * originally by Matthias Mullie.
 * MIT licence.
 *
 * @license   MIT License
 * @package   WebooMVC
 * @author    Matthias Mullie, Christoffer Bubach
 */

class Core_View_Assets_Css extends Core_View_Assets_Minify
{
    /**
     * @var int maximum import size in kB
     */
    protected $maxImportSize = 5;

    /**
     * @var string[] valid import extensions
     */
    protected $importExtensions = array(
        'gif'  => 'data:image/gif',
        'png'  => 'data:image/png',
        'jpe'  => 'data:image/jpeg',
        'jpg'  => 'data:image/jpeg',
        'jpeg' => 'data:image/jpeg',
        'svg'  => 'data:image/svg+xml',
        'woff' => 'data:application/x-font-woff',
        'tif'  => 'image/tiff',
        'tiff' => 'image/tiff',
        'xbm'  => 'image/x-xbitmap',
    );

    /**
     * Set the maximum size if files to be imported.
     *
     * Files larger than this size (in kB) will not be imported into the CSS.
     * Importing files into the CSS as data-uri will save you some connections,
     * but we should only import relatively small decorative images so that our
     * CSS file doesn't get too bulky.
     *
     * @param int $size Size in kB
     */
    public function setMaxImportSize($size)
    {
        $this->maxImportSize = $size;
    }

    /**
     * Set the type of extensions to be imported into the CSS (to save network
     * connections).
     * Keys of the array should be the file extensions & respective values
     * should be the data type.
     *
     * @param string[] $extensions Array of file extensions
     */
    public function setImportExtensions(array $extensions)
    {
        $this->importExtensions = $extensions;
    }

    /**
     * Move any import statements to the top.
     *
     * @param string $content Nearly finished CSS content
     *
     * @return string
     */
    protected function moveImportsToTop($content)
    {
        if (preg_match_all(
            '/(;?)(@import (?<url>url\()?(?P<quotes>["\']?).+?(?P=quotes)(?(url)\)))/',
            $content,
            $matches
        )) {
            foreach ($matches[0] as $import) {
                $content = str_replace($import, '', $content);
            }
            $content = implode(';', $matches[2]).';'.trim($content, ';');
        }

        return $content;
    }

    /**
     * Combine CSS from import statements.
     *
     * Any @immport's will be loaded and their content merged into the original file,
     * to save HTTP requests.
     *
     * @param string   $source The file to combine imports for
     * @param string   $content The CSS content to combine imports for
     * @param string[] $parents Parent paths, for circular reference checks
     *
     * @throws Exception
     * @return string
     */
    protected function combineImports($source, $content, $parents)
    {
        $importRegexes = array(
            '/@import\s+url\((?P<quotes>["\']?)(?P<path>.+?)(?P=quotes)\)\s*(?P<media>[^;]*)\s*;?/ix',
            '/@import\s+(?P<quotes>["\'])(?P<path>.+?)(?P=quotes)\s*(?P<media>[^;]*)\s*;?/ix',
        );

        $matches = array();
        foreach ($importRegexes as $importRegex) {
            if (preg_match_all($importRegex, $content, $regexMatches, PREG_SET_ORDER)) {
                $matches = array_merge($matches, $regexMatches);
            }
        }

        $search = array();
        $replace = array();

        foreach ($matches as $match) {
            $importPath = dirname($source).'/'.$match['path'];

            if (!$this->canImportByPath($match['path']) || !$this->canImportFile($importPath)) {
                continue;
            }

            if (in_array($importPath, $parents)) {
                throw new Exception('Failed to import file "'.$importPath.'": circular reference detected.');
            }

            $minifier = new static($importPath);
            $importContent = $minifier->execute($source, $parents);

            if (!empty($match['media'])) {
                $importContent = '@media '.$match['media'].'{'.$importContent.'}';
            }

            $search[] = $match[0];
            $replace[] = $importContent;
        }

        return str_replace($search, $replace, $content);
    }

    /**
     * Import files into the CSS, base64-ized.
     *
     * @url(image.jpg) images will be loaded and their content merged into the
     * original file, to save HTTP requests.
     *
     * @param string $source  The file to import files for
     * @param string $content The CSS content to import files for
     *
     * @return string
     */
    protected function importFiles($source, $content)
    {
        $regex = '/url\((["\']?)(.+?)\\1\)/i';

        if ($this->importExtensions && preg_match_all($regex, $content, $matches, PREG_SET_ORDER)) {
            $search = array();
            $replace = array();

            foreach ($matches as $match) {
                $extension = substr(strrchr($match[2], '.'), 1);
                if ($extension && !array_key_exists($extension, $this->importExtensions)) {
                    continue;
                }

                $path = $match[2];
                $path = dirname($source).'/'.$path;

                if ($this->canImportFile($path) && $this->canImportBySize($path)) {
                    $importContent = $this->load($path);
                    $importContent = base64_encode($importContent);

                    $search[] = $match[0];
                    $replace[] = 'url('.$this->importExtensions[$extension].';base64,'.$importContent.')';
                }
            }
            $content = str_replace($search, $replace, $content);
        }
        return $content;
    }

    /**
     * Minify the data.
     * Perform CSS optimizations.
     *
     * @param string[optional] $path    Path to write the data to
     * @param string[]         $parents Parent paths, for circular reference checks
     *
     * @return string The minified data
     */
    public function execute($path = null, $parents = array())
    {
        $content = '';

        foreach ($this->data as $source => $css) {
            $this->extractStrings();
            $this->stripComments();
            $css = $this->replace($css);

            $css = $this->stripWhitespace($css);
            $css = $this->stripEmptyTags($css);

            // restore the string we've extracted earlier
            $css = $this->restoreExtractedData($css);

            $source = is_int($source) ? '' : $source;
            $parents = $source ? array_merge($parents, array($source)) : $parents;
            $css = $this->combineImports($source, $css, $parents);
            $css = $this->importFiles($source, $css);


            $converter = $this->getPathConverter($source, $path ?: $source);
            $css = $this->move($converter, $css);

            $content .= $css;
        }

        $content = $this->moveImportsToTop($content);

        return $content;
    }

    /**
     * Moving a css file should update all relative urls.
     * Relative references (e.g. ../images/image.gif) in a certain css file,
     * will have to be updated when a file is being saved at another location
     * (e.g. ../../images/image.gif, if the new CSS file is 1 folder deeper).
     *
     * @param PathConverter $converter Relative path converter
     * @param string        $content   The CSS content to update relative urls for
     *
     * @return string
     */
    protected function move(PathConverter $converter, $content)
    {
        $relativeRegexes = array(
            '/url\(\s*(?P<quotes>["\'])?(?P<path>.+?)(?(quotes)(?P=quotes))\s*\)/ix',
            '/@import\s+(?P<quotes>["\'])(?P<path>.+?)(?P=quotes)/ix',
        );

        $matches = array();
        foreach ($relativeRegexes as $relativeRegex) {
            if (preg_match_all($relativeRegex, $content, $regexMatches, PREG_SET_ORDER)) {
                $matches = array_merge($matches, $regexMatches);
            }
        }

        $search = array();
        $replace = array();

        foreach ($matches as $match) {
            $type = (strpos($match[0], '@import') === 0 ? 'import' : 'url');

            $url = $match['path'];
            if ($this->canImportByPath($url)) {
                $params = strrchr($url, '?');
                $url = $params ? substr($url, 0, -strlen($params)) : $url;

                $url = $converter->convert($url);
                $url .= $params;
            }
            $url = trim($url);
            if (preg_match('/[\s\)\'"#\x{7f}-\x{9f}]/u', $url)) {
                $url = $match['quotes'] . $url . $match['quotes'];
            }

            $search[] = $match[0];
            if ($type === 'url') {
                $replace[] = 'url('.$url.')';
            } elseif ($type === 'import') {
                $replace[] = '@import "'.$url.'"';
            }
        }

        return str_replace($search, $replace, $content);
    }

    /**
     * Strip empty tags from source code.
     *
     * @param string $content
     *
     * @return string
     */
    protected function stripEmptyTags($content)
    {
        $content = preg_replace('/(?<=^)[^\{\};]+\{\s*\}/', '', $content);
        $content = preg_replace('/(?<=(\}|;))[^\{\};]+\{\s*\}/', '', $content);

        return $content;
    }

    /**
     * Strip comments from source code.
     */
    protected function stripComments()
    {
        $this->registerPattern('/\/\*.*?\*\//s', '');
    }

    /**
     * Strip whitespace.
     *
     * @param string $content The CSS content to strip the whitespace for
     *
     * @return string
     */
    protected function stripWhitespace($content)
    {
        $content = preg_replace('/^\s*/m', '', $content);
        $content = preg_replace('/\s*$/m', '', $content);

        $content = preg_replace('/\s+/', ' ', $content);

        $content = preg_replace('/\s*([\*$~^|]?+=|[{};,>~]|!important\b)\s*/', '$1', $content);
        $content = preg_replace('/([\[(:])\s+/', '$1', $content);
        $content = preg_replace('/\s+([\]\)])/', '$1', $content);
        $content = preg_replace('/\s+(:)(?![^\}]*\{)/', '$1', $content);

        $pseudos = array('nth-child', 'nth-last-child', 'nth-last-of-type', 'nth-of-type');

        $content = preg_replace(
            '/:(' . implode('|', $pseudos) . ')\(\s*([+-]?)\s*(.+?)\s*([+-]?)\s*(.*?)\s*\)/',
            ':$1($2$3$4$5)',
            $content
        );

        $content = str_replace(';}', '}', $content);

        return trim($content);
    }

    /**
     * Find all `calc()` occurrences.
     *
     * @param string $content The CSS content to find `calc()`s in.
     *
     * @return string[]
     */
    protected function findCalcs($content)
    {
        $results = array();
        preg_match_all('/calc(\(.+?)(?=$|;|calc\()/', $content, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $length = strlen($match[1]);
            $expr = '';
            $opened = 0;

            for ($i = 0; $i < $length; $i++) {
                $char = $match[1][$i];
                $expr .= $char;
                if ($char === '(') {
                    $opened++;
                } elseif ($char === ')' && --$opened === 0) {
                    break;
                }
            }

            $results['calc('.count($results).')'] = 'calc'.$expr;
        }

        return $results;
    }

    /**
     * Check if file is small enough to be imported.
     *
     * @param string $path The path to the file
     *
     * @return bool
     */
    protected function canImportBySize($path)
    {
        return ($size = @filesize($path)) && $size <= $this->maxImportSize * 1024;
    }

    /**
     * Check if file a file can be imported, going by the path.
     *
     * @param string $path
     *
     * @return bool
     */
    protected function canImportByPath($path)
    {
        return preg_match('/^(data:|https?:|\\/)/', $path) === 0;
    }

    /**
     * Return a converter to update relative paths to be relative to the new
     * destination.
     *
     * @param string $source
     * @param string $target
     *
     * @return ConverterInterface
     */
    protected function getPathConverter($source, $target)
    {
        return new PathConverter($source, $target);
    }
}
