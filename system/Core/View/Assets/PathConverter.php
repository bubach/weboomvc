<?php
/**
 * Core_View_Assets_PathConverter
 *
 * Handle relative path - URL's,
 * originally by Matthias Mullie.
 * MIT licence.
 *
 * @license   MIT License
 * @package   WebooMVC
 * @author    Matthias Mullie, Christoffer Bubach
 */

class PathConverter
{
    /**
     * @var string
     */
    protected $_from;

    /**
     * @var string
     */
    protected $_to;

    /**
     * Constructor
     *
     * @param string $from The original base path (directory, not file!)
     * @param string $to   The new base path (directory, not file!)
     */
    public function __construct($from, $to)
    {
        $shared = $this->shared($from, $to);

        if ($shared === '') {
            $cwd  = getcwd();
            $from = strpos($from, $cwd) === 0 ? $from : $cwd.'/'.$from;
            $to   = strpos($to, $cwd) === 0 ? $to : $cwd.'/'.$to;

            $from = realpath($from) ?: $from;
            $to   = realpath($to) ?: $to;
        }

        $from = $this->dirname($from);
        $to   = $this->dirname($to);

        $this->_from = $this->normalize($from);
        $this->_to   = $this->normalize($to);
    }

    /**
     * Normalize path, parsing '/../' in paths for example.
     *
     * @param string $path
     *
     * @return string
     */
    protected function normalize($path)
    {
        $path = rtrim(str_replace(DIRECTORY_SEPARATOR, '/', $path), '/');

        do {
            $path = preg_replace('/[^\/]+(?<!\.\.)\/\.\.\//', '', $path, -1, $count);
        } while ($count);

        return $path;
    }

    /**
     * Figure out the shared root-path of 2 locations.
     *
     * @param string $path1
     * @param string $path2
     *
     * @return string
     */
    protected function shared($path1, $path2)
    {
        $path1 = $path1 ? explode('/', $path1) : array();
        $path2 = $path2 ? explode('/', $path2) : array();

        $shared = array();

        foreach ($path1 as $i => $chunk) {
            if (isset($path2[$i]) && $path1[$i] == $path2[$i]) {
                $shared[] = $chunk;
            } else {
                break;
            }
        }

        return implode('/', $shared);
    }

    /**
     * Convert paths relative from 1 file to another.
     *
     * @param string $path The relative path that needs to be converted
     * @return string The new relative path
     */
    public function convert($path)
    {
        if ($this->_from === $this->_to) {
            return $path;
        }

        $path = $this->normalize($path);
        if (strpos($path, '/') === 0) {
            return $path;
        }

        $path   = $this->normalize($this->_from . '/' . $path);
        $shared = $this->shared($path, $this->_to);
        $path   = mb_substr($path, mb_strlen($shared));
        $to     = mb_substr($this->_to, mb_strlen($shared));
        $to     = str_repeat('../', mb_substr_count($to, '/'));

        return $to . ltrim($path, '/');
    }

    /**
     * Attempt to get the directory name from a path.
     *
     * @param string $path
     *
     * @return string
     */
    protected function dirname($path)
    {
        if (is_file($path)) {
            return dirname($path);
        }
        if (is_dir($path)) {
            return rtrim($path, '/');
        }
        if (mb_substr($path, -1) === '/') {
            return rtrim($path, '/');
        }
        if (preg_match('/.*\..*$/', basename($path)) !== 0) {
            return dirname($path);
        }

        return $path;
    }
}
