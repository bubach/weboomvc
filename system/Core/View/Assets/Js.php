<?php
/**
 * Core_View_Assets_Js
 *
 * Minify originally by Matthias Mullie.
 *
 * @license   MIT License
 * @package   WebooMVC
 * @author    Matthias Mullie, Christoffer Bubach
 */

class Core_View_Assets_Js extends Core_View_Assets_Minify
{
    /**
     * Var-matching regex, not including unicode
     *
     * @var string
     */
    const REGEX_VARIABLE = '\b[a-zA-Z_$][0-9a-zA-Z_$]*\b';

    /**
     * List of JavaScript reserved words.
     *
     * @var string[]
     */
    protected $keywordsReserved = array(
        'do', 'if', 'in', 'for', 'let', 'new', 'try', 'var', 'case', 'else', 'enum', 'eval', 'null',
        'this', 'true', 'void', 'with', 'await', 'break', 'catch', 'class', 'const', 'false', 'super',
        'throw', 'while', 'yield', 'delete', 'export', 'import', 'public', 'return', 'static', 'switch',
        'typeof', 'default', 'extends', 'finally', 'package', 'private', 'continue', 'debugger', 'function',
        'arguments', 'interface', 'protected', 'implements', 'instanceof', 'abstract', 'boolean', 'byte',
        'char', 'double', 'final', 'float', 'goto', 'int', 'long', 'native', 'short', 'synchronized',
        'throws', 'transient', 'volatile'
    );

    /**
     * List of JavaScript reserved words that accept a <variable, value, ...>
     * after them.
     *
     * @var string[]
     */
    protected $keywordsBefore = array(
        'do', 'in', 'let', 'new', 'var', 'case', 'else', 'enum', 'void', 'with', 'class', 'const', 'yield',
        'delete', 'export', 'import', 'public', 'static', 'typeof', 'extends', 'package', 'private', 'function',
        'protected', 'implements', 'instanceof'
    );

    /**
     * List of JavaScript reserved words that accept a <variable, value, ...>
     * before them.
     *
     * @var string[]
     */
    protected $keywordsAfter = array('in', 'public', 'extends', 'private', 'protected', 'implements', 'instanceof');

    /**
     * List of all JavaScript operators.
     *
     * @var string[]
     */
    protected $operators = array(
        '+', '-', '*', '/', '%', '=', '+=', '-=', '*=', '/=', '%=', '<<=', '>>=', '>>>=', '&=', '^=', '|=',
        '&', '|', '^', '~', '<<', '>>', '>>>', '==', '===', '!=', '!==', '>', '<', '>=', '<=', '&&', '||',
        '!', '.', '[', ']', '?', ':', ',', ';', '(', ')', '{', '}'
    );

    /**
     * List of JavaScript operators that accept a <variable, value, ...> after
     * them. Some end of lines are not the end of a statement, like with these
     * operators.
     *
     * @var string[]
     */
    protected $operatorsBefore = array(
        '+', '-', '*', '/', '%', '=', '+=', '-=', '*=', '/=', '%=', '<<=', '>>=', '>>>=', '&=', '^=', '|=',
        '&', '|', '^', '~', '<<', '>>', '>>>', '==', '===', '!=', '!==', '>', '<', '>=', '<=', '&&', '||',
        '!', '.', '[', '?', ':', ',', ';', '(', '{'
    );

    /**
     * List of JavaScript operators that accept a <variable, value, ...> before
     * them. Some end of lines are not the end of a statement, like when
     * continued by one of these operators on the newline.
     *
     * @var string[]
     */
    protected $operatorsAfter = array(
        '+', '-', '*', '/', '%', '=', '+=', '-=', '*=', '/=', '%=', '<<=', '>>=', '>>>=', '&=', '^=',
        '|=', '&', '|', '^', '<<', '>>', '>>>', '==', '===', '!=', '!==', '>', '<', '>=', '<=', '&&', '||', '.', '[',
        ']', '?', ':', ',', ';', '(', ')', '}'
    );

    /**
     * Public property so it can be accessed from inside the closure in
     * extractRegex. Once PHP5.3 compatibility is dropped, we can make this
     * property protected again.
     *
     * @var array
     */
    public $nestedExtracted = array();


    /**
     * Minify the data.
     * Perform JS optimizations.
     *
     * @param string[optional] $path Path to write the data to
     *
     * @return string The minified data
     */
    public function execute($path = null)
    {
        $content = '';
        $this->extractStrings('\'"`');
        $this->stripComments();
        $this->extractRegex();

        foreach ($this->data as $source => $js) {
            $js = $this->replace($js);

            $js = $this->propertyNotation($js);
            $js = $this->shortenBools($js);
            $js = $this->stripWhitespace($js);

            $content .= $js.";";
        }

        $content = ltrim($content, ';');
        $content = substr($content, 0, -1);

        $content = $this->restoreExtractedData($content);

        return $content;
    }

    /**
     * Strip comments from source code.
     *
     * @return void
     */
    public function stripComments()
    {
        $this->registerPattern('/\/\/.*$/m', '');
        $this->registerPattern('/\/\*.*?\*\//s', '');
    }

    /**
     * JS can have /-delimited regular expressions, like: /ab+c/.match(string).
     *
     * The content inside the regex can contain characters that may be confused
     * for JS code: e.g. it could contain whitespace it needs to match & we
     * don't want to strip whitespace in there.
     *
     * The regex can be pretty simple: we don't have to care about comments,
     * (which also use slashes) because stripComments() will have stripped those
     * already.
     *
     * This method will replace all string content with simple REGEX#
     * placeholder text, so we've rid all regular expressions from characters
     * that may be misinterpreted. Original regex content will be saved in
     * $this->extracted and after doing all other minifying, we can restore the
     * original content via restoreRegex()
     */
    protected function extractRegex()
    {
        $minifier = $this;

        $callback = function ($match) use ($minifier) {
            $count = count($minifier->extracted);
            $placeholder = '"'.$count.'"';
            $minifier->extracted[$placeholder] = $match['regex'];

            if (isset($match['before'])) {
                $other = new $minifier();
                $other->extractStrings('\'"`', "$count-");
                $other->stripComments();
                $match['before'] = $other->replace($match['before']);
                $minifier->nestedExtracted += $other->extracted;
            }

            return (isset($match['before']) ? $match['before'] : '').
            $placeholder.
            (isset($match['after']) ? $match['after'] : '');
        };

        $pattern = '(?P<regex>\/(?!\/).*?(?<!\\\\)(\\\\\\\\)*\/[gimy]*)(?![0-9a-zA-Z\/])';

        $keywords = array('do', 'in', 'new', 'else', 'throw', 'yield', 'delete', 'return',  'typeof');
        $before = '(?P<before>[=:,;\}\(\{\[&\|!]|^|'.implode('|', $keywords).')';
        $propertiesAndMethods = array(
            'constructor',
            'flags',
            'global',
            'ignoreCase',
            'multiline',
            'source',
            'sticky',
            'unicode',
            'compile(',
            'exec(',
            'test(',
            'toSource(',
            'toString(',
        );
        $delimiters = array_fill(0, count($propertiesAndMethods), '/');
        $propertiesAndMethods = array_map('preg_quote', $propertiesAndMethods, $delimiters);
        $after = '(?P<after>[\.,;\)\}&\|+]|$|\.('.implode('|', $propertiesAndMethods).'))';
        $this->registerPattern('/'.$before.'\s*'.$pattern.'\s*'.$after.'/', $callback);

        $before = '(?P<before>\b(if|while|for)\s*\((?P<code>[^\(]+?)\))';
        $this->registerPattern('/'.$before.'\s*'.$pattern.'\s*'.$after.'/', $callback);

        $operators = $this->getOperatorsForRegex($this->operatorsBefore, '/');
        $operators += $this->getOperatorsForRegex($this->keywordsReserved, '/');
        $after = '(?P<after>\n\s*('.implode('|', $operators).'))';
        $this->registerPattern('/'.$pattern.'\s*'.$after.'/', $callback);
    }

    /**
     * In addition to the regular restore routine, we also need to restore a few
     * more things that have been extracted as part of the regex extraction...
     *
     * {@inheritdoc}
     */
    protected function restoreExtractedData($content)
    {
        $content = parent::restoreExtractedData($content);
        $content = strtr($content, $this->nestedExtracted);

        return $content;
    }

    /**
     * Strip whitespace.
     *
     * We won't strip *all* whitespace, but as much as possible. The thing that
     * we'll preserve are newlines we're unsure about.
     * JavaScript doesn't require statements to be terminated with a semicolon.
     * It will automatically fix missing semicolons with ASI (automatic semi-
     * colon insertion) at the end of line causing errors (without semicolon.)
     *
     * Because it's sometimes hard to tell if a newline is part of a statement
     * that should be terminated or not, we'll just leave some of them alone.
     *
     * @param string $content The content to strip the whitespace for
     *
     * @return string
     */
    protected function stripWhitespace($content)
    {
        $content = str_replace(array("\r\n", "\r"), "\n", $content);
        $content = preg_replace('/[^\S\n]+/', ' ', $content);
        $content = str_replace(array(" \n", "\n "), "\n", $content);
        $content = preg_replace('/\n+/', "\n", $content);

        $operatorsBefore = $this->getOperatorsForRegex($this->operatorsBefore, '/');
        $operatorsAfter = $this->getOperatorsForRegex($this->operatorsAfter, '/');
        $operators = $this->getOperatorsForRegex($this->operators, '/');
        $keywordsBefore = $this->getKeywordsForRegex($this->keywordsBefore, '/');
        $keywordsAfter = $this->getKeywordsForRegex($this->keywordsAfter, '/');

        unset($operatorsBefore['+'], $operatorsBefore['-'], $operatorsAfter['+'], $operatorsAfter['-']);
        $content = preg_replace(
            array(
                '/('.implode('|', $operatorsBefore).')\s+/',
                '/\s+('.implode('|', $operatorsAfter).')/',
            ), '\\1', $content
        );

        $content = preg_replace(
            array(
                '/(?<![\+\-])\s*([\+\-])(?![\+\-])/',
                '/(?<![\+\-])([\+\-])\s*(?![\+\-])/',
            ), '\\1', $content
        );

        $content = preg_replace('/(^|[;\}\s])\K('.implode('|', $keywordsBefore).')\s+/', '\\2 ', $content);
        $content = preg_replace('/\s+('.implode('|', $keywordsAfter).')(?=([;\{\s]|$))/', ' \\1', $content);


        $operatorsDiffBefore = array_diff($operators, $operatorsBefore);
        $operatorsDiffAfter = array_diff($operators, $operatorsAfter);

        $content = preg_replace('/('.implode('|', $operatorsDiffBefore).')[^\S\n]+/', '\\1', $content);
        $content = preg_replace('/[^\S\n]+('.implode('|', $operatorsDiffAfter).')/', '\\1', $content);
        $content = preg_replace('/\bfor\(([^;]*);;([^;]*)\)/', 'for(\\1;-;\\2)', $content);
        $content = preg_replace('/;+/', ';', $content);
        $content = preg_replace('/\bfor\(([^;]*);-;([^;]*)\)/', 'for(\\1;;\\2)', $content);
        $content = preg_replace('/(for\([^;\{]*;[^;\{]*;[^;\{]*\));(\}|$)/s', '\\1;;\\2', $content);
        $content = preg_replace('/(for\([^;\{]+\s+in\s+[^;\{]+\));(\}|$)/s', '\\1;;\\2', $content);
        $content = preg_replace('/(while\([^;\{]+\));(\}|$)/s', '\\1;;\\2', $content);
        $content = preg_replace('/else;/s', '', $content);
        $content = preg_replace('/;(\}|$)/s', '\\1', $content);
        $content = ltrim($content, ';');

        return trim($content);
    }

    /**
     * We'll strip whitespace around certain operators with regular expressions.
     * This will prepare the given array by escaping all characters.
     *
     * @param string[] $operators
     * @param string   $delimiter
     *
     * @return string[]
     */
    protected function getOperatorsForRegex(array $operators, $delimiter = '/')
    {
        $delimiters = array_fill(0, count($operators), $delimiter);
        $escaped = array_map('preg_quote', $operators, $delimiters);

        $operators = array_combine($operators, $escaped);

        unset($operators['+'], $operators['-']);
        $operators['.'] = '(?<![0-9]\s)\.';

        $chars = preg_quote('+-*\=<>%&|', $delimiter);
        $operators['='] = '(?<!['.$chars.'])\=';

        return $operators;
    }

    /**
     * We'll strip whitespace around certain keywords with regular expressions.
     * This will prepare the given array by escaping all characters.
     *
     * @param string[] $keywords
     * @param string   $delimiter
     *
     * @return string[]
     */
    protected function getKeywordsForRegex(array $keywords, $delimiter = '/')
    {
        $delimiter = array_fill(0, count($keywords), $delimiter);
        $escaped = array_map('preg_quote', $keywords, $delimiter);

        array_walk($keywords, function ($value) {
                return '\b'.$value.'\b';
            });

        $keywords = array_combine($keywords, $escaped);

        return $keywords;
    }

    /**
     * Replaces all occurrences of array['key'] by array.key.
     *
     * @param string $content
     *
     * @return string
     */
    protected function propertyNotation($content)
    {
        $minifier = $this;
        $keywords = $this->keywordsReserved;

        $callback = function ($match) use ($minifier, $keywords) {
            $property = trim($minifier->extracted[$match[1]], '\'"');

            if (in_array($property, $keywords)) {
                return $match[0];
            }

            if (!preg_match('/^'.$minifier::REGEX_VARIABLE.'$/u', $property)) {
                return $match[0];
            }
            return '.'.$property;
        };

        preg_match('/(\[[^\]]+\])[^\]]*$/', static::REGEX_VARIABLE, $previousChar);
        $previousChar = $previousChar[1];

        $keywords = $this->getKeywordsForRegex($keywords);
        $keywords = '(?<!'.implode(')(?<!', $keywords).')';

        return preg_replace_callback('/(?<='.$previousChar.'|\])'.$keywords.'\[\s*(([\'"])[0-9]+\\2)\s*\]/u', $callback, $content);
    }

    /**
     * Replaces true & false by !0 and !1.
     *
     * @param string $content
     *
     * @return string
     */
    protected function shortenBools($content)
    {
        $callback = function ($match) {
            if (trim($match[1]) === '.') {
                return $match[0];
            }

            return $match[1].($match[2] === 'true' ? '!0' : '!1');
        };
        $content = preg_replace_callback('/(^|.\s*)\b(true|false)\b(?!:)/', $callback, $content);
        $content = preg_replace('/\bwhile\(!0\){/', 'for(;;){', $content);

        preg_match_all('/\bdo\b/', $content, $dos, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

        $dos = array_reverse($dos);
        foreach ($dos as $do) {
            $offsetDo = $do[0][1];

            preg_match_all('/\bfor\(;;\)/', $content, $whiles, PREG_OFFSET_CAPTURE | PREG_SET_ORDER, $offsetDo);
            foreach ($whiles as $while) {
                $offsetWhile = $while[0][1];

                $open = substr_count($content, '{', $offsetDo, $offsetWhile - $offsetDo);
                $close = substr_count($content, '}', $offsetDo, $offsetWhile - $offsetDo);

                if ($open === $close) {
                    $content = substr_replace($content, 'while(!0)', $offsetWhile, strlen('for(;;)'));
                    break;
                }
            }
        }

        return $content;
    }

    /**
     * Protected method in parent made public, so it can be accessed from inside
     * the closure in extractRegex. Once PHP5.3 compatibility is dropped, we can
     * remove this.
     *
     * {@inheritdoc}
     */
    public function extractStrings($chars = '\'"', $placeholderPrefix = '') {
        parent::extractStrings($chars, $placeholderPrefix);
    }

    /**
     * Protected method in parent made public, so it can be accessed from inside
     * the closure in extractRegex. Once PHP5.3 compatibility is dropped, we can
     * remove this.
     *
     * {@inheritdoc}
     */
    public function replace($content) {
        return parent::replace($content);
    }
}
