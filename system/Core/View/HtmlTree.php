<?php
/**
 * Core_View_HtmlTree
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_View_HtmlTree implements Iterator
{

    /** Constants */
    const ELEMENT_NAME = 0;

    const CHILDREN     = 1;

    const ATTRIBUTES   = 2;

    /**
     * Internal pointer position, as reference
     * point for further traversal-searches.
     *
     * @var null|array
     */
    public $referencePointer = null;

    /**
     * The string representation of current position in tree,
     * like '2.3.1.1.0' => [2][3][1][1][0]
     *
     * @var null|array
     */
    public $currentPath = '';

    /**
     * @var array Array of Id's in format as above.
     */
    public $currentSearchResult = array();

    /**
     * @var int Current pointer index-position in search result.
     */
    public $currentSearchIndex = 0;

    /**
     * @var array
     */
    public $arrayAttributes = array(
        'class'    => ' ',
        'style'    => ';',
        'keywords' => ',',
    );

    /**
     * @var array
     */
    public $elementAliases = array(
        'paragraph' => 'p',
        'bold' => 'strong',
        'italic' => 'em',
        'comment' => '!--',
    );

    /**
     * @var array
     */
    public $nullAttributes = array(
        'checked',
        'selected',
        'disabled',
    );

    /**
     * @var array
     */
    public $selfClosing = array(
        '!--', 'img', 'br', 'hr', 'input', 'meta'
    );

    /**
     * $elements nested array of all elements to be
     * rendered. each element-array contains:
     *  - string, name of tag, or empty string for raw
     *  - array, list of strings for text or arrays for child nodes
     *  - array, list of attribute-name and -values.
     *
     * @var [type]
     */
    public $elements = array();

    /**
     * @var array
     */
    public $ids = array();

    /**
     * @var array
     */
    public $classes = array();

    /**
     * @var array
     */
    protected $searchStack = array();

    /**
     * Constructor, resets current scope
     *
     * @return Core_View_HtmlTree
     */
    public function __construct()
    {
        return $this->resetRoot();
    }

    /**
     * Part of the Iterator interface
     */
    public function rewind()
    {
        $this->currentSearchIndex = 0;
    }

    /**
     * Part of the Iterator interface
     */
    public function current()
    {
        $path = isset($this->currentSearchResult[$this->currentSearchIndex]) ?
            $this->currentSearchResult[$this->currentSearchIndex] :
            '0';
        return $this->setPath($path);
    }

    /**
     * Part of the Iterator interface
     */
    public function key()
    {
        return $this->currentSearchIndex;
    }

    /**
     * Part of the Iterator interface
     */
    public function next()
    {
        ++$this->currentSearchIndex;
    }

    /**
     * Part of the Iterator interface
     */
    public function valid()
    {
        if (!isset($this->currentSearchResult[$this->currentSearchIndex])) {
            if (strlen($this->currentPath) > 1) {
                $this->up();
            } else {
                $this->popSearch();
            }
            return false;
        }
        return true;
    }


    /**
     * Push search state to stack
     *
     * @return $this
     */
    protected function pushSearch()
    {
        array_unshift($this->searchStack, array(
            $this->currentSearchIndex,
            $this->currentSearchResult,
        ));
        $this->currentSearchIndex = 0;
        $this->currentSearchResult = array();

        return $this;
    }

    /**
     * Pop search state from stack
     *
     * @return $this
     */
    protected function popSearch()
    {
        $state = array_shift($this->searchStack);
        $this->currentSearchIndex = isset($state[0]) ? $state[0] : 0;
        $this->currentSearchResult = isset($state[1]) ? $state[1] : array();

        return $this;
    }

    /**
     * @return array
     */
    public function getPath()
    {
        return $this->currentPath;
    }

    /**
     * Set internal element pointer based on path id string.
     *
     * @param string $path String with array index-path separated by dot.
     * @return $this
     */
    public function setPath($path)
    {
        if (strlen($path) < 1) {
            return $this->resetRoot();
        }
        if ($this->currentPath === $path) {
            return $this;
        }
        $pathKeys = explode('.', $path);
        $tmp =& $this->getNodeReference($pathKeys, $this->elements);

        if (!$tmp) {
            return false;
        }
        $this->referencePointer =& $tmp;
        $this->currentPath      =  $path;

        return $this;
    }


    /**
     * Get array value by another array, using named key or index.
     * Root element needs special handling, since we're not accessing
     * through parent child-array.
     *
     * Ex. input:  array('level1', 0, 2, 'another-named-node', 4);
     *
     * @param   array $keys path to follow/get
     * @param         &$arrayRef
     * @return  array|bool
     */
    public function &getNodeReference($keys = array(), &$arrayRef = null)
    {
        $keys = is_array($keys) ? $keys : (array)$keys;

        if (isset($arrayRef)) {
            $arrayReference =& $arrayRef;
        } else {
            $arrayReference =& $this->referencePointer;
        }

        $index = function($n) use(&$arrayReference) {
            return reset(array_keys(array_slice($arrayReference, $n, 1, true)));
        };

        foreach ($keys as $key) {
            if (strlen($this->currentPath) > 0 && isset($arrayReference[self::CHILDREN])) {
                $tmp =& $arrayReference[self::CHILDREN];
            } else {
                $tmp =& $arrayReference;
            }

            $key = is_numeric($key) ? intval($key) : $index($key);

            if (empty($tmp[$key])) {
                return false;
            }
            $arrayReference =& $tmp[$key];
        }
        return $arrayReference;
    }

    /**
     * Reset to root
     *
     * @return $this [type] [description]
     */
    public function resetRoot()
    {
        $this->searchStack = array();
        $this->currentSearchResult = array();

        foreach ($this->elements as $i => $elem) {
            $this->currentSearchResult[] = $i;
        }

        $this->currentSearchIndex = 0;
        $this->currentPath = '';
        $this->referencePointer =& $this->elements;

        return $this;
    }

    /**
     * Adds element as sets it as current node
     *
     * @param string $name Element name, for example 'div'.
     * @return $this
     */
    public function addElement($name)
    {
        $this->pushSearch();
        $tmp =& $this->elements;

        if (!empty($this->referencePointer)) {
            $tmp =& $this->referencePointer[self::CHILDREN];
        }
        $name  = isset($this->elementAliases[$name]) ? $this->elementAliases[$name] : $name;
        $tmp[] = array($name, array(), array());

        $pathPrefix = '';
        if (strlen($this->currentPath) > 0) {
            $pathPrefix = $this->currentPath . '.';
        }

        for ($i = 0; $i < count($tmp); $i++) {
            $this->currentSearchResult[] =  $pathPrefix . $i;
        }

        $this->currentSearchIndex = count($tmp) - 1;
        return $this->current();
    }

    /**
     * Search by element name, only one level deep - current
     * node's children.
     *
     * @param  string $name Find child-element based on name
     * @return bool
     */
    public function findElement($name)
    {
        $this->pushSearch();
        $path = $this->getPath();
        $name = isset($this->elementAliases[$name]) ? $this->elementAliases[$name] : $name;

        if (strlen($path) > 0) {
            $tmp =& $this->referencePointer[self::CHILDREN];
        } else {
            $tmp =& $this->referencePointer;
        }

        foreach ($tmp as $index => &$childElement) {
            if ($childElement[self::ELEMENT_NAME] == $name) {
                $this->currentSearchResult[] = strlen($path) > 0 ? $path . '.' . $index : $index;
            }
        }

        if (empty($this->currentSearchResult)) {
            $this->popSearch();
            return false;
        }

        return $this->current();
    }

    /**
     * Search by id-name globally.
     *
     * @param $id
     * @return bool
     */
    public function findId($id)
    {
        $this->pushSearch();

        if (isset($this->ids[$id])) {
            foreach ((array)$this->ids[$id] as $idPath) {
                $this->currentSearchResult[] = $idPath;
            }
            return $this->current();
        }

        $this->popSearch();
        return false;
    }

    /**
     * Search on class-names on children recursively.
     *
     * @param $class
     * @return bool
     */
    public function findClass($class)
    {
        $this->pushSearch();

        if (isset($this->classes[$class])) {
            foreach ((array)$this->classes[$class] as $classPath) {
                if (substr($classPath, 0, strlen($this->currentPath)) === $this->currentPath) {
                    $this->currentSearchResult[] = $classPath;
                }
            }
            return $this->current();
        }

        $this->popSearch();
        return false;
    }

    /**
     * Move up one level in the tree.
     *
     * @return $this
     */
    public function up()
    {
        $keys  = explode('.', $this->currentPath);
        $count = count($keys);

        if ($count > 0) {
            unset($keys[$count - 1]);
            $count--;
        }
        $new = '';

        for ($i = 0; $i < $count; $i++) {
            $new .= ($i == 0) ? $keys[$i] : '.' . $keys[$i];
        }

        $this->setPath($new);
        $this->popSearch();

        return $this;
    }

    /**
     * Search for children, set pointer to first hit.
     *
     * @return $this
     */
    public function children()
    {
        $this->pushSearch();
        $children =& $this->referencePointer[self::CHILDREN];

        if (is_array($children)) {
            foreach ($children as $index => $child) {
                if (is_array($child)) {
                    $path = $this->getPath();
                    $this->currentSearchResult[] = (strlen($path) < 1) ? $index : $path . '.' . $index;
                }
            }
        }
        return $this;
    }

    /**
     * Set element text, only one possible child node can be string
     * others expected to be child-elements.
     *
     * @param $text
     * @return $this|bool
     */
    public function setText($text)
    {
        if ($this->referencePointer[self::ELEMENT_NAME] == '!--') {
            $this->referencePointer[self::ATTRIBUTES] = array($text);
            return $this;
        }

        if (in_array($this->referencePointer[self::ELEMENT_NAME], $this->selfClosing)) {
            return $this;
        }

        foreach ($this->referencePointer[self::CHILDREN] as &$child) {
            if (is_string($child)) {
                $child = $text;
                return $this;
            }
        }

        $this->referencePointer[self::CHILDREN][] = $text;
        return $this;
    }

    /**
     * Remove current element and children, move active
     * element to parent.
     *
     * @return $this
     */
    public function remove()
    {
        if ($myId = $this->getAttribute('id')) {
            foreach ($this->ids as $id => $paths) {
                if ($myId == $id) {
                    foreach ((array)$paths as $index => $idPath) {
                        if ($idPath == $this->currentPath) {
                            unset($paths[$index]);
                        }
                    }
                }
            }
        }

        foreach ($this->classes as $class => $paths) {
            $method = 'hasClass';

            if ($this->{$method}($class)) {
                foreach ((array)$paths as $index => $classPath) {
                    if ($classPath == $this->currentPath) {
                        unset($paths[$index]);
                    }
                }
            }
        }

        $tmp =& $this->referencePointer;
        $this->up();
        unset($tmp);

        return $this;
    }

    /**
     * Set attribute
     *
     * @param $name
     * @param $value
     * @return $this
     */
    public function setAttribute($name, $value)
    {
        $name = strtolower($name);
        $newValue = $value;

        if (in_array($name, array_keys($this->arrayAttributes))) {
            $values = array();
            if (isset($this->referencePointer[self::ATTRIBUTES][$name])) {
                $values = $this->referencePointer[self::ATTRIBUTES][$name];
            }
            $newValue = array_merge($values, (array)$value);
        }

        $this->referencePointer[self::ATTRIBUTES][$name] = $newValue;

        if ($name == 'id') {
            $this->ids[$value] = array($this->getPath());
        } else if ($name == 'class') {
            $classPath = $this->getPath();
            if (isset($this->classes[$value])) {
                $classPath = (array)$this->classes[$value];
                $classPath[] = $this->getPath();
            }
            $this->classes[$value] = $classPath;
        }

        return $this;
    }

    /**
     * Get attribute value.
     *
     * @param $name
     * @return bool
     */
    public function getAttribute($name)
    {
        $name = strtolower($name);

        if (isset($this->referencePointer[self::ATTRIBUTES][$name])) {
            return $this->referencePointer[self::ATTRIBUTES][$name];
        }

        return false;
    }

    /**
     * Get rendered attributes for element-tag.
     *
     * @return null|string
     */
    public function renderAttributes()
    {
        $attrb = null;

        foreach ($this->referencePointer[self::ATTRIBUTES] as $name => $value) {
            $name = is_int($name) ? '' : $name;

            if (isset($this->arrayAttributes[$name])) {
                $value = implode($this->arrayAttributes[$name], (array)$value);
            }

            if (isset($this->nullAttributes[$name])) {
                $value = '';
            } else if (!empty($name)) {
                $value = sprintf('="%s"', $value);
            }

            $attrb .= sprintf(' %s%s', $name, $value);
        }

        return $attrb;
    }

    /**
     * Render HTML from array
     *
     * @param int $indentLevel
     * @return string
     */
    public function render($indentLevel = 0)
    {
        if (strlen($this->currentPath) < 1) {
            $ret = '';
            foreach ($this->currentSearchResult as $path) {
                $this->current();
                $ret .= $this->render();
                $this->next();
            }
        }

        $attrb  = $this->renderAttributes();
        $elem   = $this->referencePointer[self::ELEMENT_NAME];
        $indent = str_repeat('    ', $indentLevel);
        $tagEnd = ' />' . PHP_EOL;

        if ($elem == '!--') {
            $tagEnd = ' -->' . PHP_EOL;
        }

        if (!$selfClosing = in_array($elem, $this->selfClosing)) {
            $childHTml = PHP_EOL;

            if (is_string($this->referencePointer[self::CHILDREN])) {
                $childHTml   = $this->referencePointer[self::CHILDREN];
                $childIndent = '';
            } else {
                $childIndent = str_repeat('    ', $indentLevel);

                foreach ($this->referencePointer[self::CHILDREN] as $child) {
                    if (is_string($child)) {
                        if (count($this->referencePointer[self::CHILDREN]) < 2) {
                            $childIndent = '';
                            $childHTml   = $child;
                        } else {
                            $childHTml  .= $childIndent . '    ' . $child . PHP_EOL;
                        }
                    }
                }
                /** @var self $child */
                foreach ($this->children() as $child) {
                    $childHTml .= $child->render($indentLevel + 1);
                }
            }

            $tagEnd = sprintf('>%s%s</%s>%s', $childHTml, $childIndent, $elem, PHP_EOL);
        }
        return sprintf('%s<%s%s%s', $indent, $elem, $attrb, $tagEnd);
    }

    /**
     * Magic method calls
     *
     * @param $method
     * @param $args
     * @return bool
     * @throws BadMethodCallException
     */
    public function __call($method, $args)
    {
        if (strlen($method) < 4) {
            throw new BadMethodCallException('Method HtmlTree::'.$method.' not found!');
        }

        if (substr($method, 6) === 'remove') {
            str_replace('remove', 'uns', $method);
        } elseif (substr($method, 6) === 'toggle') {
            str_replace('toggle', 'tgl', $method);
        }

        $name = strtolower(substr($method, 3));

        switch (substr($method, 0, 3)) {
            case 'set':
                $curr = reset($args);
                return call_user_func_array(array($this, 'setAttribute'), array($name, $curr));
                break;
            case 'get':
                if (in_array($name, $this->arrayAttributes)) {
                    $curr = explode($this->arrayAttributes[$name], $this->getAttribute($name));
                    $curr[] = reset($args);
                    $curr = implode($this->arrayAttributes[$name], $curr);
                } else {
                    $curr = reset($args);
                }

                return call_user_func_array(array($this, 'getAttribute'), array($name, $curr));
                break;
            case 'uns':
                if (!isset($this->referencePointer[self::ATTRIBUTES][$name])) {
                    break;
                }

                if (in_array($name, $this->arrayAttributes)) {
                    $curr = explode($this->arrayAttributes[$name], $this->getAttribute($name));
                    $i = 0;
                    foreach ($curr as $i => $val) {
                        if ($val == $name) {
                            break;
                        }
                    }
                    unset($curr[$i]);
                    $curr = implode($this->arrayAttributes[$name], $curr);

                    return call_user_func_array(array($this, 'setAttribute'), array($name, $curr));
                } else {
                    unset($this->referencePointer[self::ATTRIBUTES][$name]);
                }
                break;
            case 'tgl':
                $method = 'get' . ucfirst($name);

                if ($this->{$method}) {
                    $method = 'uns' . ucfirst($name);
                    return call_user_func_array(array($this, $method), array($name));
                } else {
                    $method = 'set' . ucfirst($name);
                    return call_user_func_array(array($this, $method), array_merge(array($name), $args));
                }
            case 'has':
                $method = 'get' . ucfirst($name);
                return (bool)$this->{$method};
            case 'add':
            default:
                if (in_array($name, array_keys($this->arrayAttributes))) {
                    $method = 'set' . ucfirst($name);
                    return call_user_func_array(array($this, $method), $args);
                }
                return $this->addElement($name);
                break;
        }
        return false;
    }
}