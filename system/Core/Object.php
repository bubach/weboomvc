<?php
/**
 * Core_Object
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Object
{

    /**
     * Data array for getter & setter
     *
     * @var array
     */
    protected $_data = array();

    /**
     * Class constructor.
     *
     * @access  public
     */
    public function __construct($data = array())
    {
        if (!empty($data)) {
            $this->setData($data);
        }
    }

    /**
     * Set data in the object. If $name is an array all
     * attribute values will be overwritten.
     *
     * @param  string|array $name
     * @param  mixed $value
     * @return Core_Model_Base
     */
    public function setData($name, $value = null)
    {
        if (is_array($name)) {
            $this->_data = $name;
        } else {
            $this->_data[$name] = $value;
        }
        return $this;
    }

    /**
     * Get data from the object or even from child objects,
     * with simplified 'a/b/c' access for ['a']['b']['c']
     *
     * @param  string          $name
     * @param  string          $separator
     * @return string | array  $data
     */
    public function getData($name = null, $separator = '/')
    {
        if (empty($name)) {
            return $this->_data;
        }
        if (!is_string($separator)) {
            return null;
        }

        if (strpos($name, $separator)) {
            $keyArr = explode($separator, $name);
            $data   = $this->_data;

            foreach ($keyArr as $i => $k) {
                if (empty($k)) {
                    return null;
                }

                if (is_array($data)) {
                    if (!isset($data[$k])) {
                        return null;
                    }
                    $data = $data[$k];
                } elseif ($data instanceof Core_Object) {
                    $data = $data->getData($k);
                } else {
                    return null;
                }
            }
            return $data;
        }

        return isset($this->_data[$name]) ? $this->_data[$name] : null;
    }

    /**
     * Unset data from the object. If $name is null all
     * model values will be removed.
     *
     * @param  string $name
     * @return Core_Model_Base
     */
    public function unsetData($name = null)
    {
        if (is_null($name)) {
            $this->_data = array();
        } else {
            unset($this->_data[$name]);
        }
        return $this;
    }

    /**
     * Set/Get attribute wrapper, will assume
     *
     * @param   string $method
     * @param   array $args
     * @return  mixed
     */
    public function __call($method, $args)
    {
        $first = substr($method, 0, 3);

        if ($first == 'set') {
            $name = $this->_underscore(substr($method, 3));
            return $this->setData($name, isset($args[0]) ? $args[0] : null);
        } else if ($first == 'get') {
            $name = $this->_underscore(substr($method, 3));
            return $this->getData($name);
        }
    }

    /**
     * Converts field names for setters and getters
     *
     * @param  string $name
     * @return string
     */
    protected function _underscore($name)
    {
        static $_cache = array();

        if (isset($_cache[$name])) {
            return $_cache[$name];
        }

        $result = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $name));
        return $_cache[$name] = $result;
    }

    /**
     * Attribute getter
     *
     * @param  string $name
     * @return mixed
     */
    public function __get($name)
    {
        $name = $this->_underscore($name);
        return $this->getData($name);
    }

    /**
     * Attribute setter
     *
     * @param  string $name
     * @param  mixed  $value
     * @return $this
     */
    public function __set($name, $value)
    {
        $name = $this->_underscore($name);
        return $this->setData($name, $value);
    }

    /**
     * Check for data from the object.
     *
     * @param  string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->_data[$name]);
    }

    /**
     * Unset data from the object.
     *
     * @param  string $name
     */
    public function __unset($name)
    {
        unset($this->_data[$name]);
    }

    /**
     * Get class instance
     *
     * @param  string $name
     * @param  string $alias
     * @param  array  $params
     * @return object
     */
    public function getObject($name, $alias = null, $params = null)
    {
        return wmvc::app()->getObject($name, $alias, $params);
    }

    /**
     * Get MVC config value
     *
     * @param  string $path
     * @return mixed
     */
    public function getConfig($path = '')
    {
        return wmvc::app()->getConfig($path);
    }

    /**
     * Set MVC config value
     *
     * @param  string $path
     * @param  mixed  $value
     * @return bool
     */
    public function setConfig($path, $value)
    {
        return wmvc::app()->setConfig($path, $value);
    }
}
