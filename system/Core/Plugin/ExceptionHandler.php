<?php
/**
 * Core_Plugin_ExceptionHandler
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_ExceptionHandler extends ErrorException
{

    /**
     * printException
     *
     * @access  public
     */
    public static function printException($e)
    {
        switch ($e->getCode()) {
            case E_ERROR:
                $code_name = 'E_ERROR';
            break;
            case E_WARNING:
                $code_name = 'E_WARNING';
            break;
            case E_PARSE:
                $code_name = 'E_PARSE';
            break;
            case E_NOTICE:
                $code_name = 'E_NOTICE';
            break;
            case E_CORE_ERROR:
                $code_name = 'E_CORE_ERROR';
            break;
            case E_CORE_WARNING:
                $code_name = 'E_CORE_WARNING';
            break;
            case E_COMPILE_ERROR:
                $code_name = 'E_COMPILE_ERROR';
            break;
            case E_COMPILE_WARNING:
                $code_name = 'E_COMPILE_WARNING';
            break;
            case E_USER_ERROR:
                $code_name = 'E_USER_ERROR';
            break;
            case E_USER_WARNING:
                $code_name = 'E_USER_WARNING';
            break;
            case E_USER_NOTICE:
                $code_name = 'E_USER_NOTICE';
            break;
            case E_STRICT:
                $code_name = 'E_STRICT';
            break;
            case E_RECOVERABLE_ERROR:
                $code_name = 'E_RECOVERABLE_ERROR';
            break;
            default:
                $code_name = $e->getCode();
            break;
        }
        ?>
        <link rel="stylesheet" href="http://highlightjs.org/static/styles/monokai_sublime.css">
        <script src="http://highlightjs.org/static/highlight.pack.js"></script>
        <script>
            hljs.configure({tabReplace: '    '});
            hljs.initHighlightingOnLoad();
        </script>
        <span style="font-family: verdana; text-align: left; background-color: #fcc; border: 1px solid #600; color: #600; display: block; margin: 1em 0; padding: .33em 6px">
            <b>Error:</b> <?php echo $code_name; ?><br />
            <b>Message:</b> <?php echo $e->getMessage(); ?><br />
            <b>File:</b> <?php echo $e->getFile(); ?><br />
            <b>Line:</b> <?php echo $e->getLine(); ?><br />
            <b>Trace:</b> <pre><?php echo $e->getTraceAsString(); ?></pre>
        </span>
        <?php
        echo self::source($e->getFile(), $e->getLine());
    }

    /**
     * handleException
     *
     * @access  public
     */
    public static function handleException($e)
    {
        return self::printException($e);
    }

    /**
     *  handleError
     *  A simple exception handler to display exceptions in a formatted box.
     *
     * @access  public
     */
    public static function handleError($errno, $errstr, $errfile, $errline)
    {
        // do nothing if error reporting is turned off
        if (error_reporting() === 0) {
            return;
        }
        // be sure received error is supposed to be reported
        if (error_reporting() & $errno) {
            throw new Core_Plugin_ExceptionHandler($errstr, $errno, $errno, $errfile, $errline);
        }
    }

    /**
     * Fetch and HTML highlight several lines of a file.
     *
     * @param string $file to open
     * @param integer $number of line to highlight
     * @param integer $padding of lines on both side
     * @return string
    */
    public static function source($file, $number, $padding = 5)
    {
        // Get lines from file
        $lines = array_slice(file($file), $number - $padding - 1, $padding * 2 + 1, true);
        $html = '<pre><code>';
        foreach ($lines as $i => $line) {
            $html .= '<b>' . sprintf('%' . mb_strlen($number + $padding) . 'd', $i + 1) . '</b> '
                . ($i + 1 == $number ? '<em style="background:#555">' . self::h($line) . '</em>' : self::h($line));
        }
        return $html.'</code></pre>';
    }

    /**
     * format code output
     *
     * @param string $code line
     * @return string
     */
    public static function h($code)
    {
        return htmlspecialchars($code, ENT_QUOTES, 'utf-8');
    }

    /**
     * Fetch a backtrace of the code
     *
     * @param int $offset to start from
     * @param int $limit of levels to collect
     * @return array
     */
    public static function backtrace($offset, $limit = 5)
    {
        $trace = array_slice(debug_backtrace(), $offset, $limit);

        foreach ($trace as $i => &$v) {
            if (!isset($v['file'])) {
                unset($trace[$i]);
                continue;
            }
            $v['source'] = self::source($v['file'], $v['line']);
        }

        return $trace;
    }
}
