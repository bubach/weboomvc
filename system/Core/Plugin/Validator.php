<?php
/**
 * BootForm_Model_Elements_HelpBlock
 *
 * based on validfluent
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Validator
{

    /** @var bool  */
    public $isValid = true;

    /** @var bool  */
    public $isGroupValid = true;

    /** @var array  */
    public $validFields = array();

    /** @var   */
    private $currentField;


    private static $error_required      = 'This field is required';
    private static $error_date          = 'Please enter a date in the YYYY-MM-DD format';
    private static $error_email         = 'Please enter a valid email';
    private static $error_url           = 'Please enter a valid url';
    private static $error_alfa          = 'Only letters and numbers are permited';
    private static $error_text          = 'Only letters are permited';
    private static $error_minSize       = 'Please enter more than %s characters';
    private static $error_maxSize       = 'Please enter less than %s characters';
    private static $error_numberFloat   = 'Only numbers are permitted';
    private static $error_numberInteger = 'Only numbers are permitted';
    private static $error_numberMax     = 'Please enter a value lower than %s ';
    private static $error_numberMin     = 'Please enter a value greater than %s ';
    private static $error_oneOf         = 'Please choose one of " %s "';
    private static $error_equal         = 'Fields did not match';
    private static $error_regex         = 'Please choose a valid value';

    // some regEx's
    private $pattern_email         = '/^([a-zA-Z0-9_\+\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/';
    private $pattern_url           = '/^((http|ftp|https):\/\/)?www\..*.\.\w\w\w?(\/.*)?(\?.*)?$/'; //check...
    private $pattern_alfa          = '/^(\d|\-|_|\.| |(\p{L}\p{M}*))+$/u';
    private $pattern_text          = '/^( |(\p{L}\p{M}*)+$/u';
    private $pattern_numberInteger = '/^[\+\-]?[0-9]+$/';
    private $pattern_numberFloat   = '/^[\+\-]?[0-9\.]+$/';
    private $pattern_date          = '/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/';

    /**
     *
     * @param Array $post  ($Key => $value) array
     */
    function __construct($post)
    {
        foreach ($post as $key => $value) {
            $this->validFields[$key] = array('value' => trim($value), 'error' => '');
        }
    }

    /**
     * Helper: returns true if last validation passed , else FALSE
     *
     * @return Boolean
     */
    function isValid()
    {
        return $this->isValid;
    }

    /**
     * Helper: returns true if all validations passed , else FALSE
     *
     * @return Boolean
     */
    function isGroupValid()
    {
        return $this->isGroupValid;
    }

    /**
     * Return's $name validation error
     *
     * @param string $name
     * @return string the error
     */
    function getError($name)
    {
        if (isset($this->validFields[$name])) {
            return $this->validFields[$name]['error'];
        }
        return '';
    }

    /**
     * Returs $name value
     *
     * @param string $name
     * @return string the value
     */
    function getValue($name)
    {
        if (isset($this->validFields[$name])) {
            return $this->validFields[$name]['value'];
        }
        return '';
    }

    /**
     * Used to set starting values on Form data
     * ex: $valid->name('user_name)->setValue($database->getUserName() );
     *
     * @param  string $value
     * @return $this
     */
    function setValue($value)
    {
        $this->currentField['value'] = $value;
        return $this;
    }

    /**
     *  used to set error messages out of the scope of validFluent
     *  ex: $valid->name('user_name')->setError('The Name "Andre" is already taken , please try another')
     *
     *  @param string $error
     *  @return $this
     */
    function setError($error)
    {
        $this->currentField['error'] = $error;
        $this->isGroupValid          = false;
        $this->isValid               = false;
        return $this;
    }

    /**
     * PRIVATE Helper to set error messages
     *
     * @param string $errorMsg custom error message
     * @param string $default  default error message
     * @param string $params   extra parameter to default error message
     */
    private function setErrorMsg($errorMsg, $default, $params = null)
    {
        $this->isGroupValid = false;
        if (empty($errorMsg)) {
            $this->currentField['error'] = sprintf($default, $params);
        } else {
            $this->currentField['error'] = $errorMsg;
        }
    }

    /**
     * Used to set a pointer for current validation object
     * if $name does'nt exits, it will be created with a empty value
     * 		note:validation always pass on empty not required fields
     *
     * @param string $name as in array($name => 'name value')
     * @return ValidFluent
     */
    function name($name)
    {
        if (!isset($this->validFields[$name])) {
            $this->validFields[$name] = array('value' => '', 'error' => '');
        }

        $this->isValid      = true;
        $this->currentField = &$this->validFields[$name];
        return $this;
    }

    /**
     * Note if field is required , then it must me called right after name!!
     * ex: $valid->name('user_name')->required()->text()->minSize(5);
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function required($errorMsg = null)
    {
        if ($this->isValid) {
            $this->isValid = ($this->currentField['value'] != '') ? true : false;
            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_required);
            }
        }
        return $this;
    }

    /**
     *  validates a Date in yyyy-mm-dd format
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function date($errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($this->pattern_date, $this->currentField['value'])) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_date);
            }
        }
        return $this;
    }

    /**
     * validates an email address
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function email($errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($this->pattern_email, $this->currentField['value']) > 0) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_email);
            }
        }
        return $this;
    }

    /**
     * validates a URL address
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function url($errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($this->pattern_url, $this->currentField['value'])) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_url);
            }
        }
        return $this;
    }

    /**
     * ex: ->regex('/^[^<>]+$/', 'ERROR:  < and > arent valid characters')
     *
     * @param string $regex a regular expresion '/regex/'
     * @param string $errorMsg
     * @return ValidFluent
     */
    function regex($regex, $errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($regex, $this->currentField['value'])) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_regex);
            }
        }
        return $this;
    }

    /**
     *  ex: ->name('password')->equal('passwordConfirm' , 'passwords didnt match')
     *
     * @param string $value2
     * @param string $errorMsg
     * @return ValidFluent
     */
    function equal($value2, $errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = ($value2 == $this->currentField['value']);

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_equal);
            }
        }
        return $this;
    }

    /**
     * Ex: ->oneOf('blue:red:green' , 'only blue , red and green permited')
     *
     * *case insensitive*
     * @param string $items ex: 'blue:red:green'
     * @param string $errorMsg
     * @return ValidFluent
     */
    function oneOf($items, $errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $item = explode(':', strtolower($items));
            $result = array_intersect($item, array(strtolower($this->currentField['value'])));
            $this->isValid = (!empty($result));

            if (!$this->isValid) {
                $itemsList = str_replace(':', ' / ', $items);
                $this->setErrorMsg($errorMsg, self::$error_oneOf, $itemsList);
            }
        }
        return $this;
    }

    /**
     * Only allows A-Z a-Z and space
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function text($errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($this->pattern_text, $this->currentField['value'])) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_text);
            }
        }
        return $this;
    }

    /**
     * Only allows A-Z a-z 0-9 space and ( - . _ )
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function alpha($errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($this->pattern_alfa, $this->currentField['value'])) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_alfa);
            }
        }
        return $this;
    }

    /**
     * @param int $size the maximum string size
     * @param string $errorMsg
     * @return ValidFluent
     */
    function maxSize($size, $errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (strlen($this->currentField['value']) <= $size);

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_maxSize, $size);
            }
        }
        return $this;
    }

    /**
     * @param int $size the minimum string size
     * @param string $errorMsg
     * @return ValidFluent
     */
    function minSize($size, $errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (strlen($this->currentField['value']) >= $size);

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_minSize, $size);
            }
        }
        return $this;
    }

    /**
     *  checks if its a float ( +  -  . ) permited
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function numberFloat($errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($this->pattern_numberFloat, $this->currentField['value'])) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_numberFloat);
            }
        }
        return $this;
    }

    /**
     *  checks if its a integer ( +  - ) permited
     *
     * @param string $errorMsg
     * @return ValidFluent
     */
    function numberInteger($errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = (preg_match($this->pattern_numberInteger, $this->currentField['value'])) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_numberInteger);
            }
        }
        return $this;
    }


    /**
     * @param number $max
     * @param string $errorMsg
     * @return ValidFluent
     */
    function numberMax($max, $errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = ($this->currentField['value'] <= $max) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_numberMax, $max);
            }
        }
        return $this;
    }

    /**
     * @param number $min
     * @param string $errorMsg
     * @return ValidFluent
     */
    function numberMin($min, $errorMsg = null)
    {
        if ($this->isValid && (!empty($this->currentField['value']))) {
            $this->isValid = ($this->currentField['value'] >= $min) ? true : false;

            if (!$this->isValid) {
                $this->setErrorMsg($errorMsg, self::$error_numberMin, $min);
            }
        }
        return $this;
    }

}