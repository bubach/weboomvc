<?php
/**
 * Core_Plugin_Request
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Request
{

    /**
     * sorted array of routers
     *
     * @var array
     */
    public $routers = array();

    /**
     * routing response array
     *
     * @var array
     */
    public $routeResponse = array();

    /**
     * constructor for request handling
     *
     * @access  public
     */
    public function __construct()
    {
        $this->setupRequest();

        $this->routers = wmvc::app()->getConfig('Routes/handlers');
        wmvc::app()->config->sortConfigArray($this->routers);
    }

    /**
     * setup request configuration
     *
     * @access public
     */
    public function setupRequest()
    {
        $url       = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'];
        $request   = array("fullUrl" => $url.$_SERVER['REQUEST_URI']);
        $request  += array("baseUrl" => $url);

        $request  += array("fullPath" => $_SERVER['REQUEST_URI']);
        $tmpUrl    = explode("?", $request['fullPath'], 2);
        $urlNoGet  = explode("?", $tmpUrl[0], 2);

        $request  += array("path" => $urlNoGet[0]);
        $request  += array("appPath" => rtrim(dirname($_SERVER['SCRIPT_NAME']), '/'));

        $request  += array("appUrl" => $request['baseUrl'].$request['appPath']);
        $request  += array("url" => $request['baseUrl'].$request['path']);
        $request  += array("virtualPath" => str_replace($request['appPath'], "", $request['path']));

        $request  += array("documentRoot" => realpath($_SERVER['DOCUMENT_ROOT']));
        $request  += array("GET" => $_GET);
        $request  += array("POST" => $_POST);

        wmvc::app()->setConfig('Runtime/Request', $request);
    }

    /**
     * Setup controller and action
     *
     * @access  public
     */
    public function dispatchRequest()
    {
        $app = wmvc::app();
        $this->runHandlers();

        $controllerName = $app->getConfig('Runtime/Request/controller');
        $actionName     = $app->getConfig('Runtime/Request/action');

        $app->controller = $app->getObject($controllerName, 'controller');
        $app->action     = $actionName;

        if (!empty($this->routeResponse['url'])) {
            $app->controller->getResponse()->setUrlRedirect($this->routeResponse['url'], $this->routeResponse['code']);
        }
        if (!empty($this->routeResponse['code'])) {
            $app->controller->getResponse()->setHttpResponseCode($this->routeResponse['code'], $this->routeResponse['message']);
        }
    }

    /**
     * Check each route-handler for a url match.
     * use base controller & index action as fallback
     *
     * @throws Exception
     */
    public function runHandlers()
    {
        if (!$this->routeResponse = $this->cleanUrl()) {
            $this->routeResponse = array();

            foreach ($this->routers as $router => $sort) {
                $routeHandler        = wmvc::app()->getObject($router);
                $this->routeResponse = $routeHandler->urlMatch(wmvc::app()->getConfig('Runtime/Request'));

                if ($this->checkMatch()) {
                    break;
                }
            }
        }

        if (empty($this->routeResponse['controller'])) {
            $this->routeResponse['controller'] = "Core_Controller_Base";
            $this->checkMatch();
        }
    }

    /**
     * Strip extra or trailing slashes from URL
     *
     * @return array|null
     */
    public function cleanUrl()
    {
        $request = wmvc::app()->getConfig('Runtime/Request');

        if ($request['path'] == "/" || $request['virtualPath'] == "/") {
            return null;
        }

        $stripped = rtrim(preg_replace('~/+~', '/', $request['path']), "/");
        if ($stripped == $request['path']) {
            $stripped = rtrim(preg_replace('~/+~', '/', $request['virtualPath']), "/");
        }

        if ($stripped !== $request['virtualPath']) {
            return array(
                "code"    => 301,
                "message" => "Redirecting to URL without extra slash(es)",
                "url"     => $this->_getStrippedUrl($request, $stripped),
            );
        }
        return null;
    }

    /**
     * Assemble URL after stripping extra slash(es)
     *
     * @param $request
     * @param string $strippedPath
     * @return string
     */
    private function _getStrippedUrl($request, $strippedPath)
    {
        $get = wmvc::app()->getConfig('Runtime/Request/GET');

        if (!empty($get)) {
            $get = "?".http_build_query($get);
        } else {
            $get = "";
        }

        return trim($request['baseUrl'].$strippedPath, '/').$get;
    }

    /**
     * Check matched module and controller
     *
     * @return bool
     */
    public function checkMatch()
    {
        $app = wmvc::app();

        if (empty($this->routeResponse['action'])) {
            $this->routeResponse['action'] = "index";
        }
        if (empty($this->routeResponse['controller'])) {
            return false;
        }

        $moduleName     = explode('_', $this->routeResponse['controller']);
        $controllerName = "";
        if (count($moduleName) > 2) {
            $controllerName = $moduleName[2];
        }
        $moduleName = $moduleName[0];
        $actionName = $this->routeResponse['action'];
        $nameSpace  = $app->load->getNamespace($moduleName);

        if (!$app->getConfig('Modules/'.$nameSpace.'/'.$moduleName.'/Config/active')) {
            $this->routeResponse['controller'] = "";
            $this->routeResponse['message']    = "Controller for this route not found!";
            $this->routeResponse['code']       = 404;
            return false;
        }

        $app->setConfig('Runtime/Request/namespace', $nameSpace);
        $app->setConfig('Runtime/Request/module', $moduleName);
        $app->setConfig('Runtime/Request/controller', $this->routeResponse['controller']);
        $app->setConfig('Runtime/Request/action', $actionName);

        if ($app->getConfig('Runtime/Request/layoutHandle') == null) {
            $app->setConfig('Runtime/Request/layoutHandle', $moduleName.'_'.$controllerName.'_'.$actionName);
        }
        return true;
    }
}
