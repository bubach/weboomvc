<?php
/**
 * Core_Plugin_Config
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Config
{

    /** @var array Config values */
    private $_config = array();

    /**
     * Constructor, detect if we have merged config caches
     * if not, do full re-merge.
     *
     * @access  public
     */
    public function __construct()
    {
        $this->scan();

        if (empty($this->_config)) {
            $this->load();
        }
    }

    /**
     * Get configuration value for specified config path
     *
     * @param   string $path
     * @return  array|null
     */
    public function get($path)
    {
        $pieces = explode('/', $path);

        if (count($pieces) > 0 && !empty($path)) {
            $configLocation =& $this->_config;

            foreach ($pieces as $key) {
                if (!array_key_exists($key, $configLocation)) {
                    return null;
                }
                $configLocation =& $configLocation[$key];
            }

            return $configLocation;
        } else {
            return $this->_config;
        }
    }

    /**
     * Set config value to specified path.
     *
     * @param  string $path
     * @param  $value
     * @return bool
     */
    public function set($path, $value)
    {
        $pieces = explode('/', $path);

        if (count($pieces) > 0) {
            $configLocation =& $this->_config;

            foreach ($pieces as $key) {
                if (!array_key_exists($key, $configLocation)) {
                    $configLocation[$key] = array();
                }
                $configLocation =& $configLocation[$key];
            }

            $configLocation = $value;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sorts a multidimensional array based on child
     * key values. Expects child named 'before' and 'after'.
     * Values being sibling-keys or '-' for all.
     *
     * @param array $array
     * @param array $before
     * @param array $after
     */
    public function sortConfigArray(&$array, $before = array('before'), $after = array('after'))
    {
        $isBefore = function($arrRef) use ($before) {
            return $this->getArrayValue($arrRef, $before);
        };

        $isAfter = function($arrRef) use ($after) {
            return $this->getArrayValue($arrRef, $after);
        };

        uasort($array, function ($a, $b) use ($isAfter, $isBefore) {
            if (
                $isAfter($a)  == '-' || $isBefore($b) == '-' ||
                $isBefore($a) == key($b) || $isAfter($b)  == key($a)
            ) {
                return 1;
            } else if (
                $isBefore($a) == '-' || $isAfter($b)  == '-' ||
                $isAfter($a)  == key($b) || $isBefore($b) == key($a)
            ) {
                return -1;
            } else {
                return 0;
            }
        });
    }

    /**
     * Save config value to merged and original file.
     * 
     * @access  public
     */
    public function save()
    {
        //...
    }

    /**
     * Load & merge in module config files.
     * 
     * @access  public
     */
    public function load()
    {
        $mvcPaths = explode(PATH_SEPARATOR, wmvc::app()->getAppPaths());
        $configFiles = array();

        foreach ($mvcPaths as $path) {
            $dir = opendir($path);

            while (($currentFile = readdir($dir)) !== false) {
                $configPath = $path . $currentFile . DS . 'Config' . DS . 'Config.php';

                if (is_file($configPath)) {
                    $configFiles[] = $configPath;
                }
            }
            closedir($dir);
        }

        foreach ($configFiles as $file) {
            $newConfig = include($file);

            if ($this->getArrayValue($newConfig, array('Modules', 0, 0, 'Config', 'active'))) {
                $this->_config = array_replace_recursive($this->_config, $newConfig);
            } else {
                throw new Exception("Syntax error in config file: '$file'!");
            }
        }
    }

    /**
     * Get array value by another array
     *
     * @access  public
     * @param   array $array
     * @param   array $keys source array
     * @return  array|bool
     */
    public function getArrayValue(&$array, $keys = array())
    {
        if (is_array($array) && is_array($keys)) {
            $arrayReference = &$array;

            foreach ($keys as $val) {
                if (is_int($val)) {
                    $arrKeys = array_keys($arrayReference);
                }
                if (!empty($arrayReference[$val])) {
                    $arrayReference =& $arrayReference[$val];
                } else if (is_int($val) && !empty($arrKeys) && !empty($arrayReference[$arrKeys[$val]])) {
                    $arrayReference =& $arrayReference[$arrKeys[$val]];
                } else {
                    return false;
                }
            }
            return $arrayReference;
        }
        return false;
    }

    /**
     * Scan config files for changes since last merge.
     *
     * @access  public
     */
    public function scan()
    {
        //...
    }
}
