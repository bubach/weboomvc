<?php
/**
 * Core_Plugin_Routing
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Routing
{

    /**
     * route array with translated regex values
     * @var array
     */
    private $_routesRegex = array();

    /**
     * Class constructor, set up runtime info and sorts
     * regex routes based on length. More complex patterns
     * will get checked first.
     *
     * @access  public
     */
    public function __construct()
    {
        $request   = wmvc::app()->getConfig('Runtime/Request');
        $tmpRoutes = wmvc::app()->getConfig('Routes/default');

        if (is_array($tmpRoutes)) {
            foreach ($tmpRoutes as $pattern => $action) {
                $this->_routesRegex += array($this->parseUrlPattern($pattern) => $action);
            }

            $keys = array_map('strlen', array_keys($this->_routesRegex));
            array_multisort($keys, SORT_DESC, $this->_routesRegex);

            preg_match_all('#[/?&]([\w.()-]+)([/=](\d[\w.()-]+|\d))?#', $request['virtualPath'], $parts);
            if (count($parts[1]) >= 1 && count($parts[3]) >= 1) {
                $request += array('params' => array_combine($parts[1], $parts[3]));
            }

            wmvc::app()->setConfig('Runtime/Request', $request);
        }
    }

    /**
     *  match URL against routes from config or fallback
     *  to checking module controllers manually.
     *
     *  @access public
     *  @param  array $request
     *  @return array
     */
    public function urlMatch($request)
    {
        $result = array();

        foreach ($this->_routesRegex as $key => $value) {

            if (preg_match($key, $request['virtualPath'])) {
                $result += array('code' => '', 'url' => '');
                $tmp     = explode('/', $value);
                $result += array("controller" => $tmp[0]);

                if (count($tmp) > 1) {
                    $result += array('action' => $tmp[1]);
                }
            }
        }

        if (empty($result)) {
            $result = array('controller' => '', 'action' => '', 'code' => 404, 'message' => 'Route not found!');
        }

        return $result;
    }

    /**
     * Parse an URL pattern into regex valid format
     *
     * @access public
     * @param  string
     * @return string
     */
    public function parseUrlPattern($pattern)
    {
        $pattern = preg_quote($pattern, '/');
        $pattern = preg_replace('/\\\{d\\\}/', '(\d+)', $pattern);
        $pattern = preg_replace('/\\\{c\\\}/', '(\w+)', $pattern);
        $pattern = preg_replace('/\\\{s\\\}/', '(.+)', $pattern);

        if (substr($pattern, 0, 2) != '/^') {
            $pattern = '/^'. $pattern;
        }
        if (substr($pattern, -2) != '$/') {
            $pattern = $pattern . '$/';
        }

        return $pattern;
    }

}