<?php
/**
 * Core_Plugin_Setup
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Setup
{

    /**
     * runs setup for specified module
     *
     * @param  $nameSpace
     * @param  $moduleName
     * @return false|null
     * @throws Exception
     */
    public function runSetup($nameSpace, $moduleName)
    {
        $version    = wmvc::app()->getConfig('Modules/'.$nameSpace.'/'.$moduleName.'/Config/version');
        $setupFiles = $this->getSetupFiles($nameSpace, $moduleName);

        if (empty($setupFiles) || empty($version)) {
            return false;
        }
        usort($setupFiles, "version_compare");

        $db           = wmvc::app()->getDb();
        $combinedName = $nameSpace."_".$moduleName;
        $this->setupCoreTables($db);

        if ($moduleName !== "Core") {
            $schemaInfo   = $db->from('schema_version')->where('name', $combinedName)->fetch();

            if ($schemaInfo['version'] !== $version) {
                foreach ($setupFiles as $setupFile) {
                    $this->runSetupFile($setupFile, $schemaInfo, $version, $combinedName, $db);
                }
            }
        }
    }

    /**
     * Run individual setup-file and update db
     *
     * @param  $setupFile
     * @param  $schemaInfo
     * @param  $version
     * @param  $combinedName
     * @param  $db
     * @throws Exception
     */
    private function runSetupFile($setupFile, $schemaInfo, $version, $combinedName, $db)
    {
        if (preg_match("/((\.?\d)+)(\.php)$/", $setupFile, $tmpArr)) {
            $fileVersion = $tmpArr[1];
        } else {
            $fileVersion = "1.0.0";
        }

        if (version_compare($version, $fileVersion) <= 0) {
            require $setupFile;

            if ($schemaInfo) {
                $db->update('schema_version', array('version'=>$fileVersion))
                    ->where('name', $combinedName)->execute();
            } else {
                $data = array('name' => $combinedName, 'version' => $fileVersion);
                $db->insertInto('schema_version', $data)->execute();
            }
        }
    }

    /**
     * Get array of setup files to run
     *
     * @param $nameSpace
     * @param $moduleName
     * @return array|bool
     */
    public function getSetupFiles($nameSpace, $moduleName)
    {
        $filePath   = WMVC_BASEDIR . strtolower($nameSpace) . DS . $moduleName . DS . 'Config';
        $setupFiles = array();
        $dir        = opendir($filePath);

        while (($currentFile = readdir($dir)) !== false) {
            if (fnmatch("Setup*.php", $currentFile)) {
                $setupFiles[] = $filePath.DS.$currentFile;
            }
        }
        closedir($dir);

        return empty($setupFiles) ? false : $setupFiles;
    }

    /**
     * setupCoreTables
     *
     * set up any tables needed for the core framework,
     * in the future - read config for optional no-db setup.
     */
    private function setupCoreTables($db)
    {
        try {
            $db->from("schema_version")->select(1)->limit(1)->fetchColumn();
        } catch (Exception $e) {
            $table = array(
                'schema_version' => array(
                    'id'       => array('type' => 'primary'),
                    'name'     => array('type' => 'string', 'length' => 100),
                    'version'  => array('type' => 'string', 'length' => 10),
                    'created'  => array('type' => 'datetime'),
                    'modified' => array('type' => 'datetime')
                )
            );
            $db->schema()->createTable($table);
        }
    }
}
