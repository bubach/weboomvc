<?php
/**
 * Core_Plugin_Event
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Event
{

    /**
     * system wide events array in format:
     *
     * array("event_name" =>
     *     array("class_name_path" =>
     *         array("method"=>"","before"=>"-","after"=>"")
     *     )
     * );
     *
     * @var array
     */
    private $_events = array();

    /**
     * class constructor
     *
     * @access  public
     */
    public function __construct()
    {
        $this->_events = wmvc::app()->getConfig('Events');
    }

    /**
     * Trigger an event with custom data attached
     *
     * @access   public
     */
    public function triggerEvent($name, $data = array())
    {
        $handlers          = wmvc::app()->getConfig('Events/'.$name);
        $data["eventName"] = $name;

        if (!empty($handlers)) {

            // first sort events by before & after options
            wmvc::app()->config->sortConfigArray($handlers);

            foreach ($handlers as $handler => $handlerData) {
                $class = $handler;

                if (isset($handlerData["method"])) {
                    $method = $handlerData["method"];
                } else {
                    $method = "handler";
                }
                $observer = wmvc::app()->getObject($class);
                $observer->{$method}($data);
            }
        }
    }

    /**
     * Add an observer to the internal array,
     * will not save to configuration.
     *
     * @param    $eventName
     * @param    $info
     */
    public function addObserver($eventName, $info)
    {
        $this->_events[$eventName][] = $info;
    }
}
