<?php
/**
 * Core_Plugin_Profiling
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Profiling
{

    /**
     * init profiling
     *
     * @access public
     */
    public static function initProfiling()
    {
        self::timer('wmvc_app_start');
        self::ram('wmvc_app_start');
    }

    /**
     * stop profiling and insert result into html output
     *
     * @return mixed|string
     */
    public static function stopProfiling()
    {
        self::timer('wmvc_app_end');
        self::ram('wmvc_app_end');

        if (wmvc::app()->getConfig('Runtime/Timer') || wmvc::app()->getConfig('Runtime/Ram')) {
            $output = wmvc::app()->controller->getResponse()->getBody();

            $output = str_replace('{{time}}', sprintf('%0.5f', self::timer('wmvc_app_start', 'wmvc_app_end')), $output);
            $output = str_replace('{{memory}}', sprintf('%s', self::ram('wmvc_app_start', 'wmvc_app_end')), $output);

            wmvc::app()->controller->getResponse()->setBody($output);
        }
    }

    /**
     * get/set timer values
     *
     * @access  public
     * @param   string  $id the timer id to set (or compare with $id2)
     * @param   string  $id2 the timer id to compare with $id
     * @return  float   difference of two times
     */
    public static function timer($id = null, $id2 = null)
    {
        static $times = array();

        if ($id !== null && $id2 !== null) {
            return (isset($times[$id]) && isset($times[$id2])) ? ($times[$id2] - $times[$id]) : false;
        } elseif ($id !== null) {
            return $times[$id] = microtime(true);
        }

        return false;
    }

    /**
     * get/set ram usage value
     *
     * @access  public
     * @param   string  $id the ram id to set (or compare with $id2)
     * @param   string  $id2 the ram id to compare with $id
     * @return  string  formatted difference of usage
     */
    public static function ram($id = null, $id2 = null)
    {
        static $ram  = array();
        static $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');

        if ($id !== null && $id2 !== null) {
            $returnValue = (isset($ram[$id]) && isset($ram[$id2])) ? (float)($ram[$id2] - $ram[$id]) : false;

            if ($returnValue !== false) {
                return @round($returnValue / pow(1024, ($i = floor(log($returnValue, 1024)))), 2) . ' ' . $unit[(int)$i];
            } else {
                return false;
            }
        } elseif ($id !== null) {
            $ram[$id] = memory_get_usage();
            return @round($ram[$id] / pow(1024, ($i = floor(log($ram[$id], 1024)))), 2) . ' ' . $unit[(int)$i];
        }

        return false;
    }
}
