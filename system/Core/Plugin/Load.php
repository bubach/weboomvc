<?php
/**
 * Core_Plugin_Load
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Load
{

    /**
     * @var array modules that have been used
     */
    private static $_checkedModules = array();

    /**
     * Get object. Loads a class based on config overwrites if any.
     * Set alias to false to prevent getting singleton instances.
     *
     * @access  public
     * @param   string         $className the name of the model class
     * @param   string         $alias     the property name alias
     * @param   string | array $params    the constructor params if any
     * @throws  Exception
     * @return  object         $className
     */
    public function getObject($className, $alias = null, $params = null)
    {
        $app = wmvc::app();

        if (!isset($alias)) {
            $alias = $className;
        }
        if (empty($alias) ||
            !preg_match('!^[a-zA-Z][a-zA-Z0-9_]+$!', $alias) ||
            method_exists($app, $alias)
        ) {
            throw new Exception("Name '{$alias}' is an invalid syntax");
        }

        if ($alias !== false && isset($app->$alias)) {
            return $app->$alias;
        }
        $rewrites = $app->getConfig('Rewrites/' . $className);

        if (is_array($rewrites)) {
            $app->config->sortConfigArray($rewrites);

            foreach ($rewrites as $rewrite => $handlerData) {
                $className = $rewrite;
                break;
            }
        }

        $this->checkClassModule($className);
        $obj = $this->newInstance($className, $params);

        if ($alias === false) {
            return $obj;
        }
        return $app->$alias = $obj;
    }

    /**
     * Instantiate class with params from array.
     *
     * @param  $className
     * @param  $params
     * @return mixed
     */
    public function newInstance($className, $params)
    {
        $reflection  = new ReflectionClass($className);
        if ($reflection->getConstructor() === null || empty($params)) {
            return new $className();
        }
        $params = (is_array($params)) ? $params : array($params);
        $params = (is_numeric(key($params))) ? $params : array($params);
        return $reflection->newInstanceArgs($params);
    }

    /**
     * Check if the class-module is active or needs setup
     *
     * @param  $className
     * @return bool
     * @throws Exception
     */
    public function checkClassModule($className)
    {
        $moduleName = explode('_', $className);
        $moduleName = !empty($moduleName[0]) ? $moduleName[0] : $className;
        $app        = wmvc::app();

        if (isset(self::$_checkedModules[$moduleName])) {
            return true;
        }
        $nameSpace  = $this->getNamespace($moduleName);

        if (!$app->getConfig('Modules/' . $nameSpace . '/'. $moduleName.'/Config/active')) {
            throw new Exception("Module not active for class: '{$className}'!");
        }
        if ($app->getConfig('Modules/' . $nameSpace . '/' . $moduleName.'/Config/setup')) {
            $app->setup->runSetup($nameSpace, $moduleName);
        }

        self::$_checkedModules[$moduleName] = true;
    }

    /**
     * Get the module namespace
     *
     * @param  $moduleName
     * @return string
     */
    public function getNamespace($moduleName)
    {
        $app = wmvc::app();

        if ($app->getConfig('Modules/App/' . $moduleName)) {
            $nameSpace = 'App';
        } else if ($app->getConfig('Modules/Common/' . $moduleName)) {
            $nameSpace = 'Common';
        } else {
            $nameSpace = 'System';
        }

        return $nameSpace;
    }

    /**
     * Returns a database plugin object
     *
     * @access   public
     * @param    string $poolname db-name to use (or default if unset)
     * @throws   Exception
     * @return   bool|object
     */
    public function database($poolname = null)
    {
        static $dbs = array();
        $app = wmvc::app();

        $poolname       = !empty($poolname) ? $poolname : 'default';
        $config         = $app->getConfig('Connection/' . $poolname);
        $config['name'] = $poolname;

        if ($poolname && isset($dbs[$poolname])) {
            return $dbs[$poolname];
        }
        if (!empty($config['plugin'])) {
            $dbs[$poolname] = $this->getObject($config['plugin'], 'db_' . $poolname, $config);
            return $dbs[$poolname];
        }

        return false;
    }
}
