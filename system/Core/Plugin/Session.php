<?php
/**
 * Core_Plugin_Session
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Plugin_Session
{

    /**
     * session started?
     *
     * @var bool
     */
    private static $initialized = false;

    /**
     * internal session init
     *
     * @return bool
     */
    private static function initialize() {
        if (self::$initialized) {
            return true;
        }
        if (@session_start()) {
            self::$initialized = true;
            return true;
        }
        return false;
    }

    /**
     * get session variable
     *
     * @param string $name
     * @return mixed
     */
    public static function get($name) {
        if (self::initialize() && isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
        return false;
    }

    /**
     * set session variable
     *
     * @param string $name
     * @param string $value
     * @return bool
     */
    public static function set($name, $value = "") {
        if (self::initialize()) {
            $_SESSION[$name] = $value;
            return true;
        }
        return false;
    }

    /**
     * remove session variable
     *
     * @param  $name
     * @return bool
     */
    public static function remove($name) {
        if (self::initialize()) {
            unset($_SESSION[$name]);
            return true;
        }
        return false;
    }

}