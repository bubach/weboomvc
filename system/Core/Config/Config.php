<?php
return array(
    'Runtime' => array(
        'Theme' => 'JooxTheme',
        'Timer' => 1,
        'Ram'   => 1,
        'Request' => array(
            'fullPath'   => '...',
            'GET'        => '',
            'POST'       => '',
            'namespace'  => 'joox',
            'controller' => 'default',
            'action'     => 'view',
            'params'     => NULL,
        ),
    ),
    'Modules' => array(
        'System' => array(
            'Core' => array(
                'Config' => array(
                    'version' => '0.0.1',
                    'active'  => true,
                    'author'  => 'Christoffer Bubach',
                    'merged'  => '2014-05-21 00:00:00',
                ),
                'Model' => array(),
                'View' => array(),
                'Controller' => array(),
                'Plugins' => array(
                    'PDO' => array(
                        'default' => array(
                            'public' => false,
                            'dbType' => 'mysql',
                            'dbHost' => 'localhost',
                            'dbUser' => 'root',
                            'dbPass' => '12345',
                        ),
                    ),
                    'Routes' => array(
                        'merged' => '2014-05-21 00:00:00',
                    ),
                ),
            ),
        ),
    ),
    'Connection' => array(
        'default' => array(
            'plugin' => 'Core_Model_Database',
            'dbType' => 'mysql',
            'dbHost' => '127.0.0.1',
            'dbName' => 'weboo',
            'dbUser' => 'root',
            'dbPass' => 'mysqlpass',
        ),
    ),
    'Rewrites' => array(
        'Core_Plugin_Random' => array(
            'New_Controller_Default' => array(
                'after' => '-',
            ),
        ),
    ),
    'Routes' => array(
        'handlers' => array(
            'Core_Plugin_Routing' => array(
                //'before' => '-',
            ),
        ),
        'default' => array(
            '/'                          => 'Joox_Controller_Default',
            '/view/id'                   => 'Joox_Controller_Default/view',
            '/download{d}/movie-{c}/{s}' => 'Joox_Default_View',
        ),
        'special' => array(
            '/404' => 'core_controller_404',
        ),
    ),
    'Theme' => array(
        'JooxMobileTheme' => array(
            'module'  => 'JooxMobileTheme',
            'active'  => true,
            'extends' => 'JooxTheme',
        ),
    ),
    'Layout' => array(
        'default' => array(
            'root' => array(
                'view' => 'joox_views_default',
                'data' => array(),
                'childViews' => array(
                    'head' => array(
                        'data' => array(),
                        'childViews' => array(
                            'meta' => array(
                                'view' => 'joox_views_meta',
                            ),
                            'javascript' => array(
                                'data' => array(
                                    'skinurl' => 'js/custom.js',
                                    'data'    => 'inline-js',
                                    'url'     => 'http://google.com/jsFile.js',
                                    'group'   => 'joox',
                                    'before'  => 'headCss',
                                    'after'   => 'meta',
                                ),
                            ),
                            'css' => array(
                                'data' => array(
                                    'skinurl' => 'css/global.js',
                                    'data'    => 'inline-css',
                                    'url'     => 'http://google.com/cssFile.css',
                                    'group'   => 'joox',
                                    'before'  => 'headCss',
                                    'after'   => 'headJs',
                                ),
                            ),
                        ),
                    ),
                    'body' => array(
                        'view' => 'joox_views_bodywrapper',
                        'data' => array(
                            'insertAfter' => 'head',
                        ),
                        'childViews' => array(
                            'menu' => array(
                                'view' => 'joox_views_mainmenu',
                            ),
                            'contentplaceholder' => array(
                                'view' => 'joox_views_placeholder',
                            ),
                            'footer' => array(
                                'view' => 'joox_views_footer',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);