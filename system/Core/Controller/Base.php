<?php

/**
 * Base.php
 *
 * Base application controller
 *
 * @package    WebooMVC
 * @author     Christoffer Bubach
 */

class Core_Controller_Base
{

    /**
     * Response object
     *
     * @object Core_View_Response
     */
    private $_response = null;

    /**
     * Layout object
     *
     * @object Core_View_Layout
     */
    private $_layout = null;

    /**
     * Get layout object
     *
     * @param string | array  $handles
     * @return object
     */
    public function getLayout($handles = null)
    {
        if (is_object($this->_layout)) {
            if ($handles) {
                $this->_layout->addHandle($handles);
            }
        } else {
            if (!$handles) {
                $handles = wmvc::app()->getConfig('Runtime/Request/layoutHandle');
            }

            $params = array($this->getResponse(), $handles, $theme = null);
            $this->_layout = $this->getObject('Core_View_Layout', null, $params);
        }
        return $this->_layout;
    }

    /**
     * Get response
     *
     * @return Core_View_Response
     */
    public function getResponse()
    {
        if (is_object($this->_response)) {
            return $this->_response;
        }
        return $this->_response = $this->getObject('Core_View_Response');
    }

    /**
     * Get class instance
     *
     * @param  string $class
     * @param  string $alias
     * @param  array $params
     * @return object
     */
    public function getObject($class, $alias = null, $params = null)
    {
        return wmvc::app()->getObject($class, $alias, $params);
    }

    /**
     * Get MVC config value
     *
     * @param  string $path
     * @return mixed
     */
    public function getConfig($path = '')
    {
        return wmvc::app()->getConfig($path);
    }

    /**
     * Set MVC config value
     *
     * @param string $path
     * @param mixed  $value
     * @return bool
     */
    public function setConfig($path, $value)
    {
        return wmvc::app()->setConfig($path, $value);
    }

    /**
     * Get MVC request array
     *
     * @param  string $path
     * @return array
     */
    public function getRequest($path = null)
    {
        if (!empty($path)) {
            return wmvc::app()->getConfig('Runtime/Request/'.$path);
        } else {
            return wmvc::app()->getConfig('Runtime/Request');
        }
    }

    /**
     * The default controller method
     *
     * @access  public
     */
    public function index()
    {
    }

    /**
     * __call
     *
     * gets called when an unspecified method is used
     *
     * @access  public
     */
    public function __call($function, $args)
    {
        throw new Exception("Unknown controller method '{$function}'");
    }
}
