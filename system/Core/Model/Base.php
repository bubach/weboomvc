<?php
/**
 * Core_Model_Base
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class Core_Model_Base extends Core_Object
{

    /**
     * Get database object instance
     *
     * @param  null|string $poolname
     * @return Core_Model_Database
     */
    public function getDb($poolname = null)
    {
        return wmvc::app()->getDb($poolname);
    }

    /**
     *
     */
    public function find()
    {

    }

    public function findOne()
    {

    }
}
