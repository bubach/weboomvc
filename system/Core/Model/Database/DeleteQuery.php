<?php
/**
 * Core_Model_Database_DeleteQuery
 *
 * Database delete query class
 *
 * Based on FluentPDO, Apache2 or GPL2
 * licence by Marek Lichtner.
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class Core_Model_Database_DeleteQuery extends Core_Model_Database_JoinQuery
{

    /**
     * @var bool
     */
    private $ignore = false;

    /**
     * Constructor
     *
     * @param $db
     */
    public function __construct($db)
    {
        $clauses = $this->getDeleteClausesReset();
        parent::__construct($db, $clauses);
    }

    /**
     * @return array
     */
    protected function getDeleteClausesReset()
    {
        return array(
            'DELETE FROM' => array($this, 'getClauseDeleteFrom'),
            'DELETE'      => array($this, 'getClauseDelete'),
            'FROM'        => null,
            'JOIN'        => array($this, 'getClauseJoin'),
            'WHERE'       => ' AND ',
            'ORDER BY'    => ', ',
            'LIMIT'       => null,
        );
    }

    /**
     * Init DELETE query
     *
     * @param $table
     * @return $this
     */
    public function init($table)
    {
        $this->clauses = $this->getDeleteClausesReset();
        $this->initClauses();
        $this->ignore = false;
        $this->statements['DELETE FROM'] = $table;
        $this->statements['DELETE'] = $table;
        return $this;
    }

    /**
     * DELETE IGNORE - Delete operation fails silently
     *
     * @return Core_Model_Database_DeleteQuery
     */
    public function ignore()
    {
        $this->ignore = true;
        return $this;
    }

    /**
     * Build the query
     *
     * @return string
     */
    protected function buildQuery()
    {
        if ($this->statements['FROM']) {
            unset($this->clauses['DELETE FROM']);
        } else {
            unset($this->clauses['DELETE']);
        }

        return parent::buildQuery();
    }

    /**
     * Execute DELETE query
     *
     * @return boolean
     */
    public function execute()
    {
        $result = parent::execute();

        if ($result) {
            return $result->rowCount();
        }

        return false;
    }

    /**
     * Get delete clauses
     *
     * @return string
     */
    protected function getClauseDelete()
    {
        return 'DELETE' . ($this->ignore ? ' IGNORE' : '') . ' ' . $this->statements['DELETE'];
    }

    /**
     * Get delete from clauses
     * @return string
     */
    protected function getClauseDeleteFrom()
    {
        return 'DELETE' . ($this->ignore ? ' IGNORE' : '') . ' FROM ' . $this->statements['DELETE FROM'];
    }
}
