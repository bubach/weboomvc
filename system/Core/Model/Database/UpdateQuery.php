<?php
/**
 * Core_Model_Database_DeleteQuery
 *
 * Database update query class
 *
 * Based on FluentPDO, Apache2 or GPL2
 * licence by Marek Lichtner.
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class Core_Model_Database_UpdateQuery extends Core_Model_Database_JoinQuery
{

    /**
     * Constructor
     *
     * @param $db
     */
    public function __construct($db)
    {
        $clauses = array(
            'UPDATE'   => array($this, 'getClauseUpdate'),
            'JOIN'     => array($this, 'getClauseJoin'),
            'SET'      => array($this, 'getClauseSet'),
            'WHERE'    => ' AND ',
            'ORDER BY' => ', ',
            'LIMIT'    => null,
        );
        parent::__construct($db, $clauses);
    }

    /**
     * Init UPDATE query
     *
     * @param  $table
     * @return $this
     */
    public function init($table)
    {
        $this->initClauses();
        $this->statements['UPDATE'] = $table;

        $tableParts    = explode(' ', $table);
        $this->joins[] = end($tableParts);

        return $this;
    }

    /**
     * Set field to value
     *
     * @param string|array $fieldOrArray
     * @param bool|null $value
     * @throws Exception
     * @return $this
     */
    public function set($fieldOrArray, $value = false)
    {
        if (!$fieldOrArray) {
            return $this;
        }

        if (is_string($fieldOrArray) && $value !== false) {
            $this->statements['SET'][$fieldOrArray] = $value;
        } else if (!is_array($fieldOrArray)) {
            throw new Exception('You must pass a value, or provide the SET list as an associative array. column => value');
        } else {
            foreach ($fieldOrArray as $field => $value) {
                $this->statements['SET'][$field] = $value;
            }
        }
        return $this;
    }

    /**
     * Execute update query
     *
     * @param boolean $getResultAsPdoStatement true to return the pdo statement instead of row count
     * @return int|boolean|PDOStatement
     */
    public function execute($getResultAsPdoStatement = false)
    {
        $result = parent::execute();

        if ($getResultAsPdoStatement) {
            return $result;
        }

        if ($result) {
            return $result->rowCount();
        }

        return false;
    }

    /**
     * Get update part of query string
     *
     * @return string
     */
    protected function getClauseUpdate()
    {
        return 'UPDATE ' . $this->statements['UPDATE'];
    }

    /**
     * Get set part of query string
     *
     * @return string
     */
    protected function getClauseSet()
    {
        $setArray = array();

        foreach ($this->statements['SET'] as $field => $value) {
            if (is_callable($value)) {
                $setArray[] = $field . ' = ' . $value;
            } else {
                $setArray[] = $field . ' = ?';
                $this->parameters['SET'][$field] = $value;
            }
        }
        return ' SET ' . implode(', ', $setArray);
    }
}
