<?php
/**
 * Core_Model_Database_SelectQuery
 *
 * Database select-query class
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class Core_Model_Database_SelectQuery extends Core_Model_Database_JoinQuery implements Countable
{

    /** @var string */
    protected $fromTable;

    /** @var string */
    protected $fromAlias;

    /** @var boolean */
    protected $convertTypes = false;

    /**
     * Constructor
     *
     * @param Core_Model_Database $db
     */
    public function __construct($db)
    {
        $clauses = array(
            'SELECT'   => ', ',
            'FROM'     => null,
            'JOIN'     => array($this, 'getClauseJoin'),
            'WHERE'    => ' AND ',
            'GROUP BY' => ',',
            'HAVING'   => ' AND ',
            'ORDER BY' => ', ',
            'LIMIT'    => null,
            'OFFSET'   => null,
            "\n--"     => "\n--",
        );
        parent::__construct($db, $clauses);
    }

    /**
     * Init
     *
     * @param $table
     */
    public function init($table)
    {
        $this->initClauses();

        // initialize statements
        $fromParts = explode(' ', $table);
        $this->fromTable = reset($fromParts);
        $this->fromAlias = end($fromParts);

        $this->statements['FROM'] = $table;
        $this->statements['SELECT'][] = $this->fromAlias . '.*';
        $this->joins[] = $this->fromAlias;

        if (isset($this->db->convertTypes) && $this->db->convertTypes) {
            $this->convertTypes = true;
        }
    }

    /**
     * Return table name from FROM clause
     *
     * @return string
     */
    public function getFromTable()
    {
        return $this->fromTable;
    }

    /**
     * Return table alias from FROM clause
     *
     * @return string
     */
    public function getFromAlias()
    {
        return $this->fromAlias;
    }

    /**
     * Returns a single column
     *
     * @param  int $columnNumber
     * @return string|false
     */
    public function fetchColumn($columnNumber = 0)
    {
        if (($s = $this->execute()) !== false) {
            return $s->fetchColumn($columnNumber);
        }
        return $s;
    }

    /**
     * Fetch first row or column
     *
     * @param  string $column column name or empty string for the whole row
     * @return mixed string, array or false if there is no row
     */
    public function fetch($column = '')
    {
        $s = $this->execute();
        if ($s === false) {
            return false;
        }

        $row = $s->fetch();
        if ($this->convertTypes) {
            $row = $this->convertToNativeTypes($s, $row);
        }

        if ($row && $column != '') {
            return (is_object($row)) ? $row->{$column} : $row[$column];
        }
        return $row;
    }

    /**
     * Fetch pairs
     *
     * @param  $key
     * @param  $value
     * @param  $object
     * @return array of fetched rows as pairs
     */
    public function fetchPairs($key, $value, $object = false)
    {
        if (($s = $this->select("$key, $value")->asObject($object)->execute()) !== false) {
            return $s->fetchAll(PDO::FETCH_KEY_PAIR);
        }
        return array();
    }

    /**
     * Fetch all rows
     *
     * @param  string $index       specify index column
     * @param  string $selectOnly  select columns which could be fetched
     * @return array of fetched rows
     */
    public function fetchRows($index = '', $selectOnly = '')
    {
        if ($indexAsArray = strpos($index, '[]') !== FALSE) {
            $index = str_replace('[]', '', $index);
        }
        if ($selectOnly) {
            $this->select(null)->select($index . ', ' . $selectOnly);
        }

        if ($index) {
            $data = array();
            foreach ($this as $row) {
                $key = (is_object($row)) ? $row->{$index} : $row[$index];

                if ($indexAsArray) {
                    $data[$key][] = $row;
                } else {
                    $data[$key] = $row;
                }
            }
            return $data;
        } else {
            if (($s = $this->execute()) !== false) {
                if ($this->convertTypes) {
                    return $this->convertToNativeTypes($s, $s->fetchAll());
                } else {
                    return $s->fetchAll();
                }
            }
            return array();
        }
    }

    /**
     * Countable interface doesn't break current select query
     *
     * @return int
     */
    public function count()
    {
        $instance = clone $this;
        return (int) $instance->select('COUNT(*)')->fetchColumn();
    }

    /**
     * @return ArrayIterator|int|PDOStatement
     */
    public function getIterator()
    {
        if ($this->convertTypes) {
            return new ArrayIterator($this->fetchAll());
        } else {
            return $this->execute();
        }
    }

    /**
     * Converts columns from strings to types according to
     * PDOStatement::columnMeta
     *
     * @param PDOStatement $statement
     * @param              $rows
     * @internal param array|\Traversable $assoc - provided by PDOStatement::fetch with PDO::FETCH_ASSOC
     * @return array|Traversable - copy of $assoc with matching type fields
     */
    public function convertToNativeTypes(PDOStatement $statement, $rows)
    {
        for ($i = 0; $columnMeta = $statement->getColumnMeta($i); $i++) {
            $type = $columnMeta['native_type'];

            switch ($type) {
                case 'DECIMAL':
                case 'NEWDECIMAL':
                case 'FLOAT':
                case 'DOUBLE':
                case 'TINY':
                case 'SHORT':
                case 'LONG':
                case 'LONGLONG':
                case 'INT24':
                    if (isset($rows[$columnMeta['name']])) {
                        $rows[$columnMeta['name']] = $rows[$columnMeta['name']] + 0;
                    } else {
                        if (is_array($rows) || $rows instanceof Traversable) {
                            foreach ($rows as &$row) {
                                if (isset($row[$columnMeta['name']])) {
                                    $row[$columnMeta['name']] = $row[$columnMeta['name']] + 0;
                                }
                            }
                        }
                    }
                    break;
                case 'DATETIME':
                case 'DATE':
                case 'TIMESTAMP':
                    if (isset($rows[$columnMeta['name']])) {
                        $rows[$columnMeta['name']] = new DateTime($rows[$columnMeta['name']]);
                    } else {
                        if (is_array($rows) || $rows instanceof Traversable) {
                            foreach ($rows as &$row) {
                                if (isset($row[$columnMeta['name']])) {
                                    $row[$columnMeta['name']] = new DateTime($row[$columnMeta['name']]);
                                }
                            }
                        }
                    }
                    break;
            }
        }
        return $rows;
    }
}
