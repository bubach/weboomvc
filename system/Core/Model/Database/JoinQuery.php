<?php
/**
 * Core_Model_Database_JoinQuery
 *
 * Database JOIN & WHERE clause support
 *
 * Based on FluentPDO, Apache2 or GPL2
 * licence by Marek Lichtner.
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

abstract class Core_Model_Database_JoinQuery extends Core_Model_Database_BaseQuery
{

    /**
     * @var array Methods which are allowed to be call by the magic method __call()
     */
    protected $_validMethods = array(
        'from', 'fullJoin', 'group', 'groupBy', 'having', 'innerJoin', 'join', 'leftJoin',
        'limit', 'offset', 'order', 'orderBy', 'outerJoin', 'rightJoin', 'select',
    );

    /**
     * @var array Used tables (also include table from clause FROM)
     */
    protected $_joins = array();

    /**
     * @var string Main table
     */
    protected $_mainTable = '';

    /**
     * @var string Join alias
     */
    protected $_joinAlias = '';

    /**
     * @var string Join table
     */
    protected $_joinTable = '';

    /**
     * @var boolean Disable adding undefined joins to query?
     */
    public $isSmartJoinEnabled = true;

    /**
     * Enables smart joining
     *
     * @return $this
     */
    public function enableSmartJoin()
    {
        $this->isSmartJoinEnabled = true;
        return $this;
    }

    /**
     * Disable smart joining
     *
     * @return $this
     */
    public function disableSmartJoin()
    {
        $this->isSmartJoinEnabled = false;
        return $this;
    }

    /**
     * Check if smart joining is enabled
     *
     * @return boolean
     */
    public function isSmartJoinEnabled()
    {
        return $this->isSmartJoinEnabled;
    }

    /**
     * Add where condition, more calls appends with AND
     *
     * @param  string    $condition   possibly containing ? or :name (PDO syntax)
     * @param  mixed     $parameters  array or a scalar value
     * @return Core_Model_Database_JoinQuery
     */
    public function where($condition, $parameters = array())
    {
        if ($condition === null) {
            return $this->resetClause('WHERE');
        }
        if (!$condition) {
            return $this;
        }

        // where(array("column1" => 1, "column2 > ?" => 2))
        if (is_array($condition)) {
            foreach ($condition as $key => $val) {
                $this->where($key, $val);
            }

            return $this;
        }

        $args = func_get_args();

        if (count($args) == 1) {
            return $this->addStatement('WHERE', $condition);
        }

        return $this->simpleWhere($condition, $parameters);
    }

    /**
     * Add where clause when conditions is not an array
     *
     * @param  string $condition
     * @param  array $parameters
     * @return Core_Model_Database_JoinQuery
     */
    protected function simpleWhere($condition, $parameters = array())
    {
        $args = func_get_args();

        if (count($args) == 2 && !preg_match('/(\?|:\w+)/i', $condition)) {
            if (is_null($parameters)) {
                return $this->addStatement('WHERE', $condition . ' IS NULL');
            } elseif ($parameters === array()) {
                return $this->addStatement('WHERE', 'FALSE');
            } elseif (is_array($parameters)) {
                $in = $this->quote($parameters);
                return $this->addStatement('WHERE', $condition . ' IN '. $in);
            }

            if (is_callable($parameters)) {
                $condition = "{$condition} = {$parameters}";
                return $this->addStatement('WHERE', $condition);
            } else {
                $condition = "$condition = ?";
            }
        }

        array_shift($args);
        return $this->addStatement('WHERE', $condition, $args);
    }

    /**
     * Universal handler for calls to groups and order
     *
     * @param        $clause
     * @param  array $parameters - first is $statement followed by $parameters
     * @throws Exception
     * @return Core_Model_Database_JoinQuery
     */
    public function __call($clause, $parameters = array())
    {
        if (!in_array($clause, $this->_validMethods)) {
            throw new Exception('Call to invalid method ' . get_class($this) . '::' . $clause . '()');
        }
        $clause = self::toUpperWords($clause);

        switch ($clause) {
            CASE 'GROUP':
                $clause = 'GROUP BY';
                break;
            CASE 'ORDER':
                $clause = 'ORDER BY';
                break;
            CASE 'FOOT NOTE':
                $clause = "\n--";
                break;
        }

        $statement = array_shift($parameters);

        if (strpos($clause, 'JOIN') !== FALSE) {
            return $this->addJoinStatements($clause, $statement, $parameters);
        }

        return $this->addStatement($clause, $statement, $parameters);
    }

    /**
     * Get join clause
     *
     * @return string
     */
    protected function getClauseJoin()
    {
        return implode(' ', $this->statements['JOIN']);
    }

    /**
     * Match "tables AS alias"
     *
     * @param $statement
     */
    protected function getTableAlias($statement)
    {
        preg_match('/`?([a-z_][a-z0-9_\.:]*)`?(\s+AS)?(\s+`?([a-z_][a-z0-9_]*)`?)?/i', $statement, $matches);

        if ($matches) {
            $this->_joinTable = $matches[1];

            if (isset($matches[4]) && !in_array(strtoupper($matches[4]), array('ON', 'USING'))) {
                $this->_joinAlias = $matches[4];
            }
        }
    }

    /**
     * Resolve join ON / USING
     *
     * @param $clause
     * @param $statement
     * @param $parameters
     * @return $this|bool|Core_Model_Database_JoinQuery
     */
    protected function getOnUsingStatements($clause, $statement, $parameters)
    {
        if (strpos(strtoupper($statement), ' ON ') || strpos(strtoupper($statement), ' USING')) {
            if (!$this->_joinAlias) {
                $this->_joinAlias = $this->_joinTable;
            }

            if (in_array($this->_joinAlias, $this->joins)) {
                return $this;
            } else {
                $this->joins[] = $this->_joinAlias;
                $statement = " $clause $statement";

                return $this->addStatement('JOIN', $statement, $parameters);
            }
        }

        return false;
    }

    /**
     * Statement can contain more tables (e.g. "table1.table2:table3:")
     *
     * @param string $clause
     * @param $statement
     * @param array $parameters
     * @return Core_Model_Database_JoinQuery
     */
    private function addJoinStatements($clause, $statement, $parameters = array())
    {
        if ($statement === null) {
            $this->joins = array();

            return $this->resetClause('JOIN');
        }

        if (array_search(substr($statement, 0, -1), $this->joins) !== false) {
            return $this;
        }

        $this->_joinAlias = $this->_mainTable = $this->_joinTable = '';
        $this->getTableAlias($statement);

        $res = $this->getOnUsingStatements($clause, $statement, $parameters);
        if ($res) {
            return $res;
        }

        // $this->_joinTable is list of tables for join e.g.: table1.table2:table3....
        if (!in_array(substr($this->_joinTable, -1), array('.', ':'))) {
            $this->_joinTable .= '.';
        }

        preg_match_all('~([a-z_][a-z0-9_]*[\.:]?)~i', $this->_joinTable, $matches);

        if (isset($this->statements['FROM'])) {
            $this->_mainTable = $this->statements['FROM'];
        } elseif (isset($this->statements['UPDATE'])) {
            $this->_mainTable = $this->statements['UPDATE'];
        }

        $lastItem = array_pop($matches[1]);
        array_push($matches[1], $lastItem);

        foreach ($matches[1] as $joinItem) {
            if ($this->_mainTable == substr($joinItem, 0, -1)) {
                continue;
            }

            $alias = '';
            if ($joinItem == $lastItem) {
                $alias = $this->_joinAlias;
            }

            $newJoin = $this->createJoinStatement($clause, $this->_mainTable, $joinItem, $alias);
            if ($newJoin) {
                $this->addStatement('JOIN', $newJoin, $parameters);
            }

            $this->_mainTable = $joinItem;
        }
        return $this;
    }

    /**
     * Create join string
     *
     * @param string $clause
     * @param $mainTable
     * @param string $joinTable
     * @param string $joinAlias
     * @return string
     */
    private function createJoinStatement($clause, $mainTable, $joinTable, $joinAlias = '')
    {
        if (in_array(substr($mainTable, -1), array(':', '.'))) {
            $mainTable = substr($mainTable, 0, -1);
        }

        $referenceDirection = substr($joinTable, -1);
        $joinTable = substr($joinTable, 0, -1);
        $asJoinAlias = '';

        if ($joinAlias) {
            $asJoinAlias = " AS $joinAlias";
        } else {
            $joinAlias = $joinTable;
        }

        // if join exists don't create same again
        if (in_array($joinAlias, $this->joins)) {
            return '';
        } else {
            $this->joins[] = $joinAlias;
        }

        // back reference
        if ($referenceDirection == ':') {
            $primaryKey = $this->db->getPrimaryKey();
            $foreignKey = $this->db->getForeignKey($mainTable);

            return " $clause $joinTable$asJoinAlias ON $joinAlias.$foreignKey = $mainTable.$primaryKey";
        } else {
            $primaryKey = $this->db->getPrimaryKey();
            $foreignKey = $this->db->getForeignKey($joinTable);

            return " $clause $joinTable$asJoinAlias ON $joinAlias.$primaryKey = $mainTable.$foreignKey";
        }
    }

    /**
     * Build query with extra join check first
     *
     * @return string
     */
    protected function buildQuery()
    {
        $statementsWithReferences = array('WHERE', 'SELECT', 'GROUP BY', 'ORDER BY');

        foreach ($statementsWithReferences as $clause) {
            if (array_key_exists($clause, $this->statements)) {
                $this->statements[$clause] = array_map(
                    array($this, 'createUndefinedJoins'),
                    $this->statements[$clause]
                );
            }
        }

        return parent::buildQuery();
    }

    /**
     * Create undefined joins from statement with column with referenced tables
     *
     * @param string $statement
     * @return string rewritten $statement (e.g. tab1.tab2:col => tab2.col)
     */
    private function createUndefinedJoins($statement)
    {
        if (!$this->isSmartJoinEnabled) {
            return $statement;
        }

        preg_match_all('/([^[:space:]\(\)]+[.:])[\p{L}\p{N}\p{Pd}\p{Pi}\p{Pf}\p{Pc}]*/u', $statement, $matches);

        foreach ($matches[1] as $join) {
            if (!in_array(substr($join, 0, -1), $this->joins)) {
                $this->addJoinStatements('LEFT JOIN', $join);
            }
        }

        // don't rewrite table from other databases
        foreach ($this->joins as $join) {
            if (strpos($join, '.') !== false && strpos($statement, $join) === 0) {
                return $statement;
            }
        }

        // remove extra referenced tables (rewrite tab1.tab2:col => tab2.col)
        $statement = preg_replace('/(?:[^\s]*[.:])?([^\s]+)[.:]([^\s]*)/u', '$1.$2', $statement);
        return $statement;
    }
}
