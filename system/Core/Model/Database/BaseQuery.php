<?php
/**
 * Core_Model_Database_BaseQuery
 *
 * Database common shared query-code.
 *
 * Based on FluentPDO, Apache2 or GPL2
 * licence by Marek Lichtner.
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

abstract class Core_Model_Database_BaseQuery implements IteratorAggregate
{

    /**
     * @var Core_Model_Database
     */
    protected $db;

    /**
     * @var array of definition clauses
     */
    protected $clauses = array();

    /**
     * @var PDOStatement
     */
    protected $result;

    /**
     * @var float
     */
    protected $time;

    /**
     * @var bool
     */
    protected $object = false;

    /**
     * @var array of object's constructor arguments
     */
    protected $objectArguments = null;

    /**
     * @var bool
     */
    protected $hasSelect = false;

    /**
     * @var array
     */
    protected $statements = array();

    /**
     * @var array
     */
    protected $parameters = array();

    /**
     * @var array
     */
    protected $joins = array();


    /**
     * Constructor
     *
     * @param Core_Model_Database $db
     * @param $clauses
     */
    protected function __construct($db, $clauses)
    {
        $this->db      = $db;
        $this->clauses = $clauses;
        $this->initClauses();
    }

    /**
     * Return formatted query
     *
     * @return string - formatted query
     */
    public function __toString()
    {
        return $this->getQuery();
    }

    /**
     * @return \PDO
     */
    protected function getPdo()
    {
        return $this->db->getPdo();
    }

    /**
     * Get PDOStatement result
     *
     * @return \PDOStatement
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Get time of execution
     *
     * @return float
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Get query parameters
     *
     * @return array
     */
    public function getParameters()
    {
        return $this->buildParameters();
    }

    /**
     * Init
     *
     * @return $this
     */
    protected function initClauses()
    {
        foreach ($this->clauses as $clause => $value) {
            if ($value) {
                $this->statements[$clause] = array();
                $this->parameters[$clause] = array();
            } else {
                $this->statements[$clause] = null;
                $this->parameters[$clause] = null;
            }
        }

        $this->object    = false;
        $this->joins     = array();
        $this->hasSelect = false;

        return $this;
    }

    /**
     * Add statement for all kind of clauses, if it's a select
     * clause specifying columns we remove default '*'.
     *
     * @param string $clause
     * @param $statement
     * @param array $parameters
     * @return Core_Model_Database_JoinQuery
     */
    protected function addStatement($clause, $statement, $parameters = array())
    {
        if ($clause == 'SELECT' && $this->hasSelect == false) {
            array_shift($this->statements['SELECT']);
            $this->hasSelect = true;
        }

        if ($statement === null) {
            return $this->resetClause($clause);
        }

        if ($this->clauses[$clause]) {
            if (is_array($statement)) {
                $this->statements[$clause] = array_merge($this->statements[$clause], $statement);
            } else {
                $this->statements[$clause][] = $statement;
            }

            $this->parameters[$clause] = array_merge($this->parameters[$clause], $parameters);
        } else {
            $this->statements[$clause] = $statement;
            $this->parameters[$clause] = $parameters;
        }

        return $this;
    }

    /**
     * Remove all prev defined statements
     *
     * @param string $clause
     * @return $this
     */
    protected function resetClause($clause)
    {
        $this->statements[$clause] = null;
        $this->parameters[$clause] = array();
        if (isset($this->clauses[$clause]) && $this->clauses[$clause]) {
            $this->statements[$clause] = array();
        }
        return $this;
    }

    /**
     * Implements method from IteratorAggregate
     *
     * @return PDOStatement
     */
    public function getIterator()
    {
        return $this->execute();
    }

    /**
     * Execute query with earlier added parameters
     *
     * @return PDOstatement|boolean
     */
    public function execute()
    {
        $query      = $this->buildQuery();
        $parameters = $this->buildParameters();
        $result     = $this->getPdo()->prepare($query);

        if ($this->object !== false) {
            if (class_exists($this->object, false)) {
                if (is_array($this->objectArguments)) {
                    $result->setFetchMode(PDO::FETCH_CLASS, $this->object, $this->objectArguments);
                } else {
                    $result->setFetchMode(PDO::FETCH_CLASS, $this->object);
                }
            } else {
                $result->setFetchMode(PDO::FETCH_OBJ);
            }
        } elseif ($this->getPdo()->getAttribute(PDO::ATTR_DEFAULT_FETCH_MODE) == PDO::FETCH_BOTH) {
            $result->setFetchMode(PDO::FETCH_ASSOC);
        }

        $time = microtime(true);
        if ($result && $result->execute($parameters)) {
            $this->time = microtime(true) - $time;
        } else {
            $result = false;
        }

        $this->result = $result;
        return $result;
    }

    /**
     * Get query string
     *
     * @param boolean $formatted  return formatted query
     * @return string
     */
    public function getQuery($formatted = true)
    {
        $query = $this->buildQuery();

        if ($formatted) {
            $query = $this->formatQuery($query);
        }
        return $query;
    }

    /**
     * Convert "camelCaseWord" to "CAMEL CASE WORD"
     *
     * @param string $string
     * @return string
     */
    public static function toUpperWords($string)
    {
        return trim(strtoupper(preg_replace('#(.)([A-Z]+)#', '$1 $2', $string)));
    }

    /**
     * Make the query pretty
     *
     * @param string $query
     * @return string
     */
    public static function formatQuery($query)
    {
        $query = preg_replace(
            '/\b(WHERE|FROM|GROUP BY|HAVING|ORDER BY|LIMIT|OFFSET|UNION|ON DUPLICATE KEY UPDATE|VALUES|SET)\b/',
            "\n$0", $query
        );
        $query = preg_replace(
            '/\b(INNER|OUTER|LEFT|RIGHT|FULL|CASE|WHEN|END|ELSE|AND)\b/',
            "\n    $0", $query
        );
        $query = preg_replace('/\s+\n/', "\n", $query);
        return $query;
    }

    /**
     * Generate query
     *
     * @return string
     * @throws Exception
     */
    protected function buildQuery()
    {
        $query = '';

        foreach ($this->clauses as $clause => $separator) {
            if ($this->clauseNotEmpty($clause)) {
                if (is_string($separator)) {
                    $query .= " $clause " . implode($separator, $this->statements[$clause]);
                } elseif ($separator === null) {
                    $query .= " $clause " . $this->statements[$clause];
                } elseif (is_callable($separator)) {
                    $query .= call_user_func($separator);
                } else {
                    throw new Exception("Clause '$clause' is incorrectly set to '$separator'.");
                }
            }
        }
        return trim($query);
    }

    /**
     * Check for empty clause
     *
     * @param $clause
     * @return bool
     */
    protected function clauseNotEmpty($clause)
    {
        if ($this->clauses[$clause]) {
            return !empty($this->statements[$clause]);
        } else {
            return (boolean)$this->statements[$clause];
        }
    }

    /**
     * Build parameter array, checking named params
     * e.g. (':name' => 'Mark')
     *
     * @return array
     */
    protected function buildParameters()
    {
        $parameters = array();

        foreach ($this->parameters as $clauses) {
            if (is_array($clauses)) {
                foreach ($clauses as $value) {
                    if (is_array($value) &&
                        is_string(key($value)) &&
                        substr(key($value), 0, 1) == ':'
                    ) {
                        $parameters = array_merge($parameters, $value);
                    } else {
                        $parameters[] = $value;
                    }
                }
            } elseif ($clauses) {
                $parameters[] = $clauses;
            }
        }

        return $parameters;
    }

    /**
     * Quote parameter
     *
     * @param $value
     * @return string
     */
    protected function quote($value)
    {
        if (!isset($value)) {
            return 'NULL';
        }
        $value = $this->formatValue($value);

        switch ($value) {
            case false:
                return '0';
            case is_array($value):
                return '(' . implode(', ', array_map(array($this, 'quote'), $value)) . ')';
            case is_float($value):
                return sprintf('%F', $value);
            case is_int($value):
            case is_callable($value):
                return (string) $value;
            default:
                return $this->getPdo()->quote($value);
        }
    }

    /**
     * Format values, only datetime for now.
     *
     * @param $val
     * @return string
     */
    private function formatValue($val)
    {
        if ($val instanceof DateTime) {
            return $val->format('Y-m-d H:i:s');
        }
        return $val;
    }

    /**
     * Select an item as object, or true for stdClass.
     * false for associative array.
     *
     * @param  boolean|object $object    Classname or true for stdClass.
     * @param  array          $arguments If provided, passed to object constructor.
     * @return Core_Model_Database_JoinQuery
     */
    public function asObject($object = true, $arguments = null)
    {
        $this->object = $object;
        $this->objectArguments = $arguments;
        return $this;
    }
}
