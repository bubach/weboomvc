<?php
/**
 * Core_Model_Database_DeleteQuery
 *
 * Database insert query class
 *
 * Based on FluentPDO, Apache2 or GPL2
 * licence by Marek Lichtner.
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class Core_Model_Database_InsertQuery extends Core_Model_Database_JoinQuery
{

    /** @var array */
    protected $columns = array();

    /** @var array */
    protected $firstValue = array();

    /** @var boolean */
    protected $ignore = false;

    /** @var boolean */
    protected $delayed = false;

    /**
     * Constructor
     *
     * @param $db
     */
    public function __construct($db)
    {
        $clauses = array(
            'INSERT INTO'             => array($this, 'getClauseInsertInto'),
            'VALUES'                  => array($this, 'getClauseValues'),
            'ON DUPLICATE KEY UPDATE' => array($this, 'getClauseOnDuplicateKeyUpdate'),
        );
        parent::__construct($db, $clauses);
    }

    /**
     * Execute insert query
     *
     * @param null $sequence
     * @return string|false last inserted id or false
     */
    public function execute($sequence = null)
    {
        if ($result = parent::execute()) {
            return $this->getPdo()->lastInsertId($sequence);
        }
        return false;
    }

    /**
     * Add ON DUPLICATE KEY UPDATE
     *
     * @param array $values
     * @return Core_Model_Database_InsertQuery
     */
    public function onDuplicateKeyUpdate($values)
    {
        $this->statements['ON DUPLICATE KEY UPDATE'] = array_merge(
            $this->statements['ON DUPLICATE KEY UPDATE'],
            $values
        );
        return $this;
    }

    /**
     * Add VALUES
     *
     * @param  $table
     * @param  $values
     * @throws Exception
     * @return Core_Model_Database_InsertQuery
     */
    public function values($table, $values)
    {
        $this->initClauses();
        $this->statements['INSERT INTO'] = $table;
        $this->columns                   = array();
        $this->firstValue                = array();
        $this->ignore                    = false;
        $this->delayed                   = false;

        if (!is_array($values)) {
            throw new Exception("Param VALUES for INSERT query must be array");
        }
        $first = current($values);

        if (is_string(key($values))) {
            $this->addOneValue($values);
        } elseif (is_array($first) && is_string(key($first))) {
            foreach ($values as $oneValue) {
                $this->addOneValue($oneValue);
            }
        } else {
            throw new Exception("Invalid insert syntax or missing field name");
        }
        return $this;
    }

    /**
     * INSERT IGNORE - insert operation fails silently
     *
     * @return Core_Model_Database_InsertQuery
     */
    public function ignore()
    {
        $this->ignore = true;
        return $this;
    }

    /**
     * Insert operation delay
     *
     * @return Core_Model_Database_InsertQuery
     */
    public function delayed()
    {
        $this->delayed = true;
        return $this;
    }

    /**
     * Get INSERT INTO clause
     *
     * @return string
     */
    protected function getClauseInsertInto()
    {
        return sprintf('INSERT%s%s INTO %s',
            ($this->ignore ? ' IGNORE' : ''),
            ($this->delayed ? ' DELAYED' : ''),
            $this->statements['INSERT INTO']
        );
    }

    /**
     * @param $param
     *
     * @return string
     */
    protected function parameterGetValue($param)
    {
        return is_callable($param) ? (string)$param : '?';
    }

    /**
     * Get clause values
     *
     * @return string
     */
    protected function getClauseValues()
    {
        $valuesArray = array();
        foreach ($this->statements['VALUES'] as $rows) {
            $placeholders  = array_map(array($this, 'parameterGetValue'), $rows);
            $valuesArray[] = '(' . implode(', ', $placeholders) . ')';
        }
        $columns = implode(', ', $this->columns);
        $values = implode(', ', $valuesArray);
        return " ($columns) VALUES $values";
    }

    /**
     * Removes all callable Literal instances from the argument
     * to be injected directly into the query
     *
     * @param $statements
     *
     * @return array
     */
    protected function filterLiterals($statements)
    {
        $f = function ($item) {
            return !is_callable($item);
        };

        return array_map(
            function ($item) use ($f) {
                if (is_array($item)) {
                    return array_filter($item, $f);
                }
                return $item;
            },
            array_filter($statements, $f)
        );
    }

    /**
     * @return array
     */
    protected function buildParameters()
    {
        $this->parameters = array_merge(
            $this->filterLiterals($this->statements['VALUES']),
            $this->filterLiterals($this->statements['ON DUPLICATE KEY UPDATE'])
        );
        return parent::buildParameters();
    }

    /**
     * Get ON DUPLICATE clause
     *
     * @return string
     */
    protected function getClauseOnDuplicateKeyUpdate()
    {
        $result = array();
        foreach ($this->statements['ON DUPLICATE KEY UPDATE'] as $key => $value) {
            $result[] = "$key = " . $this->parameterGetValue($value);
        }
        return ' ON DUPLICATE KEY UPDATE ' . implode(', ', $result);
    }

    /**
     * Add one value
     *
     * @param $oneValue
     * @throws Exception
     */
    private function addOneValue($oneValue)
    {
        foreach ($oneValue as $key => $value) {
            if (!is_string($key)) {
                throw new Exception('INSERT query: All keys of value array have to be strings.');
            }
        }

        if (!$this->firstValue) {
            $this->firstValue = $oneValue;
        }
        if (!$this->columns) {
            $this->columns = array_keys($oneValue);
        }
        if ($this->columns != array_keys($oneValue)) {
            throw new Exception('INSERT query: All VALUES have to have same keys (columns).');
        }
        $this->statements['VALUES'][] = $oneValue;
    }
}
