<?php
/**
 * Core_Model_Database_SchemaQuery
 *
 * Database schema class
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class Core_Model_Database_SchemaQuery
{

    /**
     * @var Core_Model_Database
     */
    protected $_db;

    /**
     * defaults for columns
     * 'type' => 'primary|string|integer|boolean|enum|decimal|datetime', REQUIRED!
     *
     * @var array
     */
    protected $_defaults = array(
        'type'       => 'string',
        'length'     => null,
        'unsigned'   => false,
        'index'      => false,
        'null'       => true,
        'constraint' => false,
        'default'    => null,
        'unique'     => false,
        'precision'  => 0,
        'scale'      => 0,
    );

    /**
     * @var array
     */
    protected $_index = array();

    /**
     * @var array
     */
    protected $_unique = array();

    /**
     * @var array
     */
    protected $_constraint = array();

    /**
     * @var string|null
     */
    protected $_primary = null;

    /**
     * @var string
     */
    protected $_sql;

    /**
     * @var string
     */
    protected $_engine = 'InnoDB';

    /**
     * @var string
     */
    protected $_column;

    /**
     * @var array
     */
    protected $_data = array();

    /**
     * Constructor
     *
     * @param Core_Model_Database $db
     */
    public function __construct($db)
    {
        $this->_db = $db;
    }

    /**
     * Init variables
     */
    public function initClauses()
    {
        $this->_index      = array();
        $this->_unique     = array();
        $this->_constraint = array();
        $this->_primary    = null;
        $this->_engine     = 'InnoDB';
    }

    /**
     * Set engine used when creating tables
     *
     * @param $engine
     */
    public function setEngine($engine)
    {
        $this->_engine = $engine;
    }

    /**
     * Drop table
     *
     * @param  $table
     * @return bool
     */
    public function dropTable($table)
    {
        try {
            $this->_db->getPdo()->exec("SET foreign_key_checks = 0;");
            $this->_db->getPdo()->exec("DROP TABLE IF EXISTS `{$table}`");
            $this->_db->getPdo()->exec("SET foreign_key_checks = 1;");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * creates table(s) from array description
     *
     * @param  $tables
     * @param  bool $firstDrop
     * @throws Exception
     * @return bool $status
     */
    public function createTable($tables, $firstDrop = true)
    {
        if (empty($tables) || !is_array($tables)) {
            return false;
        }

        foreach ($tables as $table => $schema) {
            $this->_createOneTable($table, $schema, $firstDrop);
        }

        return true;
    }

    /**
     * Create a table, internal use
     *
     * @param  $table
     * @param  $schema
     * @param  boolean $firstDrop
     * @throws Exception
     */
    private function _createOneTable($table, $schema, $firstDrop)
    {
        $this->initClauses();
        $this->_sql = "CREATE TABLE `$table` (\n";

        if ($firstDrop) {
            $this->dropTable($table);
        }
        $this->buildColumns($schema);
        $this->buildTableExtras($table);

        try {
            if (!empty($this->_constraint)) {
                $this->_db->getPdo()->exec("SET foreign_key_checks = 0;");
                $this->_db->getPdo()->exec($this->_sql);
                $this->_db->getPdo()->exec("SET foreign_key_checks = 1;");
            } else {
                $this->_db->getPdo()->exec($this->_sql);
            }
        } catch (Exception $e) {
            throw new Exception("Query to create table failed!");
        }
    }

    /**
     * Build the extras and finish table SQL
     *
     * @param  $table
     */
    protected function buildTableExtras($table)
    {
        if ($this->_primary) {
            $this->_sql .= "PRIMARY KEY (`$this->_primary`),\n";
        }

        foreach ($this->_unique as $column) {
            $this->_sql .= "UNIQUE KEY `$column` (`$column`),\n";
        }

        foreach ($this->_index as $column) {
            $this->_sql .= "KEY `$column` (`$column`),\n";
        }

        foreach ($this->_constraint as $i => $info) {
            $this->_sql .= "CONSTRAINT `{$table}_ibfk_{$i}` FOREIGN KEY (`{$info['column']}`) REFERENCES `{$info['table']}` (`{$info['field']}`),\n";
        }

        // remove ending comma & set table type
        $this->_sql = substr($this->_sql, 0, -2)."\n";
        $this->_sql .=') ENGINE = '.$this->_engine.' DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;';
    }

    /**
     * Build the table columns
     *
     * @param $schema
     */
    protected function buildColumns($schema)
    {
        foreach ($schema as $column => $data) {
            $data          = array_merge($this->_defaults, $data);
            $this->_data   = $data;
            $this->_column = $column;
            $this->buildOneColumn();

            if ($this->_primary != $this->_column) {
                $this->buildDefinition();
            }
        }
    }

    /**
     * Build one column
     *
     * @return void
     */
    protected function buildOneColumn()
    {
        switch ($this->_data['type']) {
            CASE 'primary':
            CASE 'integer':
                $this->_buildInteger();
                break;
            CASE 'string':
                $this->_buildString();
                break;
            CASE 'boolean':
                $this->_data['type'] = 'TINYINT(1)';
                break;
            CASE 'enum':
                $this->_data['type'] = "ENUM('".implode("', '", $this->_data['options'])."')";
                break;
            CASE 'decimal':
                $this->_data['type'] = 'DECIMAL('. $this->_data['precision'].','. $this->_data['scale'].')';
                break;
            CASE 'datetime':
                $this->_data['type'] = 'DATETIME';
                break;
        }
        $this->_checkPrimary();
    }

    /**
     * Check if primary key
     *
     * @return void
     */
    private function _checkPrimary()
    {
        if ($this->_primary == $this->_column) {
            $this->_sql .= "\t`$this->_column` {$this->_data['type']} unsigned NOT NULL AUTO_INCREMENT,\n";
        }
    }

    /**
     * Build integer column
     *
     * @return void
     */
    private function _buildInteger()
    {
        $this->_primary = ($this->_data['type'] == 'primary') ? (string) $this->_column : $this->_primary;

        $this->_data['unsigned'] = !empty($this->_data['signed']) ? false : true;
        $this->_data['length']   = $this->_data['length'] ? $this->_data['length'] : 2147483647;
        $this->_data['type']     = $this->_getIntegerType($this->_data['length']);
    }

    /**
     * Get integer type
     *
     * @param  $length
     * @return string
     */
    private function _getIntegerType($length)
    {
        switch ($length) {
            CASE $length <= 127:
                return 'TINYINT';
            CASE $length <= 32767:
                return 'SMALLINT';
            CASE $length <= 8388607:
                return 'MEDIUMINT';
            CASE $length <= 2147483647:
                return 'INT';
            DEFAULT:
                return 'BIGINT';
        }
    }

    /**
     * Build string column
     *
     * @return void
     */
    private function _buildString()
    {
        $this->_data['length'] = $this->_data['length'] ? $this->_data['length'] : 65535;

        if ($this->_data['length'] <= 255) {
            $this->_data['type'] = 'VARCHAR('. $this->_data['length'] .')';
        } else if ($this->_data['length'] <= 65535) {
            $this->_data['type'] =  'TEXT';
        } elseif ($this->_data['length'] <= 16777215) {
            $this->_data['type'] =  'MEDIUMTEXT';
        } else {
            $this->_data['type'] =  'LONGTEXT';
        }
    }

    /**
     * Build column definitions
     *
     * @return void
     */
    protected function buildDefinition()
    {
        $this->_sql .= "\t`$this->_column` {$this->_data['type']}";

        foreach ($this->_data as $key => $value) {
            $this->buildOneDefinitionRow($key, $value);
        }

        $this->_sql .= ",\n";
    }

    /**
     * Build one definition
     *
     * @param $key
     * @param $value
     */
    protected function buildOneDefinitionRow($key, $value)
    {
        if ($key == 'unsigned' && $value) {
            $this->_sql .= " UNSIGNED";
        } else if ($key == 'null' && !$value) {
            $this->_sql .= " NOT NULL";
        } else if ($key == 'default' && $value !== null) {
            $this->_sql .= ($value === "") ? " DEFAULT ''" : " DEFAULT ".$value;
        } else if ($key == 'unique' && $value) {
            $this->_unique[] = $this->_column;
        } else if ($key == 'index' && $value) {
            $this->_index[] = $this->_column;
        } else if ($key == 'constraint' && $value) {
            $value['table']      = !empty($value['table']) ? $value['table'] : '';
            $value['field']      = !empty($value['field']) ? $value['field'] : '';
            $this->_constraint[] = array('column' => $this->_column, 'table' => $value['table'], 'field' => $value['field']);
        }
    }
}
