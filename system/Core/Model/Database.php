<?php
/**
 * Core_Model_Database
 *
 * Database access & Query builder using PDO.
 *
 * Based on FluentPDO, Apache2 or GPL2
 * licence by Marek Lichtner.
 *
 * @package   WebooMVC
 * @author    Marek Lichtner, Christoffer Bubach
 */

class Core_Model_Database
{

    /** @var PDO */
    private $pdo;

    /** @var string */
    private $_primaryKey = 'id';

    /** @var string */
    private $_foreignKey = '%s_id';

    /** @var string */
    public $connectionName = '';

    /** @var boolean */
    public $convertTypes = false;

    /**
     * Constructor
     *
     * @param array $config
     * @throws Exception
     */
    public function __construct($config)
    {
        if (!class_exists('PDO', false)) {
            throw new Exception('PHP PDO package is required.');
        }

        if (empty($config) && !is_array($config)) {
            throw new Exception('Database definition config is required.');
        }

        $this->connectionName = $config['name'];
        $this->connect($config);
    }

    /**
     * Connects to database with PDO interface
     *
     * @param  array $config
     * @throws Exception
     */
    public function connect($config)
    {
        if (empty($config['charset'])) {
            $config['charset'] = 'utf8';
        }

        if (!empty($config['dsn'])) {
            $dsn = $config['dsn'];
        } elseif ($config['dbType'] == 'sqlsrv') {
            $dsn = "{$config['dbType']}:Server={$config['dbHost']};Database={$config['dbName']}";
        } else {
            $dsn = "{$config['dbType']}:host={$config['dbHost']};dbname={$config['dbName']};charset={$config['charset']}";
        }

        try {
            $this->pdo = new PDO(
                $dsn,
                $config['dbUser'],
                $config['dbPass'],
                array(PDO::ATTR_PERSISTENT => !empty($config['persistent']) ? true : false)
            );
            $this->pdo->exec("SET CHARACTER SET {$config['charset']}");
        } catch (PDOException $e) {
            throw new Exception(
                sprintf("Can't connect to PDO database '{$config['dbType']}'. Error: %s",
                $e->getMessage())
            );
        }

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Get primary key
     *
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->_primaryKey;
    }

    /**
     * Get foreign key
     *
     * @param $table
     * @return string
     */
    public function getForeignKey($table)
    {
        return sprintf($this->_foreignKey, $table);
    }

    /**
     * Get a SQL literal as anonymous function
     *
     * @param  $string
     * @return Closure
     */
    public function getLiteral($string)
    {
        return function() use ($string) {
            return $string;
        };
    }

    /**
     * Create SELECT query from $table
     *
     * @param string $table  db table name
     * @param integer $primaryKey  return one row by primary key
     * @return Core_Model_Database_SelectQuery
     */
    public function from($table, $primaryKey = null)
    {
        $classAlias = $this->connectionName . '_SelectQuery';

        $query = wmvc::app()->getObject('Core_Model_Database_SelectQuery', $classAlias, $this);
        $query->init($table);

        if ($primaryKey) {
            $tableAlias = $query->getFromAlias();
            $primaryKeyName = $this->getPrimaryKey();
            $query = $query->where("$tableAlias.$primaryKeyName", $primaryKey);
        }

        return $query;
    }

    /**
     * Create INSERT INTO query
     *
     * @param string $table
     * @param array $values  you can add one or multi rows array @see docs
     * @return Core_Model_Database_InsertQuery
     */
    public function insertInto($table, $values = array())
    {
        $classAlias = $this->connectionName . '_InsertQuery';
        $query = wmvc::app()->getObject('Core_Model_Database_InsertQuery', $classAlias, $this);

        return $query->values($table, $values);
    }

    /**
     * Create UPDATE query
     *
     * @param string $table
     * @param array|string $set
     * @param string $primaryKey
     *
     * @return Core_Model_Database_UpdateQuery
     */
    public function update($table, $set = array(), $primaryKey = null)
    {
        $classAlias = $this->connectionName . '_UpdateQuery';
        $query = wmvc::app()->getObject('Core_Model_Database_UpdateQuery', $classAlias, $this);
        $query->init($table)->set($set);

        return $this->updateDeletePrimaryWhere($query, $primaryKey);
    }

    /**
     * Create DELETE query
     *
     * @param string $table
     * @param string $primaryKey  delete only row by primary key
     * @return Core_Model_Database_DeleteQuery
     */
    public function delete($table, $primaryKey = null)
    {
        $classAlias = $this->connectionName . '_DeleteQuery';
        $query = wmvc::app()->getObject('Core_Model_Database_DeleteQuery', $classAlias, $this);
        $query->init($table);

        return $this->updateDeletePrimaryWhere($query, $primaryKey);
    }

    /**
     * Set WHERE to primary key on updates and deletes
     * 
     * @param $query
     * @param string|null $primaryKey
     * @return mixed
     */
    private function updateDeletePrimaryWhere($query, $primaryKey)
    {
        if ($primaryKey) {
            $primaryKeyName = $this->getPrimaryKey();
            $query = $query->where($primaryKeyName, $primaryKey);
        }

        return $query;
    }

    /**
     * Create DELETE FROM query
     *
     * @param string $table
     * @param string $primaryKey
     * @return Core_Model_Database_DeleteQuery
     */
    public function deleteFrom($table, $primaryKey = null)
    {
        $args = func_get_args();
        return call_user_func_array(array($this, 'delete'), $args);
    }

    /**
     * Create schema query
     *
     * @return Core_Model_Database_SchemaQuery
     */
    public function schema()
    {
        $classAlias = $this->connectionName . '_SchemaQuery';
        return wmvc::app()->getObject('Core_Model_Database_SchemaQuery', $classAlias, $this);
    }

    /**
     * Get PDO object
     *
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}
