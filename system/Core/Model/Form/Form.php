<?php
/**
 * Core_Model_Form
 *
 * Building of html-forms
 *
 * @package   WebooMVC
 * @author    Christoffer Bubach
 */

class Core_Model_Form {

    /**
     * @var string
     */
    protected $_type;

    /**
     * @var
     */
    protected $_value;

    /**
     * @var array
     */
    protected $_attributes = array();

    /**
     * @var string
     */
    public $_renderer;

    /**
     * @var Object
     */
    protected $_validator;

    /**
     * @var Object
     */
    protected $_model;

    /**
     * @var array
     */
    protected $_formElements = array();

    /**
     * @param $validator
     */
    public function setValidator($validator) {
        $this->_validator = $validator;
    }

    /**
     * @param $model
     */
    public function bind($model) {
        $this->_model = is_array($model) ? (object) $model : $model;
    }

    /**
     * @param $name
     * @return string
     */
    protected function _getModelValue($name) {
        if (empty($this->_model)) {
            return null;
        }
        return $this->_escapeValue($this->_model->getData($name));
    }

    /**
     * @param $name
     * @return string
     */
    protected function _getValidatorValue($name) {
        if (empty($this->_validator)) {
            return null;
        }
        return $this->_escapeValue($this->_validator->getValue($name));
    }

    /**
     * @param $value
     * @return string
     */
    protected function _escapeValue($value) {
        if (!is_string($value)) {
            return $value;
        }
        return htmlentities($value, ENT_QUOTES, 'UTF-8');
    }

    /**
     * @param  $name
     * @return null|string
     */
    public function getValueFor($name) {
        if ($validatorValue = $this->_getValidatorValue($name)) {
            return $validatorValue;
        }
        if ($modelValue = $this->_getModelValue($name)) {
            return $modelValue;
        }
        return null;
    }

    /**
     * Standard element rendering, with
     * type as tag-name
     *
     * @param  $type
     * @return string
     */
    protected function _elementRender($type = null) {
        if (empty($type)) {
            $type = $this->_type;
        }
        $result  = "\n    <".$type;
        $result .= $this->_renderAttributes();
        $result .= ">\n";

        return $result;
    }

    /**
     * Void renderer
     */
    protected function _voidRender() {
        return;
    }

    /**
     * Render current object/element
     *
     * @param null $newRenderer
     * @return string
     */
    public function render($newRenderer = null) {
        $func = (!empty($newRenderer)) ? $newRenderer : $this->_renderer;

        if (empty($func)) {
            $out = '';
            /** @var Core_Model_Form $element **/
            foreach ($this->_formElements as $element) {
                $out .= $element->render();
            }
            return $out;
        }
        return $this->$func();
    }

    /**
     * @return object
     */
    public function open() {
        $open = clone $this;

        $open->_attributes     = array();
        $open->_renderer       = '_elementRender';
        $open->_type           = 'form';
        $this->_formElements[] = $open;

        return $open;
    }

    /**
     * @return string
     */
    public function close() {
        $close = clone $this;

        $close->_attributes    = array();
        $close->_renderer      = '_closeRender';
        $close->_type          = 'formClose';
        $this->_formElements[] = $close;

        return $close;
    }

    /**
     * @return string
     */
    protected function _closeRender() {
        return "\n    </form>\n";
    }

    /**
     * @param  $value
     * @param  null $name
     * @return Object
     */
    public function button($value, $name = null) {
        $button = clone $this;

        $button->_attributes    = array();
        $button->_renderer      = '_buttonRender';
        $button->_type          = 'button';
        $this->_formElements[]  = $button;

        $button->_setValue($value);
        $button->setAttribute('type', 'button');
        $button->setAttribute('name', $name);

        return $button;
    }

    /**
     * @param  $value
     * @param  null $name
     * @return Object
     */
    public function submit($value, $name = null) {
        $submit = $this->button($value, $name);
        $submit->setAttribute('type', 'submit');
        return $submit;
    }

    /**
     * @return string
     */
    protected function _buttonRender() {
        return $this->_elementRender().$this->_value."</button>\n";
    }

    /**
     * @param  $name
     * @return Object
     */
    public function textarea($name) {
        $textarea = clone $this;

        $textarea->_attributes = array();
        $textarea->_renderer   = '_textareaRender';
        $textarea->_type       = 'textarea';
        $this->_formElements[] = $textarea;

        $textarea->setAttribute('rows', '10');
        $textarea->setAttribute('cols', '50');
        $textarea->setAttribute('name', $name);

        return $textarea;
    }

    /**
     * @return string
     */
    protected function _textareaRender() {
        return $this->_elementRender().$this->_value."</textarea>\n";
    }

    /**
     * @param  $labelName
     * @return Core_Model_Form
     */
    public function label($labelName) {
        $label = clone $this;

        $label->_value = array(
            'label'         => $labelName,
            'labelBefore'   => true,
            'element'       => null,
            'elementRender' => null
        );
        $label->_attributes = array();
        $label->_renderer   = '_labelRender';
        $label->_type       = 'label';
        $this->_formElements[] = $label;

        return $label;
    }

    /**
     * @return mixed|string
     */
    protected function _labelRender() {
        $result  = $this->_elementRender();
        $label   = empty($this->_value['label']) ? '' : $this->_value['label'];
        $result .= empty($this->_value['labelBefore']) ? $this->renderElement().$label : $label.$this->renderElement();
        $result .= '</label>';

        return $result;
    }

    /**
     * @param $name
     * @return $this
     */
    public function forId($name) {
        return $this->setAttribute('for', $name);
    }

    /**
     * @param  $element
     * @return $this
     */
    public function before($element) {
        return $this->_addLabelElement($element, true);
    }

    /**
     * @param  $element
     * @return $this
     */
    public function after($element) {
        return $this->_addLabelElement($element, false);
    }

    /**
     * @param  $element
     * @param  $before
     * @return $this
     */
    protected function _addLabelElement($element, $before) {
        $this->_value['element']       = $element;
        $this->_value['labelBefore']   = $before;
        $this->_value['elementRender'] = $element->_renderer;
        $element->_renderer            = '_voidRender';
        return $this;
    }

    /**
     * @return string
     */
    protected function renderElement() {
        /** @var $element Core_Model_Form */
        $element = is_object($this->_value['element']) ? $this->_value['element'] : null;
        if (!empty($element)) {
            return $element->render($this->_value['elementRender']);
        }
        return '';
    }

    /**
     * @return mixed
     */
    public function getControl() {
        return isset($this->_value['element']) ? $this->_value['element'] : null;
    }


    /**
     * Create a select-element clone, or if called
     * from within one, use internal _select method
     *
     * @param  $name
     * @param  array $options
     * @return Core_Model_Form
     */
    public function select($name, $options = array()) {
        if ($this->_type == 'select') {
            return $this->_select($name);
        }
        $select = clone $this;

        $select->_attributes   = array();
        $select->_renderer     = '_selectRender';
        $select->_type         = 'select';
        $this->_formElements[] = $select;
        $select->_value        = array(
            'selected' => null,
            'options'  => $options
        );

        $select->setAttribute('name', $name);
        return $select;
    }

    /**
     * @return string
     */
    protected function _selectRender() {
        $out = $this->_elementRender()."\n        \n";
        return $out.$this->_renderOptions()."</select>\n";
    }

    /**
     * @param  string $result
     * @return string
     */
    protected function _renderOptions($result = '') {
        foreach ($this->_value['options'] as $value => $label) {
            if (is_array($label)) {
                $result .= $this->_renderOptGroup($value, $label);
                continue;
            }
            $result .= $this->_renderOption($value, $label);
        }
        return $result;
    }

    /**
     * @param $label
     * @param $options
     * @return string
     */
    protected function _renderOptGroup($label, $options) {
        $result = "<optgroup label=\"{$label}\">";
        foreach ($options as $value => $label) {
            $result .= $this->_renderOption($value, $label);
        }
        $result .= "</optgroup>\n";
        return $result;
    }

    /**
     * @param $value
     * @param $label
     * @return string
     */
    protected function _renderOption($value, $label) {
        $option = '<option ';
        $option .= 'value="' . $value . '"';
        $option .= ($this->_value['selected'] == $value) ? ' selected' : '';
        $option .= '>';
        $option .= $label;
        $option .= "</option>\n";
        return $option;
    }

    /**
     * @param  $option
     * @return $this
     */
    public function _select($option) {
        $this->_value['selected'] = $option;
        return $this;
    }

    /**
     * @param  $options
     * @return $this
     */
    public function options($options = array()) {
        $this->_value['options'] = $options;
        return $this;
    }

    /**
     * @param $value
     * @param $label
     * @return $this
     */
    public function addOption($value, $label) {
        $this->_value['options'][$value] = $label;
        return $this;
    }

    /**
     * @param  $value
     * @return $this
     */
    public function defaultValue($value) {
        if ($this->_type == 'select') {
            if (!empty($this->_value['selected'])) {
                return $this;
            }
            return $this->_select($value);
        }
        if (!isset($this->_attributes['value'])) {
            $this->_setValue($value);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function multiple() {
        $name = $this->_attributes['name'];
        if (substr($name, -2) != '[]') {
            $name .= '[]';
        }

        $this->setAttribute('name', $name);
        $this->setAttribute('multiple', 'multiple');
        return $this;
    }

    /**
     * Render input elements with type attribute set
     *
     * @return string
     */
    protected function _inputRender() {
        $this->setAttribute('type', $this->_type);
        return $this->_elementRender('input');
    }

    /**
     * Create input object of set type
     *
     * @param  $type
     * @param  $name
     * @return Core_Model_Form
     */
    protected function _getInputType($type, $name) {
        $input = clone $this;

        $input->_attributes     = array();
        $input->_renderer       = '_inputRender';
        $input->_type           = $type;
        $this->_formElements[]  = $input;

        $input->setAttribute('name', $name);
        return $input;
    }

    /**
     * @param  $name
     * @return Object
     */
    public function date($name) {
        return $this->_getInputType('date', $name);
    }

    /**
     * @param  $name
     * @return Object
     */
    public function email($name) {
        return $this->_getInputType('email', $name);
    }

    /**
     * @param  $name
     * @return Object
     */
    public function text($name) {
        return $this->_getInputType('text', $name);
    }

    /**
     * @param  $name
     * @return Object
     */
    public function password($name) {
        return $this->_getInputType('password', $name);
    }

    /**
     * @param  $name
     * @return Object
     */
    public function hidden($name) {
        return $this->_getInputType('hidden', $name);
    }

    /**
     * @param  $name
     * @return Object
     */
    public function file($name) {
        return $this->_getInputType('file', $name);
    }

    /**
     * @param  $name
     * @param  $value
     * @return Object
     */
    public function checkbox($name, $value = 1) {
        return $this->_getInputType('checkbox', $name)->_setValue($value);
    }

    /**
     * @param  $name
     * @param  $value
     * @return Object
     */
    public function radio($name, $value) {
        if (empty($value)) {
            $value = $name;
        }
        return $this->_getInputType('radio', $name)->_setValue($value);
    }

    /**
     * @param  string $result
     * @return string
     */
    protected function _renderAttributes($result = '') {
        foreach ($this->_attributes as $attribute => $value) {
            $result .= " {$attribute}=\"{$value}\"";
        }
        return $result;
    }

    /**
     * @param  $attribute
     * @param  null $value
     * @return $this
     */
    public function setAttribute($attribute, $value = null) {
        if (!is_null($value)) {
            $this->_attributes[$attribute] = $value;
        }
        return $this;
    }

    /**
     * @param  $attribute
     * @return $this
     */
    public function removeAttribute($attribute) {
        unset($this->_attributes[$attribute]);
        return $this;
    }

    /**
     * @param  $attribute
     * @return mixed
     */
    public function getAttribute($attribute) {
        return $this->_attributes[$attribute];
    }

    /**
     * @param  $class
     * @return $this
     */
    public function addClass($class) {
        if (isset($this->_attributes['class'])) {
            $class = $this->_attributes['class'] . ' ' . $class;
        }
        return $this->setAttribute('class', $class);
    }

    /**
     * @param  $class
     * @return $this
     */
    public function removeClass($class) {
        if (!isset($this->_attributes['class'])) {
            return $this;
        }

        $class = trim(str_replace($class, '', $this->_attributes['class']));
        if ($class == '') {
            $this->removeAttribute('class');
            return $this;
        }

        return $this->setAttribute('class', $class);
    }

    /**
     * @param  $value
     * @return $this
     */
    protected function _setValue($value) {
        if ($this->_type == 'button' || $this->_type == 'textarea') {
            $this->_value = $value;
        } else {
            if ($value instanceof DateTime) {
                $value = $value->format('Y-m-d');
            }
            $this->setAttribute('value', $value);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function post() {
        return $this->setAttribute('method', 'post');
    }

    /**
     * @return $this
     */
    public function get() {
        return $this->setAttribute('method', 'get');
    }

    /**
     * @param  $id
     * @return $this
     */
    public function id($id) {
        return $this->setAttribute('id', $id);
    }

    /**
     * @param  $name
     * @return $this
     */
    public function name($name) {
        return $this->setAttribute('name', $name);
    }

    /**
     * @return $this
     */
    public function multipart() {
        return $this->setAttribute('enctype', 'multipart/form-data');
    }

    /**
     * @param $action
     * @return $this
     */
    public function action($action) {
        return $this->setAttribute('action', $action);
    }

    /**
     * @param  $value
     * @return $this
     */
    public function value($value) {
        return $this->_setValue($value);
    }

    /**
     * @param  $placeholder
     * @return $this
     */
    public function placeholder($placeholder) {
        return $this->setAttribute('placeholder', $placeholder);
    }

    /**
     * @return $this
     */
    public function required() {
        return $this->setAttribute('required', 'required');
    }

    /**
     * @return $this
     */
    public function optional() {
        return $this->removeAttribute('required');
    }

    /**
     * @return $this
     */
    public function disable() {
        return $this->setAttribute('disabled', 'disabled');
    }

    /**
     * @return $this
     */
    public function enable() {
        return $this->removeAttribute('disabled');
    }

    /**
     * @return $this
     */
    public function autofocus() {
        return $this->setAttribute('autofocus', 'autofocus');
    }

    /**
     * @return $this
     */
    public function unfocus() {
        return $this->removeAttribute('autofocus');
    }

    /**
     * @param  $rows
     * @return $this
     */
    public function rows($rows) {
        return $this->setAttribute('rows', $rows);
    }

    /**
     * @param  $cols
     * @return $this
     */
    public function cols($cols) {
        return $this->setAttribute('cols', $cols);
    }

    /**
     * @return $this
     */
    public function defaultToChecked() {
        if (empty($this->_value)) {
            $this->check();
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function defaultToUnchecked() {
        if (empty($this->_value)) {
            $this->uncheck();
        }
        return $this;
    }

    /**
     * @param $state
     * @return $this
     */
    public function defaultCheckedState($state) {
        $state ? $this->defaultToChecked() : $this->defaultToUnchecked();
        return $this;
    }

    /**
     * @return $this
     */
    public function check() {
        $this->_setChecked(true);
        return $this;
    }

    /**
     * @return $this
     */
    public function uncheck() {
        $this->_setChecked(false);
        return $this;
    }

    /**
     * @param bool $checked
     */
    protected function _setChecked($checked = true) {
        $this->_value = $checked;
        $this->removeAttribute('checked');

        if ($checked) {
            $this->setAttribute('checked', 'checked');
        }
    }

}