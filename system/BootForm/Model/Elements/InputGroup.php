<?php
/**
 * BootForm_Model_Elements_InputGroup
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_Elements_InputGroup extends Form_Model_Elements_Input {

    /**
     * @var array
     */
    protected $_beforeAddon = array();

    /**
     * @var array
     */
    protected $_afterAddon = array();

    /**
     * @param  $addon
     * @return $this
     */
    public function beforeAddon($addon) {
        $this->_beforeAddon[] = $addon;
        return $this;
    }

    /**
     * @param  $addon
     * @return $this
     */
    public function afterAddon($addon) {
        $this->_afterAddon[] = $addon;
        return $this;
    }

    /**
     * @param  $type
     * @return $this
     */
    public function type($type) {
        $this->_attributes['type'] = $type;
        return $this;
    }

    /**
     * @param  $addons
     * @return string
     */
    protected function _renderAddons($addons) {
        $html = '';

        foreach ($addons as $addon) {
            $html .= '<span class="input-group-addon">';
            $html .= $addon;
            $html .= '</span>';
        }
        return $html;
    }

    /**
     * @return mixed|string
     */
    public function render() {
        $html = '<div class="input-group">';
        $html .= $this->_renderAddons($this->_beforeAddon);
        $html .= parent::render();
        $html .= $this->_renderAddons($this->_afterAddon);
        $html .= '</div>';

        return $html;
    }

}
