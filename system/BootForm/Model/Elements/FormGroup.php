<?php
/**
 * BootForm_Model_Elements_FormGroup
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_Elements_FormGroup extends Form_Model_Elements_Base
{

    /**
     * @var string
     */
    protected $_label;

    /**
     * @var
     */
    protected $_control;

    /**
     * @var
     */
    protected $_helpBlock;

    /**
     * @var
     */
    protected static $_helpBlockCount = 0;

    /**
     * @param string $label
     * @param string $control
     */
    public function __construct($label = '', $control = '')
    {
        $this->_label   = $label;
        $this->_control = $control;
        $this->addClass('form-group');
    }

    /**
     * @return mixed|string
     */
    public function render() {
        $html  = '<div';
        $html .= $this->renderAttributes();
        $html .= '>';
        $html .= $this->_label;
        $html .= $this->_control;
        $html .= $this->renderHelpBlock();
        $html .= '</div>';

        return $html;
    }

    /**
     * @param  $text
     * @return $this
     */
    public function helpBlock($text)
    {
        if (isset($this->_helpBlock)) {
            return $this;
        }
        self::$_helpBlockCount++;

        $this->_helpBlock = $this->getObject(
            'BootForm_Model_Elements_HelpBlock',
            'HelpBlock_' . self::$_helpBlockCount,
            array($text)
        );

        return $this;
    }

    /**
     * @return string
     */
    protected function renderHelpBlock()
    {
        if ($this->_helpBlock) {
            return $this->_helpBlock->render();
        }
        return '';
    }

    /**
     * @return string
     */
    public function control()
    {
        return $this->_control;
    }

    /**
     * @return string
     */
    public function label()
    {
        return $this->_label;
    }

    /**
     * @param  $method
     * @param  $parameters
     * @return $this
     */
    public function __call($method, $parameters)
    {
        call_user_func_array(array($this->_control, $method), $parameters);
        return $this;
    }
}
