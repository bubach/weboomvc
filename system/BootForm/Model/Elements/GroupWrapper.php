<?php
/**
 * BootForm_Model_Elements_GroupWrapper
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_Elements_GroupWrapper
{

    /**
     * @var
     */
    protected $_formGroup;

    /**
     * @param $formGroup
     */
    public function __construct($formGroup = null)
    {
        $this->_formGroup = $formGroup;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->_formGroup->render();
    }

    /**
     * @param  $text
     * @return $this
     */
    public function helpBlock($text)
    {
        $this->_formGroup->helpBlock($text);
        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @param  $class
     * @return $this
     */
    public function labelClass($class)
    {
        $this->_formGroup->label()->addClass($class);
        return $this;
    }

    /**
     * @return $this
     */
    public function hideLabel()
    {
        $this->labelClass('sr-only');
        return $this;
    }

    /**
     * @return $this
     */
    public function inline()
    {
        $this->_formGroup->inline();
        return $this;
    }

    /**
     * @param  $method
     * @param  $parameters
     * @return $this
     */
    public function __call($method, $parameters)
    {
        call_user_func_array(array($this->_formGroup->control(), $method), $parameters);
        return $this;
    }
}
