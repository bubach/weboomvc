<?php
/**
 * BootForm_Model_Elements_OffsetFormGroup
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_Elements_OffsetFormGroup
{

    /**
     * @var
     */
    protected $_control;

    /**
     * @var
     */
    protected $_columnSizes = array();

    /**
     * @param string $control
     * @param string $columnSizes
     */
    public function __construct($control = '', $columnSizes = '')
    {
        $this->_control     = $control;
        $this->_columnSizes = $columnSizes;
    }

    /**
     * @return string
     */
    public function render()
    {
        $html  = '<div class="form-group">';
        $html .= '<div class="' . $this->getControlClass() . '">';
        $html .=  $this->_control;
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * @param  $columnSizes
     * @return $this
     */
    public function setColumnSizes($columnSizes)
    {
        $this->_columnSizes = $columnSizes;
        return $this;
    }

    /**
     * @return string
     */
    protected function getControlClass()
    {
        $class = '';

        foreach ($this->_columnSizes as $breakpoint => $sizes) {
            $offset = 12 - $sizes[1];
            $class .= sprintf('col-%s-offset-%s col-%s-%s ', $breakpoint, $offset, $breakpoint, $sizes[1]);
        }

        return trim($class);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @param  $method
     * @param  $parameters
     * @return $this
     */
    public function __call($method, $parameters)
    {
        call_user_func_array(array($this->_control, $method), $parameters);

        return $this;
    }
}
