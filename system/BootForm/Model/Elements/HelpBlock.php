<?php
/**
 * BootForm_Model_Elements_HelpBlock
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_Elements_HelpBlock extends Form_Model_Elements_Base
{

    /**
     * @var null
     */
    private $_message;

    /**
     * @param string $message
     */
    public function __construct($message = '')
    {
        $this->_message = $message;
        $this->addClass('help-block');
    }

    /**
     * @return mixed|string
     */
    public function render()
    {
        $html = '<p';
        $html .= $this->renderAttributes();
        $html .= '>';
        $html .= $this->_message;
        $html .= '</p>';

        return $html;
    }
}