<?php
/**
 * BootForm_Model_Elements_HelpBlock
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_Elements_HorizontalFormGroup extends BootForm_Model_Elements_FormGroup
{

    /**
     * @var
     */
    protected $_controlSizes;

    /**
     * @param string $label
     * @param string $control
     * @param string $controlSizes
     */
    public function __construct($label = '', $control = '', $controlSizes = '')
    {
        parent::__construct($label, $control);
        $this->_controlSizes = $controlSizes;
    }

    /**
     * @return mixed|string
     */
    public function render()
    {
        $html  = '<div';
        $html .= $this->renderAttributes();
        $html .= '>';
        $html .=  $this->_label;
        $html .= '<div class="' . $this->getControlClass() . '">';
        $html .=  $this->_control;
        $html .= $this->renderHelpBlock();
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * @return string
     */
    protected function getControlClass() {
        $class = '';
        foreach ($this->_controlSizes as $breakpoint => $size) {
            $class .= sprintf('col-%s-%s ', $breakpoint, $size);
        }
        return trim($class);
    }

    /**
     * @param  $method
     * @param  $parameters
     * @return $this
     */
    public function __call($method, $parameters) {
        call_user_func_array(array($this->control, $method), $parameters);
        return $this;
    }

}