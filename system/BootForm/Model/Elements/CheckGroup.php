<?php
/**
 * BootForm_Model_Elements_CheckGroup
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_Elements_CheckGroup extends BootForm_Model_Elements_FormGroup {

    /**
     * @var Label
     */
    protected $_label;

    /**
     * @var bool
     */
    protected $_inline = false;

    /**
     * @param string $label
     */
    public function __construct($label = '')
    {
        $this->_label = $label;
    }

    /**
     * @return string
     */
    public function render()
    {
        if ($this->_inline === true) {
            return $this->_label->render();
        }

        return sprintf(
            '<div%s>%s%s</div>',
            $this->renderAttributes(),
            $this->_label,
            $this->renderHelpBlock()
        );
    }

    /**
     * @return $this
     */
    public function inline() {
        $this->_inline = true;
        $class = $this->control()->getAttribute('type') . '-inline';
        $this->label->removeClass('control-label')->addClass($class);

        return $this;
    }

    /**
     * @return mixed
     */
    public function control() {
        return $this->_label->getControl();
    }

    /**
     * @param  $method
     * @param  $parameters
     * @return $this
     */
    public function __call($method, $parameters) {
        call_user_func_array(array($this->_label->getControl(), $method), $parameters);
        return $this;
    }

}