<?php
/**
 * BootForm_Model_BootForm
 *
 * @package     WebooMVC
 * @author      Christoffer Bubach
 */

class BootForm_Model_BootForm extends Form_Model_FormBuilder
{

    /**
     * @var bool
     */
    protected $_formTypeHorizontal = false;

    /**
     * @var
     */
    protected $_columnSizes;

    /**
     * @param  $columnSizes
     * @return Form_Model_Elements_FormOpen
     */
    public function openHorizontal($columnSizes = array('lg' => array(2, 10)))
    {
        $this->_formTypeHorizontal = true;
        $this->setColumnSizes($columnSizes);

        return $this->open()->addClass('form-horizontal');
    }

    /**
     * @param  $columnSizes
     * @return $this
     */
    public function setColumnSizes($columnSizes)
    {
        $this->_columnSizes = $columnSizes;
        return $this;
    }

    /**
     * @return array
     */
    protected function getControlSizes()
    {
        $controlSizes = [];

        foreach ($this->_columnSizes as $breakpoint => $sizes) {
            $controlSizes[$breakpoint] = $sizes[1];
        }

        return $controlSizes;
    }

    /**
     * @return string
     */
    protected function getLabelClass()
    {
        $class = '';

        foreach ($this->_columnSizes as $breakpoint => $sizes) {
            $class .= sprintf('col-%s-%s ', $breakpoint, $sizes[0]);
        }

        return trim($class);
    }

    /**
     * @param  $label
     * @param  $name
     * @param  $control
     * @return BootForm_Model_Elements_GroupWrapper
     */
    protected function formGroup($label, $name, $control)
    {
        $label = $this->label($label)->addClass('control-label')->forId($name);
        $control->id($name)->addClass('form-control');

        $this->_elementCount++;
        if ($this->_formTypeHorizontal) {
            $label->addClass($this->getLabelClass());

            $formGroup = $this->getObject(
                'BootForm_Model_Elements_HorizontalFormGroup',
                'HorizontalFormGroup_'.$this->_elementCount,
                array(
                    $label,
                    $control,
                    $this->getControlSizes()
                )
            );
        } else {
            $formGroup = $this->getObject(
                'BootForm_Model_Elements_FormGroup',
                'FormGroup_'.$this->_elementCount,
                array(
                    $label,
                    $control
                )
            );
        }

        if ($this->hasError($name)) {
            $formGroup->helpBlock($this->getError($name));
            $formGroup->addClass('has-error');
        }

        return $this->wrap($formGroup);
    }

    /**
     * @param  $group
     * @return BootForm_Model_Elements_GroupWrapper
     */
    protected function wrap($group)
    {
        $this->_elementCount++;

        return $this->getObject(
            'BootForm_Model_Elements_GroupWrapper',
            'GroupWrapper_'.$this->_elementCount,
            $group
        );
    }

    /**
     * @param  $name
     * @param  null $value
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|Form_Model_Elements_Input
     */
    public function text($name, $value = null, $label = null)
    {
        $control = parent::text($name)->value($value);
        return $this->formGroup($label, $name, $control);
    }

    /**
     * @param  $name
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function password($name, $label = null)
    {
        $control = parent::password($name);
        return $this->formGroup($label, $name, $control);
    }

    /**
     * @param  $value
     * @param  null $name
     * @param  string $type
     * @return object
     */
    public function button($value, $name = null, $type = "btn-default")
    {
        $button = parent::button($value, $name)->addClass('btn')->addClass($type);

        if ($this->_formTypeHorizontal) {
            $this->_elementCount++;

            return $this->getObject(
                'BootForm_Model_Elements_OffsetFormGroup',
                'OffsetFormGroup_'.$this->_elementCount,
                array(
                    $button,
                    $this->_columnSizes
                )
            );
        } else {
            return $button;
        }
    }

    /**
     * @param  string $value
     * @param  string $type
     * @return object
     */
    public function submit($value = "Submit", $type = "btn-default")
    {
        $submit = parent::submit($value)
            ->addClass('btn')
            ->addClass($type);

        if ($this->_formTypeHorizontal) {
            $this->_elementCount++;

            return $this->getObject(
                'BootForm_Model_Elements_OffsetFormGroup',
                'OffsetFormGroup_'.$this->_elementCount,
                array(
                    $submit,
                    $this->_columnSizes
                )
            );
        } else {
            return $submit;
        }
    }

    /**
     * @param  array $name
     * @param  array $options
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function select($name, $options = array(), $label = null)
    {
        $control = parent::select($name, $options);
        return $this->formGroup($label, $name, $control);
    }

    /**
     * @param  int $name
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function checkbox($name, $label = null)
    {
        $control    = parent::checkbox($name);
        $checkGroup = $this->checkGroup($label, $name, $control);

        if ($this->_formTypeHorizontal) {
            $checkGroup->addClass('checkbox');
            $this->_elementCount++;

            return $this->getObject(
                'BootForm_Model_Elements_OffsetFormGroup',
                'OffsetFormGroup_'.$this->_elementCount,
                array(
                    $this->wrap($checkGroup),
                    $this->_columnSizes
                )
            );
        } else {
            return $checkGroup;
        }
    }

    /**
     * @param  $label
     * @param  $name
     * @return mixed
     */
    public function inlineCheckbox($label, $name)
    {
        return $this->checkbox($label, $name)->inline();
    }

    /**
     * @param  $label
     * @param  $name
     * @param  $control
     * @return BootForm_Model_Elements_GroupWrapper
     */
    protected function checkGroup($label, $name, $control)
    {
        if ($this->_formTypeHorizontal) {
            return $this->buildCheckGroup($label, $name, $control, null);
        } else {
            $checkGroup = $this->buildCheckGroup($label, $name, $control);
            return $this->wrap($checkGroup->addClass('checkbox'));
        }
    }

    /**
     * @param  $label
     * @param  $name
     * @param  $control
     * @param  string $labelClass
     * @return CheckGroup
     */
    protected function buildCheckGroup($label, $name, $control, $labelClass = 'control-label')
    {
        $label = $this->label($label)->after($control)->addClass($labelClass);

        $this->_elementCount++;
        $checkGroup = $this->getObject(
            'BootForm_Model_Elements_CheckGroup',
            'CheckGroup_'.$this->_elementCount,
            $label
        );

        if ($this->hasError($name)) {
            $checkGroup->helpBlock($this->getError($name));
            $checkGroup->addClass('has-error');
        }

        return $checkGroup;
    }

    /**
     * @param  null $name
     * @param  null $value
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function radio($name, $value = null, $label = null)
    {
        if (is_null($value)) {
            $value = $label;
        }
        $control = parent::radio($name, $value);

        if ($this->_formTypeHorizontal) {
            $this->_elementCount++;
            $checkGroup = $this->checkGroup($label, $name, $control)->addClass('radio');

            return $this->getObject(
                'BootForm_Model_Elements_OffsetFormGroup',
                'OffsetFormGroup_'.$this->_elementCount,
                array(
                    $this->wrap($checkGroup),
                    $this->_columnSizes
                )
            );
        } else {
            return $this->radioGroup($label, $name, $control);
        }
    }

    /**
     * @param  $label
     * @param  $name
     * @param  null $value
     * @return mixed
     */
    public function inlineRadio($label, $name, $value = null)
    {
        return $this->radio($label, $name, $value)->inline();
    }

    /**
     * @param  $label
     * @param  $name
     * @param  $control
     * @return BootForm_Model_Elements_GroupWrapper
     */
    protected function radioGroup($label, $name, $control)
    {
        $checkGroup = $this->buildCheckGroup($label, $name, $control);
        return $this->wrap($checkGroup->addClass('radio'));
    }

    /**
     * @param  $name
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function textarea($name, $label = null)
    {
        $control = parent::textarea($name);
        return $this->formGroup($label, $name, $control);
    }

    /**
     * @param  $name
     * @param  null $value
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function date($name, $value = null, $label = null)
    {
        $control = parent::date($name)->value($value);
        return $this->formGroup($label, $name, $control);
    }

    /**
     * @param  $name
     * @param  null $value
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function email($name, $value = null, $label = null)
    {
        $control = parent::email($name)->value($value);
        return $this->formGroup($label, $name, $control);
    }

    /**
     * @param  $name
     * @param  null $value
     * @param  null $label
     * @return BootForm_Model_Elements_GroupWrapper|object
     */
    public function file($name, $value = null, $label = null)
    {
        $control = parent::file($name)->value($value);
        $label   = $this->label($label)->addClass('control-label')->forId($name);

        $control->id($name);
        $this->_elementCount++;

        if ($this->_formTypeHorizontal) {
            $label->addClass($this->getLabelClass());

            $formGroup = $this->getObject(
                'BootForm_Model_Elements_HorizontalFormGroup',
                'HorizontalFormGroup_'.$this->_elementCount,
                array(
                    $label,
                    $control,
                    $this->getControlSizes()
                )
            );
        } else {
            $formGroup = $this->getObject(
                'BootForm_Model_Elements_FormGroup',
                'FormGroup_'.$this->_elementCount,
                array(
                    $label,
                    $control
                )
            );
        }

        if ($this->hasError($name)) {
            $formGroup->helpBlock($this->getError($name));
            $formGroup->addClass('has-error');
        }

        if ($this->_formTypeHorizontal) {
            return $formGroup;
        } else {
            return $this->wrap($formGroup);
        }
    }

    /**
     * @param  $label
     * @param  $name
     * @param  null $value
     * @return BootForm_Model_Elements_GroupWrapper
     */
    public function inputGroup($label, $name, $value = null)
    {
        $this->_elementCount++;
        $control = $this->getObject(
            'BootForm_Model_Elements_InputGroup',
            'InputGroup_'.$this->_elementCount,
            $name
        );

        if (!is_null($value) || !is_null($value = $this->getValueFor($name))) {
            $control->value($value);
        }
        return $this->formGroup($label, $name, $control);
    }
}
