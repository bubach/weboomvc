<?php
/**
 * Config for automated bootstrap forms module
 *
 * @package     BootForm
 * @author      Christoffer Bubach
 */
return array (
    'Modules' => array (
        'System' => array (
            'BootForm' => array (
                'Config' => array (
                    'version' => '0.0.1',
                    'active' => true,
                    'setup' => false,
                ),
                'Model' => array (),
                'View' => array (),
                'Controller' => array (),
                'Plugins' => array (),
            ),
        ),
    ),
);